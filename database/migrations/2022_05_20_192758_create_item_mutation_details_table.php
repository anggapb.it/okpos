<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemMutationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_mutation_details', function (Blueprint $table) {
            $table->id();
            $table->integer('item_mutation_id');
            $table->integer('item_variant_id');
            $table->integer('stock');
            $table->integer('qty_mutation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_mutation_details');
    }
}
