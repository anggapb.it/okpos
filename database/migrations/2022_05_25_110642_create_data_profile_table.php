<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_profiles', function (Blueprint $table) {
            $table->id();
            // $table->foreignId('user_id')->unique()->constrained(); Upsert
            $table->string('store_name', 255); 
            $table->string('address', 255);
            $table->string('phone_number', 20);
            $table->string('logo_image', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_profile');
    }
}
