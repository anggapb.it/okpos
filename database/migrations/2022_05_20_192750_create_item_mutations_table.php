<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemMutationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_mutations', function (Blueprint $table) {
            $table->id();
            $table->integer('warehouse_id_first');
            $table->integer('warehouse_id_end');
            $table->string('status')->nullable();
            $table->string('notes')->nullable();
            $table->string('user_input')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_mutations');
    }
}
