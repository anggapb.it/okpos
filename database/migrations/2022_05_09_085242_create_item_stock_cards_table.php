<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemStockCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_stock_cards', function (Blueprint $table) {
            $table->id();
            $table->integer('id_item_variant');
            $table->integer('id_warehouse');
            $table->integer('stock_in');
            $table->integer('stock_out');
            $table->integer('stock_first');
            $table->integer('stock_final');
            $table->string('notes')->nullable();
            $table->string('type_stock_cards')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_stock_cards');
    }
}
