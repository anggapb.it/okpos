<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\DataProfile;

class DataProfileFactory extends Factory
{
    protected $model = DataProfile::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */

    //  protected $model

    public function definition()
    {
        return [
            'store_name' => 'OKSIP Store',
            'address' => 'Perum Garaha Indah Lestari Blok B1 No 4 Kec Arjasari Kab Bandung 40379',
            'phone_number' => '+62 822-1427-9770',
            'logo_image' => null
        ];
    }
}
