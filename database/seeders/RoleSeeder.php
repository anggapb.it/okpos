<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::upsert([
            [
                "id" => 1,
                "name" => "admin",
                "guard_name" => "web"
            ],
            [
                "id" => 2,
                "name" => "kasir",
                "guard_name" => "web"
            ],
        ], ['id'], ['name', 'guard_name']);
    }
}
