<?php

namespace Database\Seeders;

use App\Models\Customer;
use Illuminate\Database\Seeder;
use App\Models\DataProfile;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DataProfile::factory(1)->create();
        Customer::factory(3)->create();
        $this->call([
            RoleSeeder::class,
            UserSeeder::class
        ]);

    }
}
