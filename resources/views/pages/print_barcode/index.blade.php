@extends('layouts.main')

@section('page.title', 'Print Barcode')
@section('page.heading', 'Print Barcode')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
          <x-auth-validation-errors class="mb-4" :errors="$errors" />

                <div class="form-row mt-1">
                    <label  class="col-sm-2">Barang<span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input type="hidden" id="id_brg" name="id_brg">
                        <input type="text" class="form-control form-control-sm" id="barang" name="barang">
                    </div>
                    <div class="col-sm-2">
                        <button type="button" class="btn waves-effect waves-light btn-sm btn-primary btn-add-items"><i class="mdi mdi-cart-plus"></i> </button>
                    </div>
                </div>
                <div class="form-row mt-1">
                    <label  class="col-sm-2">Jumlah<span class="text-danger">*</span></label>
                    <div class="col-sm-1">

                        <input type="number" class="form-control form-control-sm" id="amount" name="amount" value="3">
                    </div>
                </div>
                <br>
                <div class="button-group">
                    <button type="button" id="btn_print" onclick="print()" class="btn waves-effect waves-light btn-info">Cetak</button>
                </div>



    </div>
</div>

<div class="modal fade" id="modalItems" tabindex="-1" role="dialog" aria-labelledby="modalPasienLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">Data Barang</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
              <div class="table-responsive">
                  <table class="table table-bordered" id='tbl_list_items' width="100%">
                  <thead>
                    <tr>
                      <th class="text-center">Aksi</th>
                      <th class="text-center">Item</th>
                    </tr>
                  </thead>
                </table>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>
      </div>
    </div>
  </div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){
    loadItems();
 $('body').on('click', '.btn-add-items', function () {
       $('#modalItems').modal('show');
});

});

function print() {
    let id = $('#id_brg').val();
    const arrId = id.split("-");
    let amount = $('#amount').val();

    if(amount > 15) {
        return alert("Batas maksimal 15 setiap 1 kali cetak");
    }

    let url = "{{ url('print-barcode/print') }}" + '/' + arrId[1] + '/' + amount + '/' + id;
    let params = `scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,
width=500,height=500,left=100,top=100`;



    if(id == ''){
        alert('barang belum dipilih!')
    } else if(amount < 1){
        alert('jumlah tidak boleh kurang dari satu!')
    } else {
        open(url, 'barcode', params);
    }
}

function loadItems(){
    $("#tbl_list_items").dataTable().fnDestroy();

const page_url = "{{ route('print.barcode.list.items') }}";

var table = $('#tbl_list_items').DataTable({
      processing: true,
      serverSide: true,
      ajax: {
          url: page_url,
          type: 'GET'
      },
      columns: [
        {data: 'id',sortable: false,
                    render: function (data, type, row, meta) {
                            var result = '<center><a class="btn btn-warning btn-sm btn-pilih-item"\
                                data-id="'+row.id+'"\
                                data-item_id="'+row.get_items.id+'"\
                                data-name="'+row.get_items.name+' '+row.size+'"\
                                data-price="'+row.price+'"\
                                 href="#"\
                            data-id="'+row.id+'"\
                            ><i class="mdi mdi-plus-box"></i></a></center>';
                            return result;
                     }
                },
        {data: 'items', name: 'items', orderable: false,searchable: true }
      ],
      responsive: true,
      columnDefs: [
          {
            // render:function(data){
            //     return moment(data).format('MM-DD-YYYY');
            // }, targets:[2],
          }
      ],
     // dom: 't',

  });
}

$('body').on('click', '.btn-pilih-item', function () {
    let id = $(this).data('id');
    let item_id = $(this).data('item_id');
    let name = $(this).data('name');
    let kdbrg = item_id +'-'+ id;

    $('#id_brg').val(kdbrg);
    $('#barang').val(name);
    $('#modalItems').modal('hide');
});

</script>
@endpush
