@extends('layouts.main')

@section('page.title', 'Edit Customer')
@section('page.heading', 'Edit Customer')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
          <x-auth-validation-errors class="mb-4" :errors="$errors" />

            <form method="POST" action="{{ route('customer.update') }}" id="frm_pp" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <input type="hidden" class="form-control" id="id" name="id" value="{{ $customer->id }}">


                <div class="form-row mt-1" id="div_name">
                    <label  class="col-sm-2 text-right">Nama</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="name" name="name" value="{{ $customer->name }}" required>
                    </div>
                </div>

                <div class="form-row mt-1">
                    <label  class="col-sm-2 text-right">Diskon</label>
                    <div class="col-md-10">
                        <input type="number" class="form-control" id="discount" name="discount" value="{{ $customer->discount }}" required>
                    </div>

                </div>

                <div class="form-row mt-1">
                    <label  class="col-sm-2 text-right">No. Telp</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="phone_number" name="phone_number" value="{{ $customer->phone_number }}" required>
                    </div>

                </div>

                <div class="form-row mt-1">
                    <label  class="col-sm-2 text-right">Alamat</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="address" name="address"value="{{ $customer->address }}" required>
                    </div>

                </div>

                <br>
                <div class="button-group">
                    <button type="submit" class="btn waves-effect waves-light btn-info">Simpan</button>
                    <a href="{{ route('customer') }}" class="btn waves-effect waves-light btn-warning">Kembali</a>
                </div>
            </form>


    </div>
</div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){

$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
 });


</script>
@endpush
