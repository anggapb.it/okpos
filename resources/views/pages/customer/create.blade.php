@extends('layouts.main')

@section('page.title', 'Create Customer')
@section('page.heading', 'Create Customer')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
          <x-auth-validation-errors class="mb-4" :errors="$errors" />

            <form method="POST" action="{{ route('customer.store') }}" id="frm_pp" class="form-horizontal" enctype="multipart/form-data">
                @csrf


                <div class="form-row mt-1" id="div_name">
                    <label  class="col-sm-2 text-right">Nama</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="name" name="name" :value="old('name')" required>
                    </div>

                </div>

                <div class="form-row mt-1">
                    <label  class="col-sm-2 text-right">Diskon</label>
                    <div class="col-md-10">
                        <input type="number" class="form-control" id="discount" name="discount" :value="old('discount')" required>
                    </div>

                </div>

                <div class="form-row mt-1">
                    <label  class="col-sm-2 text-right">No. Telp</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="phone_number" name="phone_number" :value="old('phone_number')" required>
                    </div>

                </div>

                <div class="form-row mt-1">
                    <label  class="col-sm-2 text-right">Alamat</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="address" name="address" :value="old('address')" required>
                    </div>

                </div>

                <br>
                <div class="button-group">
                    <button type="submit" class="btn waves-effect waves-light btn-info">Simpan</button>
                    <a href="{{ route('customer') }}" class="btn waves-effect waves-light btn-warning">Kembali</a>
                </div>
            </form>


    </div>
</div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){

$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
 });

</script>
@endpush
