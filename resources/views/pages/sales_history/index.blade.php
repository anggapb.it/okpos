@extends('layouts.main')

@section('page.title', 'Riwayat Penjualan')
@section('page.heading', 'Riwayat Penjualan')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif
            <div class="row">
              <div class="col-sm-5 mb-3 datepicker">
                <div class="input-daterange input-group datepicker" id="date-range">
                  <input type="text" autocomplete="off" class="form-control datePicker" id="dateFirst" />
                  <div class="input-group-append">
                      <span class="input-group-text bg-info b-0 text-white">s/d</span>
                  </div>
                  <input type="text" autocomplete="off" class="form-control datePicker" id="dateEnd" />
                </div>
                <div class="mx-1">
                  <button class="btn btn-primary mr-2" onclick="getDateValue()">Cari</button>
                  <button class="btn btn-warning" id="refresh">Refresh</button>
                </div>
              </div>
            </div>

            <div class="table-responsive">
              <table  id="tbl_user" class="table table-bordered" style="width:100%">
                <thead>
                  <tr>
                    <th class="align-middle" scope="col">No</th>
                    <th class="align-middle" scope="col">Tanggal</th>
                    <th class="align-middle" scope="col">Customer</th>
                    <th class="align-middle" scope="col">Total</th>
                    <th class="align-middle" scope="col">Opsi</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>

    </div>
</div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){
  $('.datePicker').datepicker({
    format: 'yyyy-mm-dd',
    formatSubmit: 'yyyy-mm-dd',
  });
  loadList();
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
});

function getDateValue(){
  let dateFirst = $('#dateFirst').val();
  let dateEnd = $('#dateEnd').val();
  sortedList(dateFirst, dateEnd);
}

function loadList(){
        let page_url = '{{ url('sales/get-data') }}';
        $('#tbl_user').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: page_url,
        },
        columns: [
          { "data": null,"sortable": false,
              render: function (data, type, row, meta) {
                  return meta.row + meta.settings._iDisplayStart + 1;
              }
          },
          {data: 'created_at'},
          {data: 'customer.name'},
          { data: 'cost_amount',
            render: $.fn.dataTable.render.number( '.', ',', 0, 'Rp. ' )
          },
          {"data": null,"sortable": false,
              render: function (data, type, row, meta) {
                  let result = `<a class="btn waves-effect waves-light btn-primary" href="" onclick="printReceipt(${row.id})" role="button">Cetak</a>`;
                  result += `&nbsp<a class="btn waves-effect waves-light btn-warning" href="{{url('sales-history/retur/${row.id}')}}" onclick="return confirm('Anda yakin mau menghapus item ini ?')" role="button">Retur</a>`;
                  return result;
              }
          }
        ],
        responsive: true,
        oLanguage: {
            sLengthMenu: "_MENU_",
            sSearch: ""
        },
        aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
        order: [[1, "asc"]],
        pageLength: 10
    });
}
function sortedList(dateFirst, dateEnd){
        let page_url = `{{url('sorted-sales/get-data')}}/${dateFirst}/${dateEnd}`;
        if (dateFirst != '' && dateEnd != '') {
          $('#tbl_user').DataTable().destroy(); 
        }
        $('#tbl_user').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: page_url,
        },
        columns: [
          { "data": null,"sortable": false,
              render: function (data, type, row, meta) {
                  return meta.row + meta.settings._iDisplayStart + 1;
              }
          },
          {data: 'created_at'},
          {data: 'customer.name'},
          { data: 'cost_amount',
            render: $.fn.dataTable.render.number( '.', ',', 0, 'Rp. ' )
          },
          {"data": null,"sortable": false,
              render: function (data, type, row, meta) {
              }
          }
        ],
        responsive: true,
        oLanguage: {
            sLengthMenu: "_MENU_",
            sSearch: ""
        },
        aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
        order: [[1, "asc"]],
        pageLength: 10
    });
}
$('#refresh').click(function(){
  $('#dateFirst').val('');
  $('#dateEnd').val('');
  $('#tbl_user').DataTable().destroy();
  loadList();
 });

function printReceipt(id) {
    let url = "{{ url('billing/print') }}" + '/' + id;
    let params = `scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=300,height=500,left=100,top=100`;
    open(url, 'receipt', params);
}
</script>
@endpush
