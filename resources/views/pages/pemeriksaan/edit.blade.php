@extends('layouts.main')

@section('page.title', 'Edit Pemeriksaan')
@section('page.heading', 'Edit Pemeriksaan')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                 {{Session::get('message')}}
            </div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                {{Session::get('error')}}
            </div>
        @endif
            <form method="POST" id="frm_pemeriksaan" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="button-group">
                    <button type="button" id="btn_update_pemeriksaan" class="btn waves-effect waves-light btn-info" onclick="updateData()">Submit</button>
                    {{-- <button type="button" class="btn waves-effect waves-light btn-warning" onclick="inputBaru()">Gunakan Template SOAP</button> --}}
                    <a href="{{ url()->previous() }}" class="btn waves-effect waves-light btn-danger">Kembali</a>
                    <input type="hidden" class="form-control form-control-sm" autocomplete="off" id="id_template_soap" name="id_template_soap">
                    <input type="hidden" class="form-control form-control-sm" autocomplete="off" id="iddokter" name="iddokter" value="{{ $reg->iddokter }}">
                    <input type="hidden" class="form-control form-control-sm" autocomplete="off" id="idregdet" name="idregdet" value="{{ $reg->idregdet }}">

                    </div>
                    <div class="form-row mt-1">
                        <label class="col-md-1 text-right">No RM</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control form-control-sm" autocomplete="off" id="norm" name="norm" value="{{ $reg->get_registrasi->norm }}" readOnly>
                          </div>
                          <label class="col-md-1 text-right">No. Reg</label>
                        <div class="col-md-2">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="jkelamin" name="jkelamin" readOnly value="{{ $reg->noreg }}">
                        </div>
                          <label class="col-md-1 text-right">Pasien</label>
                          <div class="col-md-4">
                            <input type="text" class="form-control form-control-sm" autocomplete="off" id="nmpasien" name="nmpasien" value="{{ $reg->get_registrasi->get_pasien->nmpasien }}" readOnly>
                          </div>


                    </div>
                 <div class="form-row mt-1">

                        {{-- <label class="col-md-1 text-right">Tgl. Input</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control form-control-sm" autocomplete="off" id="tglinput" name="tglinput">
                          </div> --}}
                        <label class="col-md-1 text-right">Tgl. Reg</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control form-control-sm" autocomplete="off" id="tgllahir" name="tgllahir" readOnly value="{{ $reg->tglreg }}">
                          </div>
                        <label class="col-md-1 text-right">dokter</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control form-control-sm" autocomplete="off" id="dokter" name="dokter" readOnly value="{{ $reg->get_dokter->nmdoktergelar }}">
                          </div>


                    </div>
                    <hr>
                    <ul class="nav nav-tabs" role="tablist">
                        <li style="display:none;" class="nav-item"> <a class="nav-link" data-toggle="tab" href="#subyektif" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Subyektif (S)</span></a> </li>
                       <li  class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#obyektif" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Form Pemeriksaan</span></a> </li>
                        <li style="display:none;" class="nav-item"> <a class="nav-link" data-toggle="tab" href="#assesment" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Assesment (A)</span></a> </li>
                        <li style="display:none;" class="nav-item"> <a class="nav-link" data-toggle="tab" href="#planning" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Planning (P)</span></a> </li>
                    </ul>
                    <div class="tab-content tabcontent-border">
                        <div class="tab-pane p-20" id="subyektif" role="tabpanel">
                            <div class="p-20">
                                <div class="form-row mt-1">

                                      <div class="col-md-1">
                                          <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-subyektif">Tambahkan Keluhan</button>
                                          </div>
                                </div>
                                <div class="table-responsive">
                                    <table  id="tbl_subyektif" class="table table-bordered" style="width:100%">
                                      <thead>
                                        <tr>
                                          <th class="align-middle" scope="col">Keluhan</th>
                                          <th class="align-middle" scope="col">Opsi</th>
                                        </tr>
                                      </thead>
                                      <tbody id="tbody_subyektif">

                                    </tbody>

                                    </table>
                                  </div>
                            </div>
                        </div>
                        <div class="tab-pane active p-20" id="obyektif" role="tabpanel">
                            <h6><strong>Tanda - tanda vital</strong></h6>

                            <input type="hidden" id="id_rekam_medis_soap" name="id_rekam_medis_soap" value={{ $rkmedis->id}}>
                            <input type="hidden" id="id_rekam_medis_soap_obyektif" name="id_rekam_medis_soap_obyektif" value={{ $rkmedis_obyektif->id_obyektif}}>
                            <div class="form-row mt-1">
                                <label class="col-md-2 text-right">Kesadaran</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="kesadaran" name="kesadaran" value={{ $rkmedis_obyektif->ttv_kesadaran}}>
                                  </div>
                                  <label class="col-md-1 text-right">Nadi</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="nadi" name="nadi" value={{ $rkmedis_obyektif->ttv_nadi}}>
                                  </div>
                                  <label class="col-md-2 text-right">Tek. Darah</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="tekanan_darah" name="tekanan_darah" value={{ $rkmedis_obyektif->ttv_tek_darah}}>
                                  </div>

                            </div>
                            <div class="form-row mt-1">
                                <label class="col-md-2 text-right">Frek. Nafas</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="frek_nafas" name="frek_nafas" value={{ $rkmedis_obyektif->ttv_frek_nafas}}>
                                  </div>
                                  <label class="col-md-1 text-right">Suhu</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="suhu" name="suhu" value={{ $rkmedis_obyektif->ttv_suhu}}>
                                  </div>
                                  <label class="col-md-2 text-right">SPO2</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="spo2" name="spo2" value={{ $rkmedis_obyektif->spo2}}>
                                  </div>
                            </div>
                            <h6><strong>Pemeriksaan Fisik</strong></h6>
                            <div class="form-row mt-1">
                                {{-- <label class="col-md-2 text-right">TB<span class="text-danger">*</span></label> --}}
                                <label class="col-md-2 text-right">TB</label>
                                <div class="col-md-1">
                                    <input type="text"  class="form-control form-control-sm" onchange="hitungImt()" autocomplete="off" id="tb" name="tb" value={{ $rkmedis_obyektif->pf_tb}}>
                                  </div>
                                  <label class="col-md-1 text-right">BB</label>
                                  <div class="col-md-1">
                                    <input type="text" class="form-control form-control-sm" onchange="hitungImt()" autocomplete="off" id="bb" name="bb" value={{ $rkmedis_obyektif->pf_bb}}>
                                  </div>
                                  <label class="col-md-1 text-right">IMT<span class="text-danger">*</span></label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="imt" name="imt" value={{ $rkmedis_obyektif->pf_imt}}>
                                  </div>
                            </div>
                            <div class="form-row mt-1">
                                <label class="col-md-2 text-right">Kepala</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="kepala" name="kepala" value={{ $rkmedis_obyektif->pf_kepala}}>
                                  </div>
                                  <label class="col-md-2 text-right">Mata</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="mata" name="mata" value={{ $rkmedis_obyektif->pf_mata}}>
                                  </div>
                                  <label class="col-md-1 text-right">Telinga</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="telinga" name="telinga" value={{ $rkmedis_obyektif->pf_telinga}}>
                                  </div>
                            </div>
                            <div class="form-row mt-1">
                                <label class="col-md-2 text-right">Hidung</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="hidung" name="hidung" value={{ $rkmedis_obyektif->pf_hidung}}>
                                  </div>
                                  <label class="col-md-2 text-right">Mulut dan faring</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="mulut_faring" name="mulut_faring" value={{ $rkmedis_obyektif->pf_mulut_faring}}>
                                  </div>
                                  <label class="col-md-1 text-right">Leher</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="leher" name="leher" value={{ $rkmedis_obyektif->pf_leher}}>
                                  </div>
                            </div>
                            <h6>&nbsp;&nbsp;&nbsp;Thoraks</h6>
                            <div class="form-row mt-1">
                                <label class="col-md-2 text-right">Paru</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="paru" name="paru" value={{ $rkmedis_obyektif->pf_thoraks_paru}}>
                                  </div>
                                  <label class="col-md-2 text-right">Jantung</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="jantung" name="jantung" value={{ $rkmedis_obyektif->pf_thoraks_jantung}}>
                                  </div>
                            </div>
                            <div class="form-row mt-1">
                                <label class="col-md-2 text-right">Abdomen</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="abdomen" name="abdomen" value={{ $rkmedis_obyektif->pf_abdomen}}>
                                  </div>
                                  <label class="col-md-2 text-right">Genitalia</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="genitalia" name="genitalia" value={{ $rkmedis_obyektif->pf_genitalia}}>
                                  </div>

                            </div>
                            <div class="form-row mt-1">
                                <label class="col-md-2 text-right">Extremitas</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="extremitas" name="extremitas" value={{ $rkmedis_obyektif->pf_extremitas}}>
                                  </div>

                                  <label class="col-md-2 text-right">Rectum / Anal</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="rectum" name="rectum" value={{ $rkmedis_obyektif->pf_rectum_anal}}>
                                  </div>
                            </div>
                            <br>
                            <h6><strong>Pemeriksaan Penunjang</strong></h6>
                            <div class="form-row mt-1">
                                <div class="col-md-1">
                                    <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-pemeriksaan_penunjang">Tambah</button>
                                    </div>
                          </div>
                          <div class="table-responsive">
                              <table  id="tbl_pemeriksaan_penunjang" class="table table-bordered" style="width:100%">
                                <thead>
                                  <tr>
                                    <th class="align-middle" scope="col">Keterangan</th>
                                    <th class="align-middle" scope="col">File Scan</th>
                                    <th class="align-middle" scope="col">Opsi</th>
                                  </tr>
                                </thead>
                                <tbody id="tbody_pemeriksaan_penunjang">

                                </tbody>
                              </table>
                            </div>
                        </div>
                        <div class="tab-pane p-20" id="assesment" role="tabpanel">
                            <div class="form-row mt-1">

                                  <div class="col-md-1">
                                      <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-diagnosa">Tambahkan Diagnosa</button>
                                      </div>
                            </div>
                            <div class="table-responsive">
                                <table  id="tbl_diagnosa" class="table table-bordered" style="width:100%">
                                  <thead>
                                    <tr>
                                      <th class="align-middle" scope="col">Kode</th>
                                      <th class="align-middle" scope="col">Nama</th>
                                      <th class="align-middle" scope="col">Opsi</th>
                                    </tr>
                                  </thead>
                                  <tbody id="tbody_diagnosa">

                                </tbody>
                                </table>
                              </div>
                        </div>
                        <div class="tab-pane p-20" id="planning" role="tabpanel">
                            <div class="form-row mt-1">

                                  <div class="col-md-1">
                                      <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-obat">Tambahkan Obat </button>
                                      </div>
                            </div>
                            <div class="table-responsive">
                                <table  id="tbl_obat" class="table table-bordered" style="width:100%">
                                  <thead>
                                    <tr>
                                      <th class="align-middle" scope="col">Obat</th>
                                      <th class="align-middle" scope="col">Catatan</th>
                                      <th class="align-middle" scope="col">Opsi</th>
                                    </tr>
                                  </thead>
                                  <tbody id="tbody_obat">

                                </tbody>
                                </table>
                              </div>
                              <hr>
                              <div class="form-row mt-1">

                                  <div class="col-md-1">
                                      <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-tindakan">Tambahkan Tindakan </button>
                                      </div>
                            </div>
                            <div class="table-responsive">
                                <table  id="tbl_tindakan" class="table table-bordered" style="width:100%">
                                  <thead>
                                    <tr>
                                      <th class="align-middle" scope="col">Tindakan</th>
                                      <th class="align-middle" scope="col">Catatan</th>
                                      <th class="align-middle" scope="col">Opsi</th>
                                    </tr>
                                  </thead>
                                  <tbody id="tbody_tindakan">

                                </tbody>
                                </table>
                              </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalSubyektif" tabindex="-1" role="dialog" aria-labelledby="modalSubyektifLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">List Data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <div class="col-md-1">
                <button type="button" class="btn waves-effect waves-light btn-sm btn-info btn-add-keluhan">Tambah</button>
                </div>
            <table class="table table-bordered" id='tbl_list_subyektif' width="100%">
                <thead>
                  <tr>
                    <th class="text-center">Opsi</th>
                    <th class="text-center">Nama</th>
                  </tr>
                </thead>
              </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>

<div class="modal fade" id="modalPenunjang" tabindex="-1" role="dialog" aria-labelledby="modalPenunjangLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">Tambah Pemeriksaan Penunjang</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" id="frm_pemeriksaan_penunjang" class="form-horizontal" enctype="multipart/form-data">
            @csrf
          <div class="modal-body">
            <div class="col-md-1">
                {{-- <button type="button" class="btn waves-effect waves-light btn-sm btn-info btn-add-penunjang">Tambah</button> --}}
                </div>
                    <div class="form-row mt-1" id="div_name">
                        <label  class="col-sm-4 text-right">Penunjang</label>
                        <div class="col-md-8">
                            <select class="form-control form-control-sm select2 cb_penunjang" name="cb_penunjang[]" id="cb_penunjang">
                                <option value="">Pilih</option>
                                @foreach ($mpenunjang as $row)
                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                              </select>
                        </div>
                    </div>
                    <div class="form-row mt-1" id="div_name">
                        <label  class="col-sm-4 text-right">File</label>
                        <div class="col-md-8">
                            <input type="file" class="form-control" id="file_penunjang" name="file_penunjang" autocomplete="off">
                        </div>
                    </div>


          </div>
          <div class="modal-footer">
            <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-ins-rkmedis-penunjang" onclick="insRkmedisPenunjang()">Simpan</button>
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
          </div>
        </form>
      </div>
    </div>
</div>

<div class="modal fade" id="modalDiagnosa" tabindex="-1" role="dialog" aria-labelledby="modalDiagnosaLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">List Data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <table class="table table-bordered" id='tbl_list_diagnosa' width="100%">
                <thead>
                  <tr>
                    <th class="text-center">Opsi</th>
                    <th class="text-center">Kode</th>
                    <th class="text-center">Nama</th>
                  </tr>
                </thead>
              </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>

<div class="modal fade" id="modalObat" tabindex="-1" role="dialog" aria-labelledby="modalObatLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">List Obat</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <table class="table table-bordered" id='tbl_list_obat' width="100%">
                <thead>
                  <tr>
                    <th class="text-center">Opsi</th>
                    <th class="text-center">Obat</th>
                  </tr>
                </thead>
              </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>

<div class="modal fade" id="modalTindakan" tabindex="-1" role="dialog" aria-labelledby="modalTindakanLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">List Tindakan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <table class="table table-bordered" id='tbl_list_tindakan' width="100%">
                <thead>
                  <tr>
                    <th class="text-center">Opsi</th>
                    <th class="text-center">Tindakan</th>
                  </tr>
                </thead>
              </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>

<div class="modal fade" id="modalTemplateSoap" tabindex="-1" role="dialog" aria-labelledby="modalTemplateLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">Template SOAP</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <table class="table table-bordered" id='tbl_template_soap' width="100%">
                <thead>
                  <tr>
                    <th class="text-center">Opsi</th>
                    <th class="text-center">Subyektif (S)</th>
                    <th class="text-center">Obyektif (O)</th>
                    <th class="text-center">Assesment (A)</th>
                    <th class="text-center">Planning (P)</th>
                  </tr>
                </thead>
              </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>


<div class="modal fade" id="modalAddKeluhan" tabindex="-1" role="dialog" aria-labelledby="modalKeluhanLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">Tambah Baru</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <form method="POST" id="frm_mkeluhan" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="form-row mt-1" id="div_name">
                    <label  class="col-sm-2 text-right">Keluhan</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="t_keluhan" name="t_keluhan" autocomplete="off">
                    </div>

                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-ins-keluhan" onclick="insKeluhan()">Simpan</button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            </div>
         </form>
      </div>
    </div>
</div>

<div class="modal fade" id="modalAddPenunjang" tabindex="-1" role="dialog" aria-labelledby="modalPenunjangLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">Tambah Baru</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <form method="POST" id="frm_mkeluhan" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="form-row mt-1" id="div_name">
                    <label  class="col-sm-4 text-right">Keterangan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="t_penunjang" name="t_penunjang" autocomplete="off">
                    </div>

                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-ins-keluhan" onclick="insPenunjang()">Simpan</button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            </div>
         </form>
      </div>
    </div>
</div>
@endsection

@push('bottom.scripts')
<script type="text/javascript">
 var rowIdxFile = 0;
$(document).ready(function(){
        $('#tglinput').datepicker('update', new Date());
        var id = "{{ $rkmedis->id }}";
        loadSoapPenunjang(id);

});

$('body').on('click', '.btn-add-keluhan', function () {
       $('#modalAddKeluhan').modal('show');
});

$('body').on('click', '.btn-add-penunjang', function () {
       $('#modalAddPenunjang').modal('show');
});

function insKeluhan()
{
    var keluhan = $("#t_keluhan").val();
    if(keluhan == '' || keluhan == null){
        Swal.fire("Error!", "Data tidak boleh kosong", "error");
    }else{
        var form_data = new FormData();
                    form_data.append("keluhan", keluhan);
          $.ajax({
            type: "POST",
            url: "{{ route('mkeluhan.store') }}"+'?_token=' + '{{ csrf_token() }}',
            data: form_data,
            dataType: "json",
            contentType: false,
            cache : false,
            processData : false,
            success: function(result){
                var oTable = $('#tbl_list_subyektif').dataTable();
                    oTable.fnDraw(false);
                    $('#modalAddKeluhan').modal('hide');
            } ,error: function(xhr, status, error) {
                alert(error);
            },

        });
    }
}

function insPenunjang()
{
    var penunjang = $("#t_penunjang").val();
    if(penunjang == '' || penunjang == null){
        Swal.fire("Error!", "Data tidak boleh kosong", "error");
    }else{
        var form_data = new FormData();
                    form_data.append("penunjang", penunjang);
          $.ajax({
            type: "POST",
            url: "{{ route('mpenunjang.store') }}"+'?_token=' + '{{ csrf_token() }}',
            data: form_data,
            dataType: "json",
            contentType: false,
            cache : false,
            processData : false,
            success: function(result){
                // var oTable = $('#tbl_list_penunjang').dataTable();
                //     oTable.fnDraw(false);
                   // $('#modalAddPenunjang').modal('hide');
                   location.reload();
                   $("html, body").animate({
            scrollTop: $('html, body').get(0).scrollHeight}, 1000);
            } ,error: function(xhr, status, error) {
                alert(error);
            },

        });
    }
}

function insRkmedisPenunjang()
{
    let id_rkmedis = "{{ $rkmedis->id }}";
    let id_rkmedis_obyektif = "{{ $rkmedis_obyektif->id_obyektif }}";
    let penunjang = $("#cb_penunjang").val();
    let file = $("#file_penunjang").val();
    if(penunjang == '' || file == ''){
        Swal.fire("Error!", "Penunjang dan file tidak boleh kosong", "error");
    }else{
        var form_data = new FormData();
            form_data.append("id_rkmedis", id_rkmedis);
            form_data.append("id_rkmedis_obyektif", id_rkmedis_obyektif);
            form_data.append("penunjang", penunjang);
            form_data.append("hasil", '');
            form_data.append("file_penunjang", document.getElementById("file_penunjang").files[0]);
          $.ajax({
            type: "POST",
            url: "{{ route('rkmedis-penunjang.store') }}"+'?_token=' + '{{ csrf_token() }}',
            data: form_data,
            dataType: "json",
            contentType: false,
            cache : false,
            processData : false,
            success: function(result){
                location.reload();
                $("html, body").animate({
            scrollTop: $('html, body').get(0).scrollHeight}, 1000);
                    //$('#modalAddPenunjang').modal('hide');
            } ,error: function(xhr, status, error) {
                alert(error);
            },

        });
    }
}


$('body').on('click', '.btn-subyektif', function () {
      loadKeluhan();
       $('#modalSubyektif').modal('show');

    });
$('body').on('click', '.btn-pemeriksaan_penunjang', function () {
      loadPenunjang();
       $('#modalPenunjang').modal('show');

    });

$('body').on('click', '.btn-diagnosa', function () {
      loadDiagnosa();
       $('#modalDiagnosa').modal('show');

    });

$('body').on('click', '.btn-obat', function () {
      loadObat();
       $('#modalObat').modal('show');

    });

$('body').on('click', '.btn-tindakan', function () {
      loadTindakan();
       $('#modalTindakan').modal('show');

    });

    function loadTindakan()
    {
        $("#tbl_list_tindakan").dataTable().fnDestroy();

        const page_url = "{{ route('template-soap.get-mtindakan') }}";
        var table = $('#tbl_list_tindakan').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                type: 'GET'
            },
            columns: [
                { "data": null,"sortable": false,
                            render: function (data, type, row, meta) {
                                    var result = '<a class="btn btn-success btn-set-tindakan" href="#"\
                                    data-kode="'+row.kdpelayanan+'"\
                                    data-name="'+row.nmpelayanan+'"\
                                    >Pilih</a>';
                                    return result;
                            }
                        },
                {data: 'nmpelayanan', name: 'nmpelayanan', orderable: true,searchable: true}
            ],
            responsive: true,
            columnDefs: [

            ],

        });
    }


    function loadObat()
    {
        $("#tbl_list_obat").dataTable().fnDestroy();

        const page_url = "{{ route('template-soap.get-mobat') }}";
        var table = $('#tbl_list_obat').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                type: 'GET'
            },
            columns: [
                { "data": null,"sortable": false,
                            render: function (data, type, row, meta) {
                                    var result = '<a class="btn btn-success btn-set-obat" href="#"\
                                    data-kode="'+row.kdbrg+'"\
                                    data-name="'+row.nmbrg+'"\
                                    >Pilih</a>';
                                    return result;
                            }
                        },
                {data: 'nmbrg', name: 'nmbrg', orderable: true,searchable: true}
            ],
            responsive: true,
            columnDefs: [

            ],

        });
    }

    function loadDiagnosa()
    {
        $("#tbl_list_diagnosa").dataTable().fnDestroy();

        const page_url = "{{ route('template-soap.get-mdiagnosa') }}";
        var table = $('#tbl_list_diagnosa').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                type: 'GET'
            },
            columns: [
                { "data": null,"sortable": false,
                            render: function (data, type, row, meta) {
                                    var result = '<a class="btn btn-success btn-set-diagnosa" href="#"\
                                    data-id="'+row.idpenyakit+'"\
                                    data-kode="'+row.kdpenyakit+'"\
                                    data-name="'+row.nmpenyakiteng+'"\
                                    >Pilih</a>';
                                    return result;
                            }
                        },
                {data: 'kdpenyakit', name: 'kdpenyakit', orderable: true,searchable: true},
                {data: 'nmpenyakiteng', name: 'nmpenyakiteng', orderable: true,searchable: true}
        ],
            responsive: true,
            columnDefs: [

            ],

        });
    }

    function loadPenunjang()
    {
        $("#tbl_list_penunjang").dataTable().fnDestroy();

        const page_url = "{{ route('template-soap.get-mpenunjang') }}";
        var table = $('#tbl_list_penunjang').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                type: 'GET'
            },
            columns: [
                { "data": null,"sortable": false,
                            render: function (data, type, row, meta) {
                                    var result = '<a class="btn btn-success btn-set-penunjang" href="#"\
                                    data-id="'+row.id+'"\
                                    data-name="'+row.name+'"\
                                    >Pilih</a>';
                                    return result;
                            }
                        },
                {data: 'name', name: 'name', orderable: true,searchable: true}
        ],
            responsive: true,
            columnDefs: [

            ],

        });
    }

    function loadKeluhan()
    {
        $("#tbl_list_subyektif").dataTable().fnDestroy();
        const page_url = "{{ route('template-soap.get-mkeluhan') }}";

        var table = $('#tbl_list_subyektif').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                type: 'GET'
            },
            columns: [
                { "data": null,"sortable": false,
                            render: function (data, type, row, meta) {
                                    var result = '<a class="btn btn-success btn-set-subyektif" href="#"\
                                    data-id="'+row.id+'"\
                                    data-subyektif="'+row.name+'"\
                                    >Pilih</a>';
                                    return result;
                            }
                        },
                {data: 'name', name: 'name', orderable: true,searchable: true}
        ],
            responsive: true,
            columnDefs: [

            ],

        });
    }



    $('body').on('click', '.btn-set-template', function () {
        var id_template_soap = $(this).data('id');

        $('#id_template_soap').val(id_template_soap);
        loadTempSubyektif(id_template_soap);
        loadTempPenunjang(id_template_soap);
        loadTempAssesment(id_template_soap);
        loadTempTindakan(id_template_soap);
        loadTempObat(id_template_soap);
        $('#modalTemplateSoap').modal('hide');

    });

    function loadTempSubyektif(id)
    {
        $('#tbl_subyektif tbody').empty();
        var page_url = "{{ route('rekam-medis.get-detail-template-soap') }}";
        $.ajax({
        url: page_url,
        type: 'GET',
        data: {
            id_template_soap : id,
            jns_soap : "subyektif"
        },
        success: function(result) {
                for(i = 0; i < result.total; i++)
                {
                    currentdata = result.list[i];

					var tr ="\
                        <tr>\
                            <td>"+ currentdata.subyektif +"</td>\
                            <td><a class='btn btn-danger btn-sm btn-delete-subyektif row-index'\
                            href='javascript:void(0);'>Hapus</a>\
                            <input type='hidden' name='id_subyektif[]' value="+currentdata.id+" />\
                            <input type='hidden' name='subyektif[]' value="+currentdata.subyektif+" />\
                            </td>\
                        </tr>\
                    ";


                    $('#tbl_subyektif tbody').append(tr);
                }
        }
      });
    }

    var rowIdx = 0;

$('body').on('click', '.btn-set-subyektif', function () {
        var id = $(this).data('id');
        var subyektif = $(this).data('subyektif');
        $('#modalSubyektif').modal('hide');
        $('#tbody_subyektif').append(`<tr id="R${++rowIdx}">
            <td class="row-index text-left">
                ${subyektif}
            </td>
            <td><a class='btn btn-danger btn-sm btn-delete-subyektif row-index'
            href='javascript:void(0);'>Hapus</a>
            <input type='hidden' name="id_subyektif[]" value="${id}" />
            <input type='hidden' name="subyektif[]" value="${subyektif}" />
            </td>

           </tr>`);

    });
    $('#tbody_subyektif').on('click', '.btn-delete-subyektif', function () {
        // var child = $(this).closest('tr').nextAll();
        // child.each(function () {
        //   var id = $(this).attr('id');
        //   var dig = parseInt(id.substring(1));
        //   $(this).attr('id', `R${dig - 1}`);
        // });
        $(this).closest('tr').remove();
        rowIdx--;
    });


    var rowIdxPenunjang = 0;

    $('body').on('click', '.btn-set-penunjang', function () {
        $("html, body").animate({
            scrollTop: $('html, body').get(0).scrollHeight}, 1000);
        var id = $(this).data('id');
        var name = $(this).data('name');
        $('#modalPenunjang').modal('hide');
        $('#tbody_pemeriksaan_penunjang').append(`<tr id="R${++rowIdxPenunjang}">
            <td class="row-index text-left">
                ${name}
            </td>
            <td class="row-index text-left">
                <input type="file" class="form-control hidden" id="file${rowIdxFile}">
                <label for="file">Format PDF / JPG / JPEG / PNG | Maks. 5 Mb</label>
            </td>
            <td><a class='btn btn-danger btn-sm btn-delete-penunjang row-index'
            href='javascript:void(0);'>Hapus</a>
            <input type='hidden' name="id_penunjang[]" value="${id}" />
            <input type='hidden' name="penunjang[]" id="penunjang${rowIdxFile}" value="${name}" />
            </td>
            ${++rowIdxFile}
           </tr>`);

    });

    // $('#tbody_pemeriksaan_penunjang').on('click', '.btn-delete-penunjang', function () {

    //     $(this).closest('tr').remove();
    //     rowIdxPenunjang--;
    // });
     $('#tbody_pemeriksaan_penunjang').on('click', '.btn-delete-penunjang', function () {

        if (confirm('Are you sure you want to delete?')) {
            $.ajax({
            type: "POST",
            url: "{{ url('rkmedis-penunjang/hapus') }}"+'?_token=' + '{{ csrf_token() }}',
            data: {
                id : $(this).data('id')
            },
            success: function(result){
                location.reload();
                $("html, body").animate({
            scrollTop: $('html, body').get(0).scrollHeight}, 1000);
            } ,error: function(xhr, status, error) {
                alert(error);
            },

        });
        } else {
        // Do nothing!
        console.log('Thing was not saved to the database.');
        }
     });



    var rowIdxDiagnosa = 0;
    $('body').on('click', '.btn-set-diagnosa', function () {
        var id = $(this).data('id');
        var kode = $(this).data('kode');
        var name = $(this).data('name');
        $('#modalDiagnosa').modal('hide');
        $('#tbody_diagnosa').append(`<tr id="R${++rowIdxDiagnosa}">
            <td class="row-index text-left">
                ${kode}
            </td>
            <td class="row-index text-left">
                ${name}
            </td>
            <td><a class='btn btn-danger btn-sm btn-delete-diagnosa row-index'
            href='javascript:void(0);'>Hapus</a>
            <input type='hidden' name="idpenyakit[]" value="${id}" />
            <input type='hidden' name="kdpenyakit[]" value="${kode}" />
            <input type='hidden' name="nmpenyakiteng[]" value="${name}" />
            </td>

           </tr>`);

    });

    $('#tbody_diagnosa').on('click', '.btn-delete-diagnosa', function () {
        // var child = $(this).closest('tr').nextAll();
        // child.each(function () {
        //   var id = $(this).attr('id');
        //   var dig = parseInt(id.substring(1));
        //   $(this).attr('id', `R${dig - 1}`);
        // });
        $(this).closest('tr').remove();
        rowIdxDiagnosa--;
    });

    var rowIdxObat = 0;
    $('body').on('click', '.btn-set-obat', function () {
        var kode = $(this).data('kode');
        var name = $(this).data('name');
        $('#modalObat').modal('hide');
        $('#tbody_obat').append(`<tr id="R${++rowIdxObat}">
            <td class="row-index text-left">
                ${name}
            </td>
            <td class="row-index text-left">
                <input type='text' name="catatan_obat[]"/>
            </td>
            <td><a class='btn btn-danger btn-sm btn-delete-obat row-index'
            href='javascript:void(0);'>Hapus</a>
            <input type='hidden' name="kdbrg[]" value="${kode}" />
            <input type='hidden' name="nmbrg[]" value="${name}" />
            </td>

           </tr>`);

    });

    $('#tbody_obat').on('click', '.btn-delete-obat', function () {
        // var child = $(this).closest('tr').nextAll();
        // child.each(function () {
        //   var id = $(this).attr('id');
        //   var dig = parseInt(id.substring(1));
        //   $(this).attr('id', `R${dig - 1}`);
        // });
        $(this).closest('tr').remove();
        rowIdxObat--;
    });


    var rowIdxTindakan = 0;
    $('body').on('click', '.btn-set-tindakan', function () {
        var kode = $(this).data('kode');
        var name = $(this).data('name');
        $('#modalTindakan').modal('hide');
        $('#tbody_tindakan').append(`<tr id="R${++rowIdxTindakan}">
            <td class="row-index text-left">
                ${name}
            </td>
            <td class="row-index text-left">
                <input type='text' name="catatan_tindakan[]"/>
            </td>
            <td><a class='btn btn-danger btn-sm btn-delete-tindakan row-index'
            href='javascript:void(0);'>Hapus</a>
            <input type='hidden' name="kdpelayanan[]" value="${kode}" />
            <input type='hidden' name="nmpelayanan[]" value="${name}" />
            </td>

           </tr>`);

    });


    $('#tbody_tindakan').on('click', '.btn-delete-tindakan', function () {
        // var child = $(this).closest('tr').nextAll();
        // child.each(function () {
        //   var id = $(this).attr('id');
        //   var dig = parseInt(id.substring(1));
        //   $(this).attr('id', `R${dig - 1}`);
        // });
        $(this).closest('tr').remove();
        rowIdxTindakan--;
    });

    function hitungImt()
    {
        var tb = $("#tb").val();
        var bb = $("#bb").val();
        if(tb != '' && bb != ''){
            var tinggirt = tb/100;
            var bmi = bb / (tinggirt * tinggirt);
            bmi = bmi.toFixed(2);
        }else{
            var bmi = 0;
        }

        $("#imt").val(bmi);
    }

    function updateData()
    {
        var tb = $("#tb").val();
        var bb = $("#bb").val();
        // if(tb == '' && bb == ''){
        //     Swal.fire("Error!", "Silahkan lengkapi form isian yang bertanda (*)", "error");
        // }
      //  else{
          //  $('#frm_pemeriksaan').submit();
          document.getElementById("btn_update_pemeriksaan").disabled = true;
          Swal.showLoading();
                const url = "{{ route('pemeriksaan.update') }}";
                 const files = document.querySelector('[type=file]').files;

                 const formData = new FormData();
                // for (let i = 0; i < files.length; i++) {
                //     let file = files[i];
                //     let penunjang =  $("#penunjang"+i).val();
                //     formData.append('files[]', file);
                //     formData.append('penunjang[]', penunjang);
                // }

                var rows = document.querySelectorAll('#tbl_pemeriksaan_penunjang tbody tr');
                for (let i = 0; i < rows.length; i++) {
                            let penunjang =  $("#penunjang"+i).val(); //rows[i];
                            let file_scan =  document.getElementById("file"+i);
                                formData.append('penunjang[]', penunjang);
                                formData.append("file[]", file_scan.files[0]);

                }


                formData.append("norm", $("#norm").val());
                formData.append("tglinput", $("#tglinput").val());
                formData.append("iddokter", $("#iddokter").val());
                formData.append("idregdet", $("#idregdet").val());
                formData.append("id_rekam_medis_soap", $("#id_rekam_medis_soap").val());
                formData.append("id_rekam_medis_soap_obyektif", $("#id_rekam_medis_soap_obyektif").val());
                //obyektif
                formData.append("kesadaran", $("#kesadaran").val());
                formData.append("nadi", $("#nadi").val());
                formData.append("tekanan_darah", $("#tekanan_darah").val());
                formData.append("frek_nafas", $("#frek_nafas").val());
                formData.append("suhu", $("#suhu").val());
                formData.append("tb", $("#tb").val());
                formData.append("bb", $("#bb").val());
                formData.append("imt", $("#imt").val());
                formData.append("kepala", $("#kepala").val());
                formData.append("mata", $("#mata").val());
                formData.append("telinga", $("#telinga").val());
                formData.append("hidung", $("#hidung").val());
                formData.append("mulut_faring", $("#mulut_faring").val());
                formData.append("leher", $("#leher").val());
                formData.append("paru", $("#paru").val());
                formData.append("jantung", $("#jantung").val());
                formData.append("abdomen", $("#abdomen").val());
                formData.append("genitalia", $("#genitalia").val());
                formData.append("rectum", $("#rectum").val());
                formData.append("extremitas", $("#extremitas").val());
                formData.append("spo2", $("#spo2").val());
                formData.append('_token', "{{ csrf_token() }}");
                         $.ajax({
                        type: "POST",
                        url: url,
                        data: formData,
                        dataType: "json",
                        contentType: false,
                        cache : false,
                        processData : false,
                        success: function(result){
                            Swal.hideLoading();
                            if(result.message != "") {
                                window.location = "{{ route('pemeriksaan') }}";
                                Swal.fire("Success!", result.message, "success");
                            } else {
                                Swal.fire("Error!", result.error, "error");
                                document.getElementById("btn_update_pemeriksaan").disabled = false;
                            }
                        } ,error: function(xhr, status, error) {
                            alert(error);
                        },

                    });

     //   }

    }


    function loadSoapPenunjang(id)
    {
        $('#tbl_pemeriksaan_penunjang tbody').empty();
        var page_url = "{{ route('rekam-medis.get-detail-soap') }}";
        $.ajax({
        url: page_url,
        type: 'GET',
        data: {
            id_rekam_medis_soap : id,
            jns_soap : "penunjang"
        },
        success: function(result) {
                for(i = 0; i < result.total; i++)
                {
                    currentdata = result.list[i];

                    if(currentdata.file == null || currentdata.file == '')
                    {
                        var file = '';
                    }else{
                        var file = currentdata.file;
                    }

					var tr ="\
                        <tr>\
                            <td>"+ currentdata.name +"</td>\
                            <td><a href='javascript:void(0)' onclick='downloadFile("+ currentdata.id +")'>"+ file +"</a></td>\
                            <td>\
                            <a class='btn btn-danger btn-sm btn-delete-penunjang row-index'\
                            href='#' data-id='"+currentdata.id+"'>Hapus</a>\
                            <input type='hidden' name='id_penunjang[]' value="+currentdata.id+" />\
                            <input type='file' class='form-control hidden' id='file"+rowIdxFile+"' style='display:none;'>\
                            <textarea name='penunjang[]' id='penunjang"+rowIdxFile+"' style='display:none;'>"+currentdata.name+"</textarea>\
                            </td>\
                        </tr>\
                    ";


                    $('#tbl_pemeriksaan_penunjang tbody').append(tr);
                    ++rowIdxFile;
                }
        }
      });
    }

    function downloadFile(id){
            var url = "{{url('rekam-medis/get_file_pasien')}}"+"/"+id
        window.open(url);
    }



</script>
@endpush
