@extends('layouts.main')

@section('page.title', 'Pemeriksaan Awal')
@section('page.heading', 'Pemeriksaan Awal')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <div class="row">
                <div class="col-md-5">
                  <h5 class="box-title m-t-30">Periode</h5>

                  <div class="input-daterange input-group" id="date-range">
                      <input type="text" autocomplete="off" class="form-control" id="tglawal" />
                      <div class="input-group-append">
                          <span class="input-group-text bg-info b-0 text-white">s/d</span>
                      </div>
                      <input type="text" autocomplete="off" class="form-control" id="tglakhir" />
                  </div>
                </div>
                <div class="col-mt-1">
                  <h5 class="box-title m-t-30">&nbsp;</h5>
                  <button type="button" id="btn_cari" class="btn btn-primary">Cari</button>
                </div>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered" id='tbl_pasien_registrasi' width="100%">
                <thead>
                  <tr>
                    <th class="text-center">Aksi</th>
                    <th class="text-center">No. Register</th>
                    <th class="text-center">Tgl Register</th>
                    <th class="text-center">No RM</th>
                    <th class="text-center">Nama</th>
                    <th class="text-center">L/P</th>
                    <th class="text-center">Dokter</th>
                    <th class="text-center">No. HP</th>
                    <th class="text-center">Status Pemeriksaan</th>
                  </tr>
                </thead>
              </table>
            </div>

        </div>
    </div>
</div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){
  $('#tglawal').datepicker('update', new Date());
    $('#tglakhir').datepicker('update', new Date());

    loadPasienReg();
});

function loadPasienReg(){
    $("#tbl_pasien_registrasi").dataTable().fnDestroy();

    const page_url = "{{ route('pemeriksaan.list-reg') }}";

    var table = $('#tbl_pasien_registrasi').DataTable({
          processing: true,
          serverSide: true,
          ajax: {
              url: page_url,
              type: 'GET',
              data: {
                        tglawal :  $("#tglawal").val(),
                        tglakhir :  $("#tglakhir").val()
                }
          },
          columns: [
            { "data": null,"sortable": false,
                        render: function (data, type, row, meta) {
                            if(row.st_input == '-1'){
                                var result = '<a class="btn btn-info btn-sm btn-pilih-pasien" href="{{ url('pemeriksaan/create').'/' }}'+row.noreg+'"\
                                >Input Pemeriksaan</a>';
                            }else{
                                var result = '<a class="btn btn-warning btn-sm btn-pilih-pasien" href="{{ url('pemeriksaan/edit').'/' }}'+row.get_rekam_medis.id+'"\
                                >Edit Pemeriksaan</a>';
                            }

                                return result;
                         }
                    },
            {data: 'noreg', name: 'noreg', orderable: false,searchable: true},
            {data: 'tglreg', name: 'tglreg', orderable: false,searchable: true},
            {data: 'get_registrasi.norm', name: 'get_registrasi.norm', orderable: false,searchable: true},
            {data: 'get_registrasi.get_pasien.nmpasien', name: 'get_registrasi.get_pasien.nmpasien', orderable: true,searchable: true},
            {data: 'get_registrasi.get_pasien.get_jkelamin.kdjnskelamin', name: 'get_registrasi.get_pasien.get_jkelamin.kdjnskelamin', orderable: false,searchable: true},
            {data: 'get_dokter.nmdoktergelar', name: 'get_dokter.nmdoktergelar', orderable: false,searchable: true},
            {data: 'get_registrasi.get_pasien.nohp', name: 'get_registrasi.get_pasien.nohp', orderable: false,searchable: true},
            {data: 'st_pemeriksaan', name: 'st_pemeriksaan', orderable: false,searchable: true},
          ],
          responsive: true,
          columnDefs: [
              {

              }
          ],
         // dom: 't',

      });
}
$('#btn_cari').click(function(){
          $('#tbl_pasien_registrasi').DataTable().destroy();
          loadPasienReg();
});
</script>
@endpush
