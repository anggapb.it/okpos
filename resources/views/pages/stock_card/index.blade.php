@extends('layouts.main')

@section('page.title', 'Kartu Stok')
@section('page.heading', 'Kartu Stok')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
          <x-auth-validation-errors class="mb-4" :errors="$errors" />

                <div class="form-row mt-1">
                    <label  class="col-sm-2">Lokasi<span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                       <select class="form-control form-control form-control-sm" name="warehouse_id" id="warehouse_id">
                        <option value="">Pilih</option>
                        @foreach ($warehouse as $id => $name)
                            <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                       </select>
                    </div>
                </div>

                <div class="form-row mt-1">
                    <label  class="col-sm-2">Barang<span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input type="hidden" id="id_brg" name="id_brg">
                        <input type="text" class="form-control form-control-sm" id="barang" name="barang" readonly>
                    </div>
                    <div class="col-sm-2">
                        <button type="button" class="btn waves-effect waves-light btn-sm btn-primary btn-add-items"><i class="mdi mdi-cart-plus"></i> </button>
                    </div>
                </div>

                <div class="form-row mt-1">
                    <label  class="col-sm-2">Stock</label>
                    <div class="col-sm-8">

                        <input type="text" class="form-control form-control-sm" id="stock" name="stock" readonly>
                    </div>

                </div>

                <hr>
                <div class="table-responsive">
                    <table  id="tbl_stock_card" class="table table-bordered" style="width:100%">
                      <thead>
                        <tr>
                          <th class="align-middle" scope="col">Item</th>
                          <th class="align-middle" scope="col">Stok Awal</th>
                          <th class="align-middle" scope="col">Stok Masuk</th>
                          <th class="align-middle" scope="col">Stok Keluar</th>
                          <th class="align-middle" scope="col">Stok Akhir</th>
                          <th class="align-middle" scope="col">Keterangan</th>
                          <th class="align-middle" scope="col">Tanggal</th>
                        </tr>
                      </thead>
                      <tbody id="tbody_stock_card">

                    </tbody>

                    </table>
                  </div>




    </div>
</div>

<div class="modal fade" id="modalItems" tabindex="-1" role="dialog" aria-labelledby="modalPasienLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">Data Barang</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
              <div class="table-responsive">
                  <table class="table table-bordered" id='tbl_list_items' width="100%">
                  <thead>
                    <tr>
                      <th class="text-center">Aksi</th>
                      <th class="text-center">Item</th>
                      <th class="text-center">Stock</th>
                    </tr>
                  </thead>
                </table>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>
      </div>
    </div>
  </div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){
  //  loadItems();
 $('body').on('click', '.btn-add-items', function () {
       $('#modalItems').modal('show');
});

});

$("#warehouse_id").change(function () {
        let wh = this.value;
        loadItems(wh);
});

function loadItems(wh){
    $("#tbl_list_items").dataTable().fnDestroy();

const page_url = "{{ url('so/list-items') }}" + '/' +wh;

var table = $('#tbl_list_items').DataTable({
      processing: true,
      serverSide: true,
      ajax: {
          url: page_url,
          type: 'GET'
      },
      columns: [
        {data: 'id',sortable: false,
                    render: function (data, type, row, meta) {
                            var result = '<center><a class="btn btn-warning btn-sm btn-pilih-item"\
                                data-id="'+row.id_item_variant+'"\
                                data-name="'+row.get_item_variant.get_items.name+' '+row.get_item_variant.size+'"\
                                data-stock="'+row.stock+'"\
                                 href="#"\
                            data-id="'+row.id+'"\
                            ><i class="mdi mdi-plus-box"></i></a></center>';
                            return result;
                     }
                },
        {data: 'get_items.name', name: 'get_items.name', orderable: false,searchable: true,
                render: function (data, type, row, meta) {
                    var result = row.get_item_variant.get_items.name+' '+row.get_item_variant.size;
                            return result;
                }

        },
        {data: 'stock', name: 'stock', orderable: false,searchable: false}
      ],
      responsive: true,
      columnDefs: [
          {
            // render:function(data){
            //     return moment(data).format('MM-DD-YYYY');
            // }, targets:[2],
          }
      ],
     // dom: 't',

  });
}

$('body').on('click', '.btn-pilih-item', function () {
    let id = $(this).data('id');
    let item_id = $(this).data('item_id');
    let name = $(this).data('name');
    let kdbrg = item_id +'-'+ id;
    let stock = $(this).data('stock');
    let id_warehouse = $("#warehouse_id").val();

    $('#id_brg').val(kdbrg);
    $('#barang').val(name);
    $('#stock').val(stock);
    $('#modalItems').modal('hide');

    loadStockCard(id, id_warehouse);

});

function loadStockCard(id,id_warehouse) {
    $('#tbl_stock_card tbody').empty();
        var page_url = "{{ route('stock.card.list.data') }}";
        $.ajax({
        url: page_url,
        type: 'GET',
        data: {
            id_item_variant : id,
            id_warehouse : id_warehouse
        },
        success: function(result) {
                for(i = 0; i < result.total; i++)
                {
                    currentdata = result.list[i];

					var tr ="\
                        <tr>\
                            <td>"+ currentdata.get_item_variant.get_items.name + ' ' +currentdata.get_item_variant.color+ ' ' +currentdata.get_item_variant.size+"</td>\
                            <td>"+ currentdata.stock_first +"</td>\
                            <td>"+ currentdata.stock_in +"</td>\
                            <td>"+ currentdata.stock_out +"</td>\
                            <td>"+ currentdata.stock_final +"</td>\
                            <td>"+ currentdata.notes +"</td>\
                            <td>"+ currentdata.created_at +"</td>\
                        </tr>\
                    ";


                    $('#tbl_stock_card tbody').append(tr);
                }
        }
      });
}

</script>
@endpush
