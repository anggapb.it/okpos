@extends('layouts.main')

@section('page.title', 'Billing')
{{-- @section('page.heading', 'Billing') --}}

@section('page.content')
<style>
    * {
 font-size: 12px;
 font-family: Arial;
}

.box-amount{
    background: dimgray;
    padding: 1px;
}

.card{
    margin-top: -30px;
}
</style>
<div class="col-12">
    <div class="card">

        <div class="card-body">
            @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                 {{Session::get('message')}}
            </div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                {{Session::get('error')}}
            </div>
        @endif

                <input type="hidden" id="salesId" name="salesId">
                <div class="form-row mt-1">
                    <label  class="col-sm-2">Scan Barang<span class="text-danger">*</span></label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm" id="cari_barang" name="cari_barang" autofocus>
                        {{-- <span id="demo" style="color: blue">Ready to scan!</span> --}}
                    </div>
                    <div class="col-sm-2">
                        <button type="button" class="btn waves-effect waves-light btn-sm btn-primary btn-add-items"><i class="mdi mdi-cart-plus"></i> </button>
                    </div>
                </div>
                <form id="frm_billing" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                <div class="form-row mt-1">
                    <label  class="col-sm-2">Customer<span class="text-danger">*</span></label>
                    <div class="col-sm-7">
                        <select class="form-control form-control form-control-sm" name="customer" id="customer" onchange="getDiscount(this.options[this.selectedIndex].getAttribute('discount'))" required>
                            <option value="">Pilih</option>
                                @foreach ($customer as $row)
                                    <option discount="{{ $row->discount }}" value="{{ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                          </select>
                    </div>
                </div>


                    <div class="table-responsive">
                        <table  id="tbl_variant" class="table table-bordered" style="width:100%">
                          <thead>
                            <tr>
                              <th class="align-middle" scope="col">Item</th>
                              <th class="align-middle" scope="col">Harga</th>
                              <th class="align-middle" scope="col">Qty</th>
                              <th class="align-middle" scope="col">Opsi</th>
                            </tr>
                          </thead>
                          <tbody id="tbody_variant">

                        </tbody>

                        </table>
                      </div>
                  <hr>
                  <div class="box-amount text-right" id="div-disc">
                    <h3 id="discount" style="color: white;">0</h3>
                    <input type="hidden" id="hidden_discount" name="discount">
                  </div>
                  <div class="box-amount text-right">

                      <h1 id="amount" style="color: white;">0</h1>
                      <input type="hidden" id="hidden_total" name="amount">
                  </div>
                  <br>
                  <button type="button" class="btn-sm waves-effect waves-light btn-info btn-submit" onclick="simpanData(event, 'tunai')">Tunai</button>
                  <button type="button" class="btn-sm waves-effect waves-light btn-primary btn-submit" onclick="simpanData(event, 'transfer')">Transfer</button>
                  <button type="button" class="btn-sm waves-effect waves-light btn-danger btn-submit" onclick="simpanData(event, 'bon')">Bon</button>
                  <button type="button" class="btn-sm waves-effect waves-light btn-warning" onClick="window.location.reload();">Refresh</button>

            </form>

        </div>
    </div>
</div>

<div class="modal fade" id="modalItems" tabindex="-1" role="dialog" aria-labelledby="modalPasienLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">Data Barang</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
              <div class="table-responsive">
                  <table class="table table-bordered" id='tbl_list_items' width="100%">
                  <thead>
                    <tr>
                      <th class="text-center">Aksi</th>
                      <th class="text-center">Item</th>
                      <th class="text-center">Stok</th>
                      <th style="width:20%;" class="text-center">Harga</th>
                    </tr>
                  </thead>
                </table>
              </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
  </div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
let paramDisc = 0;
$(document).ready(function(){
    document.getElementById("div-disc").style.visibility = "hidden";
    //set default value customer
    $("#customer").val(1);
    loadItems();

    $('#cari_barang').keypress(function(e) {
        if(e.which == 13) {
            let id = $("#cari_barang").val();
            let itemFound = false;
            const arrId = id.split("-");
            //const idWithoutLeading0 = parseInt(arrId[1], 10);



            $( ".group_id_item_id_variant" ).each(function( index ) {
                            if($( this ).val() == arrId[1]){
                                itemFound = true;
                                alert("Barang sudah di tambahkan");
                                $("#cari_barang").val("");
                            }
            });

            if(itemFound == false) {
                $.ajax({
                type: 'GET',
                url: "{{ url('billing/list-items-by-id') }}" + '/' + arrId[1],
                success: function (response) {
                    res = response.data;
                    if(response.success === 'true') {
                        // if(char>limit){
                        //       e.preventDefault();

                        // } else{
                            let name = res.get_item_variant.get_items.name+ ' '+res.get_item_variant.size;
                            let price = res.get_item_variant.price;
                            let stock = res.get_item_variant.stock;
                            let subtotal = parseInt(res.get_item_variant.price) * 1;
                            let idItemIdVariant = res.get_item_variant.id;

                            $('#tbody_variant').append(`<tr id="R${++rowIdx}">
                                <td class="row-index text-left item-name">
                                    ${name}
                                </td>
                                <td class="row-index text-right cprice">
                                    ${price}
                                </td>
                                <td style="width:18%;">
                                    <input type='number' class="form-control form-control-sm cqty" name="qty[]" value="1" autocomplete="off"/>
                                </td>

                                <td><a class='btn btn-danger btn-xs btn-delete-variant row-index'
                                    data-price=${price}
                                href='javascript:void(0);'>-</a>
                                <input type='hidden' class="group_id_item_id_variant" value="${idItemIdVariant}" />
                                <input type='hidden' name="id_item_variant[]" value="${arrId[1]}" />
                                <input type='hidden' name="harga[]" value="${price}" />
                                <input class="csubtotal" type='hidden' name="subtotal[]" value="${subtotal}" />
                                <input class="cstock" type='hidden' name="stock[]" value="${stock}" />
                                </td>

                            </tr>`);
                            calculateGrandTotal();
                            $("#cari_barang").val("");
                            // }

                            }

                        }
                    });
            }
        }
    });
});

function getDiscount(disc) {
    paramDisc = disc;
    calculateGrandTotal();
    if(parseInt(disc) > 0){
        document.getElementById("div-disc").style.visibility = "visible";
    } else {
        document.getElementById("div-disc").style.visibility = "hidden";
    }
}



$('body').on('click', '.btn-add-items', function () {
       $('#modalItems').modal('show');
});

function loadItems(){
    $("#tbl_list_items").dataTable().fnDestroy();

const page_url = "{{ route('billing.list-items') }}";

var table = $('#tbl_list_items').DataTable({
      processing: true,
      serverSide: true,
      ajax: {
          url: page_url,
          type: 'GET'
      },
      columns: [
        {data: 'id',sortable: false,
                    render: function (data, type, row, meta) {
                            var result = '<center><a class="btn btn-warning btn-sm btn-pilih-item"\
                                data-id="'+row.id_item_variant+'"\
                                data-name="'+row.get_item_variant.get_items.name+' '+row.get_item_variant.size+'"\
                                data-price="'+row.get_item_variant.price+'"\
                                data-stock="'+row.stock+'"\
                                data-idgroup="'+row.get_item_variant.id+'"\
                                 href="#"\
                            data-id="'+row.id_item_variant+'"\
                            ><i class="mdi mdi-plus-box"></i></a></center>';
                            return result;
                     }
                },
        {data: 'items', name: 'items'},
        {data: 'stock', name: 'stock', orderable: false,searchable: false},
        {data: 'get_item_variant.price', name: 'get_item_variant.price', orderable: false,searchable: false}
      ],
      responsive: true,
      columnDefs: [
          {
            // render:function(data){
            //     return moment(data).format('MM-DD-YYYY');
            // }, targets:[2],
          }
      ],
     // dom: 't',

  });
}

function formatNumber(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
}

var rowIdx = 0;
let total = 0;
$('body').on('click', '.btn-pilih-item', function () {
        var id = $(this).data('id');
        var name = $(this).data('name');
        var price = $(this).data('price');
        var stock = $(this).data('stock');
        let idItemIdVariant = $(this).data('idgroup');
        var subtotal = parseInt(price) * 1;

        let itemFound = false;

        $( ".group_id_item_id_variant" ).each(function( index ) {
                        if($( this ).val() == idItemIdVariant){
                            itemFound = true;
                            alert("Barang sudah di tambahkan");
                            return false;
                        }
        });

        if(itemFound == false) {
            $('#modalItems').modal('hide');
            $('#tbody_variant').append(`<tr id="R${++rowIdx}">
                <td class="row-index text-left item-name">
                    ${name}
                </td>
                <td class="row-index text-right cprice">
                    ${price}
                </td>
                <td style="width:18%;">
                    <input type='number' class="form-control form-control-sm cqty" name="qty[]" value="1" autocomplete="off"/>
                </td>

                <td><a class='btn btn-danger btn-xs btn-delete-variant row-index'
                    data-price=${price}
                href='javascript:void(0);'>-</a>
                <input type='hidden' class="group_id_item_id_variant" value="${idItemIdVariant}" />
                <input type='hidden' name="id_item_variant[]" value="${id}" />
                <input type='hidden' name="harga[]" value="${price}" />
                <input class="csubtotal" type='hidden' name="subtotal[]" value="${subtotal}" />
                <input class="cstock" type='hidden' name="stock[]" value="${stock}" />
                </td>

            </tr>`);
            calculateGrandTotal();
        }



});

$("#tbody_variant").delegate(".cqty", "input",function(e){
    var row = $(this).closest("tr");
    let index = $('.cqty').index(this);
    let price = $( ".cprice" ).eq( index ).text();
    let stock = row.find(".cstock").val();
    let subtotal = parseInt(price) * e.target.value;

    if(parseInt($(this).val()) > parseInt(stock)){
        Swal.fire(
                    'Qty tidak boleh melebihi stok!  ('+stock+')',
                    'error'
                )
        row.find(".cqty").val(1);
        subtotal = parseInt(price) * 1;
        row.find(".csubtotal").val(isNaN(subtotal) ? "" : subtotal);
        calculateGrandTotal();
    } else{
        row.find(".csubtotal").val(isNaN(subtotal) ? "" : subtotal);
        calculateGrandTotal();
    }



});

    $('#tbody_variant').on('click', '.btn-delete-variant', function () {
        $("#cari_barang").focus();
        var price = $(this).data('price');
        total = parseInt(total) - parseInt(price);
        var child = $(this).closest('tr').nextAll();
        child.each(function () {
          var id = $(this).attr('id');
          var dig = parseInt(id.substring(1));
          $(this).attr('id', `R${dig - 1}`);
        });
        $(this).closest('tr').remove();
        rowIdx--;
        calculateGrandTotal();

        //  const element = document.getElementById("amount");
        //  element.innerHTML = formatNumber(total);
        //     $("#hidden_total").val(total);
    });

    function calculateGrandTotal() {

        var total = 0;
        let countData = 1;
            $('[id*=tbl_variant] tbody tr').each(function () {
                total += parseFloat($(this).find('.csubtotal').val());
                const element = document.getElementById("amount");
                const elDiscount = document.getElementById("discount");
                //let discount = (total * parseInt(paramDisc)) / 100;
                let discount = paramDisc * countData;
                let grandTotal = total - parseInt(discount);

                element.innerHTML = formatNumber(grandTotal);
                elDiscount.innerHTML = 'Diskon : ' + formatNumber(discount);

                    $("#hidden_total").val(grandTotal);
                    $("#hidden_discount").val(discount);

                    countData++;
            });
    }


function simpanData(event, paymentMethod){
    event.preventDefault();

    var frm = $('#frm_billing');

        $.ajax({
            type: 'POST',
            url: "{{ route('billing.store') }}",
            data: frm.serialize() + "&payment_method=" + paymentMethod,
            success: function (data) {
                $(".btn-submit").hide();
               $('#tbl_variant tbody').empty();
               document.getElementById("div-disc").style.visibility = "hidden";
               const element = document.getElementById("amount");
                element.innerHTML = 0;

                let salesId = data.sales_id;
               // $("#salesId").val(salesId);
                printReceipt(salesId);
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },
        });
}



function printReceipt(salesId) {
    let id = $('#id_brg').val();
    let amount = $('#amount').val();
    let url = "{{ url('billing/print') }}" + '/' + salesId;
    let params = `scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,
width=300,height=500,left=100,top=100`;

        open(url, 'receipt', params);

}

</script>
@endpush
