
  <style>
  h2{
      font-size: 12px;
  }

  .info{
      font-size: 11px;
  }

  .itemtext{
      font-size: 11px;
  }

  .itemnumber{
      font-size: 11px;
      text-align: right;
  }

  hr{
    border-top: 1px dashed;
  }
  .logo{
    width: 100px;
    height: 100px;
  }


  </style>
  <div id="invoice-POS">

    <center id="top">
      <div class="logo">
        <img src="{{ asset('storage'.'/'.$profile->logo_image) }}" alt="" width="100" height="80">
      </div>
      <div class="info">
        <h2>{{ $profile->store_name }}</h2>
      </div><!--End Info-->
    </center><!--End InvoiceTop-->

    <div id="mid">
      <div class="info">
        <center>
            {{ $profile->address.' '.$profile->phone_number }}
        </center>
            <hr>
            <div style="text-align: right">
             {{ $sales->customer->name }}
            </div>
      </div>
    </div><!--End Invoice Mid-->
    <hr>

    <div id="bot">

					<div id="table">
						<table>
              @foreach ($salesDetail as $value)
              <tr class="tabletitle">
                  <td class="tableitem"><p class="itemtext">{{ $value->itemVariant->get_items->name.' '.$value->itemVariant->size.' '.$value->itemVariant->color }}</p></td>
                  <td class="tableitem"><p class="itemtext">{{ $value->qty }}</p></td>
                  <td class="tableitem"><p class="itemnumber">{{ number_format($value->price,0,",",".") }}</p></td>
                  <td class="tableitem"><p class="itemnumber">{{ number_format($value->subtotal,0,",",".") }}</p></td>
              </tr>
              @endforeach

							<tr class="tabletitle">
								<td colspan="4"><hr></td>
							</tr>
							<tr class="tabletitle">
								<td></td>
								<td></td>
								<td class="Rate"><p class="itemtext">Total</p></td>
								<td class="payment"><p class="itemnumber">{{ number_format((int) $sales->cost_amount + (int) $sales->discount,0,",",".")   }}</p></td>
							</tr>

                            <tr class="tabletitle">
								<td></td>
								<td></td>
								<td class="Rate"><p class="itemtext">Diskon</p></td>
								<td class="payment"><p class="itemnumber">{{ number_format($sales->discount,0,",",".") }}</p></td>
							</tr>

                            <tr class="tabletitle">
								<td></td>
								<td></td>
								<td class="Rate"><p class="itemtext">Bayar</p></td>
								<td class="payment"><p class="itemnumber">{{ number_format((int) $sales->cost_amount,0,",",".") }}</p></td>
							</tr>

                            <tr class="tabletitle">
								<td></td>
								<td></td>
								<td class="Rate"><p class="itemtext">Transaksi</p></td>
								<td class="payment"><p class="itemnumber">{{ strtoupper($sales->payment_method) }}</p></td>
							</tr>



						</table>
					</div><!--End Table-->
                    <hr>
					<div id="legalcopy">
						<center><p style="font-size: 12px">Terimakasih sudah belanja di tempat kami</p></center>

					</div>

				</div><!--End InvoiceBot-->
  </div><!--End Invoice-->

<script>
    print();
</script>
