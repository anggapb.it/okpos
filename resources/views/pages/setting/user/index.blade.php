@extends('layouts.main')

@section('page.title', 'User')
@section('page.heading', 'User')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

            <div class="row">
                <a href="{{ route('user.create') }}" class="btn btn-primary mt-4 ml-4">Tambah</a>
              </div>
              <div class="table-responsive">
                <table  id="tbl_user" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">No</th>
                      <th class="align-middle" scope="col">Nama</th>
                      <th class="align-middle" scope="col">Email</th>
                      <th class="align-middle" scope="col">Role</th>
                      <th class="align-middle" scope="col">Opsi</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>

    </div>
</div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){
    loadList();
$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
 });

});

function loadList(){
        const page_url = '{{ url('user/get-data') }}';

        var table = $('#tbl_user').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: page_url,
        },
        columns: [
        { "data": null,"sortable": false,
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        },
        {data: 'name', name: 'name'},
        {data: 'email', name: 'email'},
        {data: 'role', name: 'role'},
        { "data": null,"sortable": false,
            render: function (data, type, row, meta) {
                var result = '<a class="btn waves-effect waves-light btn-warning" href="{{ url('user/edit').'/' }}'+row.id+'">Ubah</a>';
                result += '&nbsp;<a class="btn waves-effect waves-light btn-danger" href="{{ url('user/hapus').'/' }}'+row.id+'">Hapus</a>';
                return result;
            }
        }

        ],
        responsive: true,
        oLanguage: {
            sLengthMenu: "_MENU_",
            sSearch: ""
        },
        aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
        order: [[1, "asc"]],
        pageLength: 10
    });
}
</script>
@endpush
