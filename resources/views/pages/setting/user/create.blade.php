@extends('layouts.main')

@section('page.title', 'Create User')
@section('page.heading', 'Create User')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
          <x-auth-validation-errors class="mb-4" :errors="$errors" />

            <form method="POST" action="{{ route('user.store') }}" id="frm_pp" class="form-horizontal" enctype="multipart/form-data">
                @csrf

                <div class="form-row mt-1">
                    <label  class="col-sm-2 text-right">Role</label>
                    <div class="col-md-5">
                        <select class="form-control form-control-sm" name="role" id="role" required>
                            <option value="">Pilih</option>
                                @foreach ($roles as $row)
                                    <option value="{{ $row->name }}">{{ $row->name }}</option>
                                @endforeach
                          </select>
                    </div>

                </div>
                <div class="form-row mt-1" id="div_warehouse">
                    <label  class="col-sm-2 text-right">Gudang</label>
                    <div class="col-md-5">
                        <select class="form-control form-control-sm select2" name="warehouse" id="warehouse">
                            <option value="">Pilih</option>
                                @foreach ($warehouse as $row)
                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                          </select>
                    </div>

                </div>
                <div class="form-row mt-1" id="div_name">
                    <label  class="col-sm-2 text-right">Nama</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="name" name="name" :value="old('name')" required>
                    </div>

                </div>
                <div class="form-row mt-1">
                    <label  class="col-sm-2 text-right">Email</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="email" name="email" :value="old('email')" required>
                    </div>

                </div>
                <div class="form-row mt-1">
                    <label  class="col-sm-2 text-right">Password</label>
                    <div class="col-md-10">
                        <input type="password" class="form-control" id="password" name="password" placeholder="minimal 8 karakter" required autocomplete="new-password">
                    </div>

                </div>
                <div class="form-row mt-1">
                    <label  class="col-sm-2 text-right">Confirm Password</label>
                    <div class="col-md-10">
                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
                    </div>

                </div>
                <br>
                <div class="button-group">
                    <button type="submit" class="btn waves-effect waves-light btn-info">Simpan</button>
                    <a href="{{ route('user') }}" class="btn waves-effect waves-light btn-warning">Kembali</a>
                </div>
            </form>


    </div>
</div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){

$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
 });
//$('.select2').select2({ width: '100%',allowClear: true, placeholder :'--Pilih--' });
$("#div_warehouse").hide();
});

$("#role").change(function() {
    var role = this.value;
    if(role == 'kasir'){
        $("#div_warehouse").show();
     //   $("#div_name").hide();
    }else{
        $("#div_warehouse").hide();
       // $("#div_name").show();
    }
});

// $("#warehouse").change(function() {
//     $("#name").val(this.value);
// });

</script>
@endpush
