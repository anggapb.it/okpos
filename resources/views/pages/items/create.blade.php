@extends('layouts.main')

@section('page.title', 'Tambah Barang')
@section('page.heading', 'Tambah Barang')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <form method="POST" action="{{ route('items.store') }}" id="frm_barang" class="form-horizontal" enctype="multipart/form-data">
                @csrf

                <div class="button-group">
                    <button type="button" class="btn waves-effect waves-light btn-info" onclick="simpanData()">Simpan</button>
                    <a href="{{ url()->previous() }}" class="btn waves-effect waves-light btn-warning">Kembali</a>
                </div>
                <hr>

                <div class="form-row mt-1">
                    <label  class="col-sm-2 text-right">Kategori Barang</label><span class="text-danger">*</span>
                    <div class="col-md-5">
                        <select class="form-control form-control select2" name="kategori_barang" id="kategori_barang">
                            <option value="">Pilih</option>
                                @foreach ($kategori_barang as $row)
                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                          </select>
                    </div>

                </div>
                <div class="form-row mt-1">
                    <label  class="col-sm-2 text-right">Nama Barang<span class="text-danger">*</span></label>
                    <div class="col-md-7">
                        <input type="text" class="form-control" id="nama_barang" name="nama_barang">
                    </div>
                </div>

                <br>
                <div class="form-row mt-1">
                    <div class="col-md-1">
                        <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-add-variant">Tambah Varian</button>
                    </div>
                </div>
                    <div class="table-responsive">
                        <table  id="tbl_variant" class="table table-bordered" style="width:100%">
                          <thead>
                            <tr>
                              <th class="align-middle" scope="col">Ukuran <span class="text-danger">*</span></th>
                              <th class="align-middle" scope="col">Harga <span class="text-danger">*</span></th>
                              {{-- <th class="align-middle" scope="col">Qty <span class="text-danger">*</span></th> --}}
                              <th class="align-middle" scope="col">Opsi</th>
                            </tr>
                          </thead>
                          <tbody id="tbody_variant">
                            <tr id="0">
                                <td class="row-index text-left">
                                    <input type='text' class="form-control" name="ukuran[]" id="ukuran0"/>
                                </td>
                                <td class="row-index text-left">
                                    <input type='text' class="form-control" name="harga[]" value="0"/>
                                </td>
                                {{-- <td class="row-index text-left">
                                    <input type='text' class="form-control" name="qty[]" value="1"/>
                                </td> --}}

                                <td>
                                <input type='hidden' name="idvariant[]" id="idvariant0" />
                                </td>

                               </tr>
                        </tbody>

                        </table>
                      </div>

            </form>

        </div>
    </div>
</div>
@endsection

@push('bottom.scripts')
<script type="text/javascript">

var rowIdx = 0;
    $('body').on('click', '.btn-add-variant', function () {
        $('#tbody_variant').append(`<tr id="R${++rowIdx}">
            <td class="row-index text-left">
                <input type='text' class="form-control" name="ukuran[]"/>
            </td>
            <td class="row-index text-left">
                <input type='text' class="form-control" name="harga[]"/>
            </td>

            <td><a class='btn btn-danger btn-sm btn-delete-variant row-index'
            href='javascript:void(0);'>Hapus</a>
            <input type='hidden' name="idvariant[]" id="idvariant${rowIdx}" />
            </td>

           </tr>`);
           loadObat(rowIdx);
    });

    $('#tbody_variant').on('click', '.btn-delete-variant', function () {
        var child = $(this).closest('tr').nextAll();
        child.each(function () {
          var id = $(this).attr('id');
          var dig = parseInt(id.substring(1));
          $(this).attr('id', `R${dig - 1}`);
        });
        $(this).closest('tr').remove();
        rowIdx--;
    });



function simpanData()
{
    if($("#nama_barang").val() == ''){
        Swal.fire("Error!", "Nama barang harus diisi", "error");
    }else if($("#kategori_barang").val() == ''){
        Swal.fire("Error!", "Kategori Barang harus diisi", "error");
    }else if($("#ukuran0").val() == ''){
        Swal.fire("Error!", "Ukuran harus diisi", "error");
    }
    else{
        $("#frm_barang").submit();
    }

}

</script>
@endpush
