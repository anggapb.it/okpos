@extends('layouts.main')

@section('page.title', 'Barang')
@section('page.heading', 'Barang')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                 {{Session::get('message')}}
            </div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                {{Session::get('error')}}
            </div>
        @endif
            <div class="row">
                <a href="{{ route('items.create') }}" class="btn btn-primary mt-4 ml-4">Tambah</a>
                <div class="table-responsive">
                    <table class="table table-bordered" id='tbl_items' width="100%">
                    <thead>
                      <tr>
                        <th class="text-center">Aksi</th>
                        <th class="text-center">Info Barang</th>
                        <th class="text-center">Variant</th>
                        <th class="text-center">Stok</th>

                      </tr>
                    </thead>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){

    loadData();
});

 function loadData()
 {
    $("#tbl_items").dataTable().fnDestroy();

const page_url = "{{ route('items.get-data') }}";

var table = $('#tbl_items').DataTable({
      processing: true,
      serverSide: true,
      ajax: {
          url: page_url,
          type: 'GET'
      },
      columns: [
        { "data": null,"sortable": false,
            render: function (data, type, row, meta) {
                var result = '<a class="btn-sm waves-effect waves-light btn-warning" href="{{ url('items/edit').'/' }}'+row.id+'">Ubah</a>';
                result += '&nbsp;<a class="btn-sm waves-effect waves-light btn-danger" href="{{ url('items/hapus').'/' }}'+row.id+'">Hapus</a>';

                return result;
            }
        },
        {data: 'info_items', name: 'info_items', orderable: false,searchable: true},
        {data: 'variant', name: 'variant', orderable: false,searchable: true},
        {data: 'stock', name: 'stock', orderable: false,searchable: false},

      ],
      responsive: true,
      columnDefs: [
          {

          }
      ],
     // dom: 't',

  });
 }
</script>
@endpush
