<html>
    <style>
        table, th, td {
            border: 1px solid black;
        }
    </style>


    <p>Laporan Customer</p>
<p>Periode {{ $dateFirst }} sd {{ $dateEnd }}</p>
<p>&nbsp;</p>
<table>
    <thead>
    <tr>
        <th>Tanggal</th>
        <th>Nama Customer</th>
        <th>Nomor Telepon</th>
        <th>Alamat</th>
        <th>Diskon</th>
    </tr>
    </thead>
    <tbody>
    @foreach($customer as $val)
        <tr>
            <td>{{ $val->created_at }}</td>
            <td>{{ $val->name }}</td>
            <td>{{ $val->phone_number }}</td>
            <td>{{ $val->address }}</td>
            <td>{{ $val->discount }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

</html>

