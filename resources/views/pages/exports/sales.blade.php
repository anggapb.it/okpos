<html>
    <style>
        table, th, td {
            border: 1px solid black;
        }
    </style>


    <p>Laporan Penjualan</p>
<p>Periode {{ $dateFirst }} sd {{ $dateEnd }}</p>
<p>&nbsp;</p>
<table>
    <thead>
    <tr>
        <th>Tanggal</th>
        <th>Customer</th>
        <th>Sub Total</th>
        <th>Diskon</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
    @foreach($sales as $val)
        <tr>
            <td>{{ $val->created_at }}</td>
            <td>{{ $val->customer->name }}</td>
            <td>{{ $val->cost_amount }}</td>
            <td>{{ $val->discount }}</td>
            <td>{{ $val->total }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

</html>

