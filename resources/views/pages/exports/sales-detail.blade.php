<html>
    <style>
        table, th, td {
            border: 1px solid black;
        }
    </style>


    <p>Laporan Penjualan</p>
<p>Periode {{ $dateFirst }} sd {{ $dateEnd }}</p>
<p>&nbsp;</p>
<table>
    <thead>
    <tr>
        <th>Tanggal</th>
        <th>Customer</th>
        <th>Item</th>
        <th>Quantity</th>
        <th>Harga</th>
        <th>Sub Total</th>
    </tr>
    </thead>
    <tbody>
    @foreach($salesDetail as $val)
        <tr>
            <td>{{ $val->created_at }}</td>
            <td>{{ $val->customers_name }}</td>
            <td>{{ $val->items_name . ' ' .$val->size . ' ' . $val->color }}</td>
            <td>{{ $val->qty }}</td>
            <td>{{ $val->price }}</td>
            <td>{{ $val->subtotal }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

</html>

