<html>
    <style>
        table, th, td {
            border: 1px solid black;
        }
    </style>
    

    <p>Laporan Barang</p>
    @if (isset($dataFirst, $dataEnd))
<p>Periode {{ $dateFirst }} sd {{ $dateEnd }}</p>        
    @endif
<p>Periode -</p>
<p>&nbsp;</p>
<table>
    <thead>
    <tr>
        <th>Barang</th>
        <th>Deskripsi</th>
        <th>Ukuran </th>
        <th>Stock</th>
        <th>Harga</th>
    </tr>
    </thead>
    <tbody>
    @foreach($items as $val)
        <tr>
            <td>{{ $val->get_items->name.' '.$val->color.' '.$val->size }}</td>
            <td>{{ $val->get_items->description }}</td>
            <td>{{ $val->size }}</td>
            <td>{{ $val->stock }}</td>
            <td>{{ $val->price }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

</html>

