@extends('layouts.main')

@section('page.title', 'Data Profil')
@section('page.heading', 'Data Profil')

@section('page.content')
<style>
    * {
 font-size: 12px;
 font-family: Arial;
}

.box-amount{
    background: dimgray;
    padding: 1px;
}

.card{
    margin-top: -30px;
}
</style>
<div class="col-12">
    <div class="card">

        <div class="card-body">
          @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                 {{Session::get('message')}}
            </div>
          @endif
          @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                {{Session::get('error')}}
            </div>
          @endif
        </div>
          <form method="post" id="formProfile" action="{{route('profile.update')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id">
            <div class="col-10 mb-3">
              {{-- <input type="hidden" class="form-control" id="id" name="id"> --}}
              <div class="mb-4 row">
                <label for="nama_toko" class="col-sm-2 col-form-label">Nama Toko</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="nama_toko" name="store_name" placeholder="Nama Toko">
                </div>
              </div>
              <div class="mb-4 row">
                <label for="address" class="col-sm-2 col-form-label">Alamat</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="address" name="address" placeholder="Alamat Toko">
                </div>
              </div>
              <div class="mb-4 row">
                <label for="phone_number" class="col-sm-2 col-form-label">Nomor Telepon</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Nomor Telepon">
                </div>
              </div>
              <div class="mb-4 row">
                <label for="logo" class="col-sm-2 col-form-label">Logo</label>
                <div class="col-sm-10">
                  <input type="hidden" id="old_logo_image" name="old_logo_image" @if (isset($profile)) value="{{$profile->logo_image}}" @endif>
                  <input type="file" class="form-control-file" id="logo_image" name="logo_image">
                </div>
              </div>
              <div class="mb-4 row">
                <div class="col-sm-10">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </div>
            </div>
          </form>

    </div>
</div>


@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){
    loadItems();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
});

function loadItems(){
  const page_url = "{{ url('profile/get-data') }}";
  $.ajax({
    type: 'GET',
    url: page_url,
    success: function (response) {
      res = response.data;
      let nama_toko = res[0].store_name;
      let alamat = res[0].address;
      let noHp = res[0].phone_number;
      let logo = res[0].logo_image;
      console.log(logo);
      $('#nama_toko').val(nama_toko);
      $('#address').val(alamat);
      $('#phone_number').val(noHp);
      $('#old_logo_image').val(logo);
    }
  });
}
</script>
@endpush

