@extends('layouts.main')

@section('page.title', 'Edit Profile')
@section('page.heading', 'Edit Profile')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
          <x-auth-validation-errors class="mb-4" :errors="$errors" />

            <form method="POST" action="{{ route('profile.update') }}" id="frm_pp" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <input type="hidden" class="form-control" id="id" name="id" value="{{ $profile->id }}">


                <div class="form-row mt-1" id="div_name">
                    <label  class="col-sm-2">Nama Toko</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="store_name" name="store_name" value="{{ $profile->store_name }}" required>
                    </div>
                </div>

                <div class="form-row mt-1">
                    <label  class="col-sm-2">Alamat</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="address" name="address" value="{{ $profile->address }}" required>
                    </div>

                </div>

                <div class="form-row mt-1">
                    <label  class="col-sm-2">No. Telp</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="phone_number" name="phone_number" value="{{ $profile->phone_number }}" required>
                    </div>

                </div>
                <div class="form-row mt-1">
                    <label  class="col-sm-2">Logo</label>
                    <div class="col-md-10">
                        <input class="w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 cursor-pointer dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400" id="logo_images" name="logo_image" type="file">
                    </div>

                </div>
                <br>
                <div class="button-group">
                    <button type="submit" class="btn waves-effect waves-light btn-info">Update</button>
                </div>
            </form>


    </div>
</div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){
    $('.btn-reset-password').on('click', function () {
       $('#modalItems').modal('show');
    });
});
$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
 });


</script>
@endpush
