@extends('layouts.main')

@section('page.title', 'Cek Stock Barang')
@section('page.heading', 'Cek Stock Barang')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
          <x-auth-validation-errors class="mb-4" :errors="$errors" />

                <div class="form-row mt-1">
                    <label  class="col-sm-2">Barang<span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input type="hidden" id="id_brg" name="id_brg">
                        <input type="text" class="form-control form-control-sm" id="barang" name="barang">
                    </div>
                    <div class="col-sm-2">
                        <button type="button" class="btn waves-effect waves-light btn-sm btn-primary btn-add-items"><i class="mdi mdi-cart-plus"></i> </button>
                    </div>
                </div>

                <hr>
                <p id="resultStock" style="color: blue"></p>



    </div>
</div>

<div class="modal fade" id="modalItems" tabindex="-1" role="dialog" aria-labelledby="modalPasienLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">Data Barang</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
              <div class="table-responsive">
                  <table class="table table-bordered" id='tbl_list_items' width="100%">
                  <thead>
                    <tr>
                      <th class="text-center">Aksi</th>
                      <th class="text-center">Item</th>
                    </tr>
                  </thead>
                </table>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>
      </div>
    </div>
  </div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){
    loadItems();
 $('body').on('click', '.btn-add-items', function () {
       $('#modalItems').modal('show');
});

});

function loadItems(){
    $("#tbl_list_items").dataTable().fnDestroy();

const page_url = "{{ route('check.stock.list.items') }}";

var table = $('#tbl_list_items').DataTable({
      processing: true,
      serverSide: true,
      ajax: {
          url: page_url,
          type: 'GET'
      },
      columns: [
        {data: 'id',sortable: false,
                    render: function (data, type, row, meta) {
                            var result = '<center><a class="btn btn-warning btn-sm btn-pilih-item"\
                                data-id="'+row.id+'"\
                                data-item_id="'+row.get_items.id+'"\
                                data-name="'+row.get_items.name+' '+row.size+'"\
                                data-price="'+row.price+'"\
                                data-warehouse="'+row.warehouse+'"\
                                data-stock="'+row.stock+'"\
                                data-stock_warehouse="'+row.stock_warehouse+'"\
                                 href="#"\
                            data-id="'+row.id+'"\
                            ><i class="mdi mdi-plus-box"></i></a></center>';
                            return result;
                     }
                },
        {data: 'get_items.name', name: 'get_items.name', orderable: false,searchable: true,
                render: function (data, type, row, meta) {
                    var result = row.get_items.name+' '+row.size;
                            return result;
                }

        }
      ],
      responsive: true,
      columnDefs: [
          {
            // render:function(data){
            //     return moment(data).format('MM-DD-YYYY');
            // }, targets:[2],
          }
      ],
     // dom: 't',

  });
}

$('body').on('click', '.btn-pilih-item', function () {
    let id = $(this).data('id');
    let item_id = $(this).data('item_id');
    let name = $(this).data('name');
    let kdbrg = item_id +'-'+ id;
    let warehouse = $(this).data('warehouse');
    let stock_warehouse = $(this).data('stock_warehouse');
    let stock = $(this).data('stock');

    $('#id_brg').val(kdbrg);
    $('#barang').val(name);
    $('#modalItems').modal('hide');

        const element = document.getElementById("resultStock");
          element.innerHTML = warehouse + '<br>' + 'Total : ' +stock_warehouse;

});

</script>
@endpush
