@extends('layouts.main')

@section('page.title', 'Stok Masuk')
@section('page.heading', 'Stok Masuk')

@section('page.content')
<style>
    * {
 font-size: 12px;
 font-family: Arial;
}

.box-amount{
    background: dimgray;
    padding: 1px;
}

.card{
    margin-top: -30px;
}
</style>
<div class="col-12">
    <div class="card">

        <div class="card-body">
            @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                 {{Session::get('message')}}
            </div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                {{Session::get('error')}}
            </div>
        @endif

        <div class="form-row mt-1">
            <label  class="col-sm-2">Cari Barang<span class="text-danger">*</span></label>
            <div class="col-sm-2">
              <input type="text" class="form-control form-control-sm" id="cari_barang" name="cari_barang" autofocus>
              {{-- <span id="demo" style="color: blue">Ready to scan!</span> --}}
            </div>
            <div class="col-sm-2">
                <button type="button" class="btn waves-effect waves-light btn-sm btn-primary btn-add-items"><i class="mdi mdi-cart-plus"></i> </button>
            </div>
        </div>

            <form method="POST" action="{{ route('stockin.store') }}" id="frm_stockin" class="form-horizontal" enctype="multipart/form-data">
                @csrf


                <div class="form-row mt-1">
                    <label  class="col-sm-2">Lokasi<span class="text-danger">*</span></label>
                    <div class="col-sm-7">
                        <select class="form-control form-control form-control-sm" name="warehouse_id" id="warehouse_id">
                            <option value="">Pilih</option>
                                @foreach ($warehouse as $id => $name)
                                    <option value="{{ $id }}">{{ $name }}</option>
                                @endforeach
                          </select>
                    </div>
                </div>



                    <div class="table-responsive">
                        <table  id="tbl_variant" class="table table-bordered" style="width:100%">
                          <thead>
                            <tr>
                              <th class="align-middle" scope="col">Item</th>
                              <th class="align-middle" scope="col">Stok Masuk</th>
                              <th class="align-middle" scope="col">Opsi</th>
                            </tr>
                          </thead>
                          <tbody id="tbody_variant">

                        </tbody>

                        </table>
                      </div>
                  <hr>
                  <button type="submit" class="btn-sm waves-effect waves-light btn-info" onclick="simpanData(event)">Submit</button>
                  <button type="button" class="btn-sm waves-effect waves-light btn-warning" onClick="window.location.reload();">Refresh</button>

            </form>

        </div>
    </div>
</div>

<div class="modal fade" id="modalItems" tabindex="-1" role="dialog" aria-labelledby="modalPasienLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">Data Barang</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
              <div class="table-responsive">
                  <table class="table table-bordered" id='tbl_list_items' width="100%">
                  <thead>
                    <tr>
                      <th class="text-center">Aksi</th>
                      <th class="text-center">Item</th>
                      <th class="text-center">Stok</th>

                    </tr>
                  </thead>
                </table>
              </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
  </div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
var rowIdx = 0;
let total = 0;
$(document).ready(function(){
   // loadItems();
   $('#cari_barang').keypress(function(e) {
        if(e.which == 13) {
            let id = $("#cari_barang").val();
            let itemFound = false;
        let arrId = id.split("-");
        let warehouse = $('#warehouse_id option:selected').val();
        // document.getElementById("demo").innerHTML = "Kode: " + id;
        $( ".id_item_variant" ).each(function( index ) {
            if($( this ).val() == arrId[1]){
                itemFound = true;
                alert("Barang sudah di tambahkan");
                $("#cari_barang").val("");
            }
        });
        //  let char = elem.value.length;
        if(itemFound == false) {
            $.ajax({
            type: 'GET',
            url: "{{ url('mutation/list-items-by-id') }}" + '/' + arrId[1] + '/' + warehouse,
            success: function (response) {
                res = response.data;
                if(response.success === 'true') {
                    let name = res.get_item_variant.get_items.name+ ' '+res.get_item_variant.size;
                    let stock = res.stock;
                    let id = res.get_item_variant.id;

                    total = parseInt(total) + parseInt(stock);
                    $('#tbody_variant').append(
                    `<tr id="R${++rowIdx}">
                        <td class="row-index text-left item-name">
                            ${name}
                        </td>
                        <td style="width:18%;">
                            <input type='number' class="form-control form-control-sm" name="stock_in[]" value="1"/>
                        </td>

                        <td><a class='btn btn-danger btn-xs btn-delete-variant row-index'
                        href='javascript:void(0);'>-</a>
                        <input type='hidden' name="id_item_variant[]" class="id_item_variant" value="${id}" />
                        <input type='hidden' name="stock[]" value="${stock}" />
                        </td>

                    </tr>`
                    );
                    $("#cari_barang").val("");

                }
            }
            });
            }
        }

    });
});

$("#warehouse_id").change(function () {
        let wh = this.value;
        loadItems(wh);
});

$('body').on('click', '.btn-add-items', function () {
       $('#modalItems').modal('show');
});

function loadItems(wh){
    $("#tbl_list_items").dataTable().fnDestroy();

const page_url = "{{ url('stockin/list-items') }}" + '/' +wh;

var table = $('#tbl_list_items').DataTable({
      processing: true,
      serverSide: true,
      ajax: {
          url: page_url,
          type: 'GET'
      },
      columns: [
        {data: 'id',sortable: false,
                    render: function (data, type, row, meta) {
                            var result = '<center><a class="btn btn-warning btn-sm btn-pilih-item"\
                                data-id="'+row.id_item_variant+'"\
                                data-name="'+row.get_item_variant.get_items.name+' '+row.get_item_variant.size+'"\
                                data-stock="'+row.stock+'"\
                                 href="#"\
                            data-id="'+row.id+'"\
                            ><i class="mdi mdi-plus-box"></i></a></center>';
                            return result;
                     }
                },
        {data: 'items', name: 'items', orderable: false,searchable: true
        },
        {data: 'stock', name: 'stock', orderable: false,searchable: false}
      ],
      responsive: true,
      columnDefs: [
          {
            // render:function(data){
            //     return moment(data).format('MM-DD-YYYY');
            // }, targets:[2],
          }
      ],
     // dom: 't',

  });
}

function formatNumber(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
}

$('body').on('click', '.btn-pilih-item', function () {
        var id = $(this).data('id');
        var name = $(this).data('name');
        var stock = $(this).data('stock');

         total = parseInt(total) + parseInt(stock);
        $('#modalItems').modal('hide');
        $('#tbody_variant').append(`<tr id="R${++rowIdx}">
            <td class="row-index text-left item-name">
                ${name}
            </td>
            <td style="width:18%;">
                <input type='number' class="form-control form-control-sm" name="stock_in[]" value="1"/>
            </td>

            <td><a class='btn btn-danger btn-xs btn-delete-variant row-index'
            href='javascript:void(0);'>-</a>
            <input type='hidden' name="id_item_variant[]" value="${id}" />
            <input type='hidden' name="stock[]" value="${stock}" />
            </td>

           </tr>`);

});


    $('#tbody_variant').on('click', '.btn-delete-variant', function () {
        var child = $(this).closest('tr').nextAll();
        child.each(function () {
          var id = $(this).attr('id');
          var dig = parseInt(id.substring(1));
          $(this).attr('id', `R${dig - 1}`);
        });
        $(this).closest('tr').remove();
        rowIdx--;

    });

function simpanData(event){
    event.preventDefault();
    var frm = $('#frm_stockin');

        $.ajax({
            type: 'POST',
            url: "{{ route('stockin.store') }}",
            data: frm.serialize(),
            success: function (data) {
                Swal.fire(
                    'Simpan Data Berhasil!',
                    'success'
                )
                window.location.reload();
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },
        });
}


</script>
@endpush

