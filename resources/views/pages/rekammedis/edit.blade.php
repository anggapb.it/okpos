@extends('layouts.main')

@section('page.title', 'Rekam Medis SOAP')
@section('page.heading', 'Rekam Medis SOAP')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <div class="runtext-container">
                <div class="main-runtext">
                <marquee direction="" onmouseover="this.stop();" onmouseout="this.start();">

                <div class="holder">
                     <div class="text-container">
                  Jika pasien lama dan rekam medis SOAP kunjungan ini belum di input. Maka data Subyektif (S), Assesment (A) dan Planning (P) secara default akan menampilkan data history rekam medis SOAP yang sebelumnya.
                    </div>

                </div>

                </marquee>
                </div>
                </div>
            <form method="POST" action="{{ route('rekam-medis.update') }}" id="frm_rk_medis" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="button-group">
                    <button type="button" class="btn waves-effect waves-light btn-info" onclick="updateData()">Submit</button>
                    <button type="button" class="btn waves-effect waves-light btn-warning" onclick="templateSoap()">Gunakan Template SOAP</button>
                    <a href="{{ route('rekam-medis') }}" class="btn waves-effect waves-light btn-danger">Kembali</a>
                    <input type="hidden" class="form-control form-control-sm" autocomplete="off" id="id_template_soap" name="id_template_soap" value="{{ $rkmedis->id_template_soap }}">
                    <input type="hidden" class="form-control form-control-sm" autocomplete="off" id="id_rekam_medis_soap" name="id_rekam_medis_soap" value="{{ $rkmedis->id }}">
                    <input type="hidden" class="form-control form-control-sm" autocomplete="off" id="iddokter" name="iddokter" value="{{ $reg->iddokter }}">
                    <input type="hidden" class="form-control form-control-sm" autocomplete="off" id="idregdet" name="idregdet" value="{{ $reg->idregdet }}">
                    <input type="hidden" class="form-control form-control-sm" autocomplete="off" id="idbagian" name="idbagian" value="{{ $reg->idbagian }}">
                    <input type="hidden" class="form-control form-control-sm" autocomplete="off" id="idshift" name="idshift" value="{{ $reg->idshift }}">

                    </div>
                    <div class="form-row mt-1">
                        <label class="col-md-1 text-right">No RM</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control form-control-sm" autocomplete="off" id="norm" name="norm" value="{{ $reg->get_registrasi->norm }}" readOnly>
                          </div>
                          <label class="col-md-1 text-right">No. Reg</label>
                        <div class="col-md-2">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="jkelamin" name="jkelamin" readOnly value="{{ $reg->noreg }}">
                        </div>
                          <label class="col-md-1 text-right">Pasien</label>
                          <div class="col-md-4">
                            <input type="text" class="form-control form-control-sm" autocomplete="off" id="nmpasien" name="nmpasien" value="{{ $reg->get_registrasi->get_pasien->nmpasien }}" readOnly>
                          </div>


                    </div>
                 <div class="form-row mt-1">

                        <label class="col-md-1 text-right">Tgl. Input</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control form-control-sm" autocomplete="off" id="tglinput" name="tglinput" value="{{ $rkmedis->tgl_rekam_medis }}">
                          </div>
                        <label class="col-md-1 text-right">Tgl. Reg</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control form-control-sm" autocomplete="off" id="tgllahir" name="tgllahir" readOnly value="{{ $reg->tglreg }}">
                          </div>
                        <label class="col-md-1 text-right">dokter</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control form-control-sm" autocomplete="off" id="dokter" name="dokter" readOnly value="{{ $reg->get_dokter->nmdoktergelar }}">
                          </div>


                    </div>
                    <hr>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#subyektif" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Subyektif (S)</span></a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#obyektif" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Obyektif (O)</span></a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#assesment" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Assesment (A)</span></a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#planning" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Planning (P)</span></a> </li>
                    </ul>
                    <div class="tab-content tabcontent-border">
                        <div class="tab-pane active" id="subyektif" role="tabpanel">
                            <div class="p-20">
                                <div class="form-row mt-1">

                                      <div class="col-md-1">
                                          <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-subyektif">Tambahkan Keluhan</button>
                                          </div>
                                </div>
                                <div class="table-responsive">
                                    <table  id="tbl_subyektif" class="table table-bordered" style="width:100%">
                                      <thead>
                                        <tr>
                                          <th class="align-middle" scope="col">Keluhan</th>
                                          <th class="align-middle" scope="col">Catatan</th>
                                          <th class="align-middle" scope="col">Opsi</th>
                                        </tr>
                                      </thead>
                                      <tbody id="tbody_subyektif">

                                    </tbody>

                                    </table>
                                  </div>
                            </div>
                        </div>
                        <div class="tab-pane  p-20" id="obyektif" role="tabpanel">
                            <h6><strong>Tanda - tanda vital</strong></h6>
                            <div class="form-row mt-1">
                                <label class="col-md-2 text-right">Kesadaran</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="kesadaran" name="kesadaran">
                                  </div>
                                  <label class="col-md-1 text-right">Nadi</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="nadi" name="nadi">
                                  </div>
                                  <label class="col-md-2 text-right">Tek. Darah</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="tekanan_darah" name="tekanan_darah">
                                  </div>

                            </div>
                            <div class="form-row mt-1">
                                <label class="col-md-2 text-right">Frek. Nafas</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="frek_nafas" name="frek_nafas">
                                  </div>
                                  <label class="col-md-1 text-right">Suhu</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="suhu" name="suhu">
                                  </div>
                                  <label class="col-md-2 text-right">SPO2</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="spo2" name="spo2">
                                  </div>
                            </div>
                            <h6><strong>Pemeriksaan Fisik</strong></h6>
                            <div class="form-row mt-1">
                                <label class="col-md-2 text-right">TB</label>
                                <div class="col-md-1">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="tb" name="tb">
                                  </div>
                                  <label class="col-md-1 text-right">BB</label>
                                  <div class="col-md-1">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="bb" name="bb">
                                  </div>
                                  <label class="col-md-1 text-right">IMT</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="imt" name="imt">
                                  </div>
                            </div>
                            <div class="form-row mt-1">
                                <label class="col-md-2 text-right">Kepala</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="kepala" name="kepala">
                                  </div>
                                  <label class="col-md-2 text-right">Mata</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="mata" name="mata">
                                  </div>
                                  <label class="col-md-1 text-right">Telinga</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="telinga" name="telinga">
                                  </div>
                            </div>
                            <div class="form-row mt-1">
                                <label class="col-md-2 text-right">Hidung</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="hidung" name="hidung">
                                  </div>
                                  <label class="col-md-2 text-right">Mulut dan faring</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="mulut_faring" name="mulut_faring">
                                  </div>
                                  <label class="col-md-1 text-right">Leher</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="leher" name="leher">
                                  </div>
                            </div>
                            <h6>&nbsp;&nbsp;&nbsp;Thoraks</h6>
                            <div class="form-row mt-1">
                                <label class="col-md-2 text-right">Paru</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="paru" name="paru">
                                  </div>
                                  <label class="col-md-1 text-right">Jantung</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="jantung" name="jantung">
                                  </div>
                            </div>
                            <div class="form-row mt-1">
                                <label class="col-md-2 text-right">Abdomen</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="abdomen" name="abdomen">
                                  </div>
                                  <label class="col-md-2 text-right">Genitalia</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="genitalia" name="genitalia">
                                  </div>

                            </div>
                            <div class="form-row mt-1">
                                <label class="col-md-2 text-right">Extremitas</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="extremitas" name="extremitas">
                                  </div>

                                  <label class="col-md-2 text-right">Rectum / Anal</label>
                                  <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="rectum" name="rectum">
                                  </div>
                            </div>
                            <br>
                            <h6><strong>Pemeriksaan Penunjang</strong></h6>
                            <div class="form-row mt-1">
                                <div class="col-md-1">
                                    <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-pemeriksaan_penunjang">Tambah</button>
                                    </div>
                          </div>
                          <div class="table-responsive">
                              <table  id="tbl_pemeriksaan_penunjang" class="table table-bordered" style="width:100%">
                                <thead>
                                  <tr>
                                    <th class="align-middle" scope="col">Catatan</th>
                                    <th class="align-middle" scope="col">File</th>
                                    <th class="align-middle" scope="col">Catatan</th>
                                    <th class="align-middle" scope="col">Opsi</th>
                                  </tr>
                                </thead>
                                <tbody id="tbody_pemeriksaan_penunjang">

                                </tbody>
                              </table>
                            </div>
                        </div>
                        <div class="tab-pane p-20" id="assesment" role="tabpanel">
                            <div class="form-row mt-1">

                                  <div class="col-md-1">
                                      <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-diagnosa">Tambahkan Diagnosa</button>
                                      </div>
                            </div>
                            <div class="table-responsive">
                                <table  id="tbl_diagnosa" class="table table-bordered" style="width:100%">
                                  <thead>
                                    <tr>
                                      <th class="align-middle" scope="col">Id</th>
                                      <th class="align-middle" scope="col">Nama</th>
                                      <th class="align-middle" scope="col">Catatan</th>
                                      <th class="align-middle" scope="col">Opsi</th>
                                    </tr>
                                  </thead>
                                  <tbody id="tbody_diagnosa">

                                </tbody>
                                </table>
                              </div>
                        </div>
                        <div class="tab-pane p-20" id="planning" role="tabpanel">
                            <div class="form-row mt-1">
                                <label class="col-md-1 text-right">ITER</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="iter" name="iter" value="{{ $rkmedis->iter }}">
                                </div>
                            </div>
                            <hr>


                            <div class="table-responsive">
                              <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-obat">Tambahkan Obat Non Racik </button>
                              <button type="button" class="btn waves-effect waves-light btn-sm btn-warning btn-obat-racikan">Tambahkan Obat Racikan</button>

                                <table  id="tbl_obat" class="table table-bordered" style="width:100%">
                                  <thead>
                                    <tr>
                                      <th class="align-middle" scope="col">Obat</th>
                                      <th class="align-middle" scope="col">Qty</th>
                                      <th class="align-middle" scope="col">Aturan Pakai</th>
                                      <th class="align-middle" scope="col">Opsi</th>
                                    </tr>
                                  </thead>
                                  <tbody id="tbody_obat">

                                </tbody>
                                </table>
                              </div>
                              <hr>
                              <div class="form-row mt-1">

                                  <div class="col-md-1">
                                      <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-tindakan">Tambahkan Tindakan </button>
                                      </div>
                            </div>
                            <div class="table-responsive">
                                <table  id="tbl_tindakan" class="table table-bordered" style="width:100%">
                                  <thead>
                                    <tr>
                                      <th class="align-middle" scope="col">Tindakan</th>
                                      <th class="align-middle" scope="col">Catatan</th>

                                      <th class="align-middle" scope="col">Opsi</th>
                                    </tr>
                                  </thead>
                                  <tbody id="tbody_tindakan">

                                </tbody>
                                </table>
                              </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalResep" tabindex="-1" role="dialog" aria-labelledby="modalResepLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">List Resep Obat</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <table class="table table-bordered" id='tbl_list_resep' width="100%">
                <thead>
                  <tr>
                    <th class="text-center">Opsi</th>
                    <th class="text-center">Resep</th>
                  </tr>
                </thead>
              </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>

<div class="modal fade" id="modalSubyektif" tabindex="-1" role="dialog" aria-labelledby="modalSubyektifLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">List Data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <div class="col-md-1">
                <button type="button" class="btn waves-effect waves-light btn-sm btn-info btn-add-keluhan">Tambah</button>
                </div>
            <table class="table table-bordered" id='tbl_list_subyektif' width="100%">
                <thead>
                  <tr>
                    <th class="text-center">Opsi</th>
                    <th class="text-center">Nama</th>
                  </tr>
                </thead>
              </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>

<div class="modal fade" id="modalPenunjang" tabindex="-1" role="dialog" aria-labelledby="modalPenunjangLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">Tambah Pemeriksaan Penunjang</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" id="frm_pemeriksaan_penunjang" class="form-horizontal" enctype="multipart/form-data">
            @csrf
          <div class="modal-body">
            <div class="col-md-1">
                {{-- <button type="button" class="btn waves-effect waves-light btn-sm btn-info btn-add-penunjang">Tambah</button> --}}
                </div>
                    <div class="form-row mt-1" id="div_name">
                        <label  class="col-sm-4 text-right">Penunjang</label>
                        <div class="col-md-8">
                            <select class="form-control form-control-sm select2 cb_penunjang" name="cb_penunjang[]" id="cb_penunjang">
                                <option value="">Pilih</option>
                                @foreach ($mpenunjang as $row)
                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                              </select>
                        </div>
                    </div>
                    <div class="form-row mt-1" id="div_name">
                        <label  class="col-sm-4 text-right">File</label>
                        <div class="col-md-8">
                            <input type="file" class="form-control" id="file_penunjang" name="file_penunjang" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-row mt-1" id="div_name">
                        <label  class="col-sm-4 text-right">Hasil</label>
                        <div class="col-md-8">
                            <input type="textarea" class="form-control" id="hasil_penunjang" name="hasil_penunjang" autocomplete="off">
                        </div>
                    </div>


          </div>
          <div class="modal-footer">
            <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-ins-rkmedis-penunjang" onclick="insRkmedisPenunjang()">Simpan</button>
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
          </div>
        </form>
      </div>
    </div>
</div>

<div class="modal fade" id="modalDiagnosa" tabindex="-1" role="dialog" aria-labelledby="modalDiagnosaLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">List Data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <div class="col-md-1">
                <button type="button" class="btn waves-effect waves-light btn-sm btn-info btn-add-diagnosa">Tambah</button>
                </div>
            <table class="table table-bordered" id='tbl_list_diagnosa' width="100%">
                <thead>
                  <tr>
                    <th class="text-center">Opsi</th>
                    <th class="text-center">Nama</th>
                  </tr>
                </thead>
              </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>

<div class="modal fade" id="modalObat" tabindex="-1" role="dialog" aria-labelledby="modalObatLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">List Obat</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <table class="table table-bordered" id='tbl_list_obat' width="100%">
                <thead>
                  <tr>
                    <th class="text-center">Opsi</th>
                    <th class="text-center">Obat</th>
                    <th class="text-center">Stok</th>
                  </tr>
                </thead>
              </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>

<div class="modal fade" id="modalObatRacikan" role="dialog" aria-labelledby="modalObatRacikanLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="InputModalLabel">Tambah Obat Racikan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
          <div class="form-row mt-1">
              <label class="col-md-2 text-right">Nama Racikan</label>
              <div class="col-md-4">
              <input type="text" class="form-control form-control-sm" autocomplete="off" id="nmracik" name="nmracik">
              </div>
              <label class="col-md-2 text-right">Jumlah Racikan</label>
              <div class="col-md-2">
                  <input type="text" class="form-control form-control-sm" id="qtyracik" name="qtyracik" value="0">
                </div>
          </div>
          <div class="form-row mt-1">
              <div class="col-md-1">
                  <button type="button" class="btn waves-effect waves-light btn-sm btn-warning btn-add-obat-in-racikan">Tambah</button>
              </div>
          </div>
          <div class="table-responsive">
              <table  id="tbl_obat_inracikan" class="table table-bordered" style="width:100%">
                <thead>
                  <tr>
                    <th class="align-middle" scope="col">Obat</th>
                    <th class="align-middle" scope="col">Qty</th>
                    <th class="align-middle" scope="col">Opsi</th>
                  </tr>
                </thead>
                <tbody id="tbody_obat_inracikan">
              </tbody>

              </table>
            </div>


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success btn-add-obat-racikan">Tambahkan Obat Racikan</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </div>

    </div>
  </div>
</div>

<div class="modal fade" id="modalTindakan" tabindex="-1" role="dialog" aria-labelledby="modalTindakanLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">List Tindakan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <table class="table table-bordered" id='tbl_list_tindakan' width="100%">
                <thead>
                  <tr>
                    <th class="text-center">Opsi</th>
                    <th class="text-center">Tindakan</th>
                  </tr>
                </thead>
              </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>

<div class="modal fade" id="modalTemplateSoap" tabindex="-1" role="dialog" aria-labelledby="modalTemplateLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">Template SOAP</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <table class="table table-bordered" id='tbl_template_soap' width="100%">
                <thead>
                  <tr>
                    <th class="text-center">Opsi</th>
                    <th class="text-center">Subyektif (S)</th>
                    <th class="text-center">Assesment (A)</th>
                    <th class="text-center">Planning (P)</th>
                  </tr>
                </thead>
              </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>


<div class="modal fade" id="modalAddKeluhan" tabindex="-1" role="dialog" aria-labelledby="modalKeluhanLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">Tambah Baru</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <form method="POST" id="frm_mkeluhan" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="form-row mt-1" id="div_name">
                    <label  class="col-sm-2 text-right">Keluhan</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="t_keluhan" name="t_keluhan" autocomplete="off">
                    </div>

                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-ins-keluhan" onclick="insKeluhan()">Simpan</button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            </div>
         </form>
      </div>
    </div>
</div>

<div class="modal fade" id="modalAddDiagnosa" tabindex="-1" role="dialog" aria-labelledby="modalDiagnosaLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">Tambah Baru</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <form method="POST" id="frm_mdiagnosa" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="form-row mt-1" id="div_name">
                    <label  class="col-sm-2 text-right">Diagnosa</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="t_diagnosa" name="t_diagnosa" autocomplete="off">
                    </div>

                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-ins-diagnosa" onclick="insDiagnosa()">Simpan</button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            </div>
         </form>
      </div>
    </div>
</div>

<div class="modal fade" id="modalAddPenunjang" tabindex="-1" role="dialog" aria-labelledby="modalPenunjangLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">Tambah Baru</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <form method="POST" id="frm_mkeluhan" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="form-row mt-1" id="div_name">
                    <label  class="col-sm-4 text-right">Keterangan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="t_penunjang" name="t_penunjang" autocomplete="off">
                    </div>

                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-ins-keluhan" onclick="insPenunjang()">Simpan</button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            </div>
         </form>
      </div>
    </div>
</div>

<div class="modal fade" id="modalListObatRacikan" tabindex="-1" role="dialog" aria-labelledby="modalListObatRacikanLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">List Obat Racikan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <div class="form-row mt-1">
                <div class="col-md-1">
                    <button type="button" class="btn waves-effect waves-light btn-sm btn-warning btn-add-obat-mracik">Tambah</button>
                </div>
            </div>
            <table class="table table-bordered" id='tbl_list_obat_racikan' width="100%">
                <thead>
                  <tr>
                    <th class="text-center">Opsi</th>
                    <th class="text-center">Nama Racikan</th>
                  </tr>
                </thead>
              </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>
@endsection

@push('bottom.scripts')
<script type="text/javascript">
  var rowIdxObat = 0;
$(document).ready(function(){
        $('#tglinput').datepicker('update', new Date());
        var id = "{{ $rkmedis->id }}";
        let norm = "{{ $reg->get_registrasi->norm }}";
        loadSoapSubyektif(id,norm);
        loadSoapObyektif(id);
        loadSoapPenunjang(id);
        loadSoapAssesment(id,norm);
        loadSoapObat(id,norm);
        loadSoapTindakan(id,norm);

});

$('body').on('click', '.btn-add-keluhan', function () {
       $('#modalAddKeluhan').modal('show');
});

$('body').on('click', '.btn-add-diagnosa', function () {
       $('#modalAddDiagnosa').modal('show');
});

$('body').on('click', '.btn-add-penunjang', function () {
       $('#modalAddPenunjang').modal('show');
});

function insKeluhan()
{
    var keluhan = $("#t_keluhan").val();
    if(keluhan == '' || keluhan == null){
        Swal.fire("Error!", "Data tidak boleh kosong", "error");
    }else{
        var form_data = new FormData();
                    form_data.append("keluhan", keluhan);
          $.ajax({
            type: "POST",
            url: "{{ route('mkeluhan.store') }}"+'?_token=' + '{{ csrf_token() }}',
            data: form_data,
            dataType: "json",
            contentType: false,
            cache : false,
            processData : false,
            success: function(result){
                var oTable = $('#tbl_list_subyektif').dataTable();
                    oTable.fnDraw(false);
                    $('#modalAddKeluhan').modal('hide');
            } ,error: function(xhr, status, error) {
                alert(error);
            },

        });
    }
}

function insDiagnosa()
{
    var diagnosa = $("#t_diagnosa").val();
    if(diagnosa == '' || diagnosa == null){
        Swal.fire("Error!", "Data tidak boleh kosong", "error");
    }else{
        var form_data = new FormData();
                    form_data.append("diagnosa", diagnosa);
          $.ajax({
            type: "POST",
            url: "{{ route('mdiagnosa.store') }}"+'?_token=' + '{{ csrf_token() }}',
            data: form_data,
            dataType: "json",
            contentType: false,
            cache : false,
            processData : false,
            success: function(result){
                var oTable = $('#tbl_list_diagnosa').dataTable();
                    oTable.fnDraw(false);
                    $('#modalAddDiagnosa').modal('hide');
            } ,error: function(xhr, status, error) {
                alert(error);
            },

        });
    }
}

function insPenunjang()
{
    var penunjang = $("#t_penunjang").val();
    if(penunjang == '' || penunjang == null){
        Swal.fire("Error!", "Data tidak boleh kosong", "error");
    }else{
        var form_data = new FormData();
                    form_data.append("penunjang", penunjang);
          $.ajax({
            type: "POST",
            url: "{{ route('mpenunjang.store') }}"+'?_token=' + '{{ csrf_token() }}',
            data: form_data,
            dataType: "json",
            contentType: false,
            cache : false,
            processData : false,
            success: function(result){
                var oTable = $('#tbl_list_penunjang').dataTable();
                    oTable.fnDraw(false);
                    $('#modalAddPenunjang').modal('hide');
            } ,error: function(xhr, status, error) {
                alert(error);
            },

        });
    }
}

function insRkmedisPenunjang()
{
    let id_rkmedis = "{{ $rkmedis->id }}";
    let id_rkmedis_obyektif = "{{ $rkmedis_obyektif->id_obyektif }}";
    let penunjang = $("#cb_penunjang").val();
    let file = $("#file_penunjang").val();
    let hasil = $("#hasil_penunjang").val();

    if(penunjang == '' || file == ''){
        Swal.fire("Error!", "Penunjang dan file tidak boleh kosong", "error");
    }else{
        var form_data = new FormData();
            form_data.append("id_rkmedis", id_rkmedis);
            form_data.append("id_rkmedis_obyektif", id_rkmedis_obyektif);
            form_data.append("penunjang", penunjang);
            form_data.append("hasil", hasil);
            form_data.append("file_penunjang", document.getElementById("file_penunjang").files[0]);
          $.ajax({
            type: "POST",
            url: "{{ route('rkmedis-penunjang.store') }}"+'?_token=' + '{{ csrf_token() }}',
            data: form_data,
            dataType: "json",
            contentType: false,
            cache : false,
            processData : false,
            success: function(result){
            //     location.reload();
            //     $("html, body").animate({
            // scrollTop: $('html, body').get(0).scrollHeight}, 1000);
                    $('#modalPenunjang').modal('hide');
                    loadSoapPenunjang(id_rkmedis);
            } ,error: function(xhr, status, error) {
                alert(error);
            },

        });
    }
}



$('body').on('click', '.btn-subyektif', function () {
      loadKeluhan();
       $('#modalSubyektif').modal('show');

    });
$('body').on('click', '.btn-pemeriksaan_penunjang', function () {
      loadPenunjang();
       $('#modalPenunjang').modal('show');

    });

$('body').on('click', '.btn-diagnosa', function () {
      loadDiagnosa();
       $('#modalDiagnosa').modal('show');

    });

$('body').on('click', '.btn-obat', function () {
      loadObat();
       $('#modalObat').modal('show');

    });

    $('body').on('click', '.btn-obat-racikan', function () {
    loadObatRacikan();
        $('#modalListObatRacikan').modal('show');

    });

    $('body').on('click', '.btn-add-obat-mracik', function () {
       $("#tbody_obat_inracikan").empty();
       $('#modalListObatRacikan').modal('hide');
    $("#nmracik").val('');
    $("#qtyracik").val('');
       $('#modalObatRacikan').modal('show');
});

$('body').on('click', '.btn-tindakan', function () {
      loadTindakan();
       $('#modalTindakan').modal('show');

    });

    function loadObatRacikan()
    {
        $("#tbl_list_obat_racikan").dataTable().fnDestroy();

        const page_url = "{{ route('template-soap.get-mobat-racik') }}";
        var table = $('#tbl_list_obat_racikan').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                type: 'GET'
            },
            columns: [
                { "data": null,"sortable": false,
                            render: function (data, type, row, meta) {
                                    var result = '<a class="btn btn-success btn-set-obat-racikan" href="#"\
                                    data-id="'+row.id+'"\
                                    data-nmracik="'+row.name+'"\
                                    data-qtyracik="'+row.qty+'"\
                                    >Pilih</a>';
                                    result += '&nbsp;<a class="btn btn-danger btn-del-obat-racikan" href="#"\
                                    data-id="'+row.id+'"\
                                    >Hapus</a>';
                                    return result;
                            }
                        },
                {data: 'name', name: 'name', orderable: true,searchable: true}

            ],
            responsive: true,
            columnDefs: [

            ],

        });
    }

    function loadTindakan()
    {
        $("#tbl_list_tindakan").dataTable().fnDestroy();

        const page_url = "{{ route('template-soap.get-mtindakan') }}";
        var table = $('#tbl_list_tindakan').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                type: 'GET'
            },
            columns: [
                { "data": null,"sortable": false,
                            render: function (data, type, row, meta) {
                                    var result = '<a class="btn btn-success btn-set-tindakan" href="#"\
                                    data-kode="'+row.kdpelayanan+'"\
                                    data-name="'+row.nmpelayanan+'"\
                                    >Pilih</a>';
                                    return result;
                            }
                        },
                {data: 'nmpelayanan', name: 'nmpelayanan', orderable: true,searchable: true}
            ],
            responsive: true,
            columnDefs: [

            ],

        });
    }


    function loadObat()
    {
        $("#tbl_list_obat").dataTable().fnDestroy();

        const page_url = "{{ route('template-soap.get-mobat') }}";
        var table = $('#tbl_list_obat').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                type: 'GET'
            },
            columns: [
                { "data": null,"sortable": false,
                            render: function (data, type, row, meta) {
                                    var result = '<a class="btn btn-success btn-set-obat" href="#"\
                                    data-kode="'+row.kdbrg+'"\
                                    data-name="'+row.get_brg.nmbrg+'"\
                                    data-stok="'+row.stoknowbagian+'"\
                                    >Pilih</a>';
                                    return result;
                            }
                        },
                {data: 'get_brg.nmbrg', name: 'get_brg.nmbrg', orderable: true,searchable: true},
                {data: 'stoknowbagian', name: 'stoknowbagian', orderable: true,searchable: true}
            ],
            responsive: true,
            columnDefs: [

            ],

        });
    }

    function loadDiagnosa()
    {
        $("#tbl_list_diagnosa").dataTable().fnDestroy();

        const page_url = "{{ route('template-soap.get-mdiagnosa') }}";
        var table = $('#tbl_list_diagnosa').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                type: 'GET'
            },
            columns: [
                { "data": null,"sortable": false,
                            render: function (data, type, row, meta) {
                                    var result = '<a class="btn btn-success btn-set-diagnosa" href="#"\
                                    data-id="'+row.id+'"\
                                    data-name="'+row.name+'"\
                                    >Pilih</a>';
                                    result += '&nbsp;<a class="btn btn-danger btn-del-diagnosa" href="#"\
                                    data-id="'+row.id+'"\
                                    >Hapus</a>';
                                    return result;
                            }
                        },
                {data: 'name', name: 'name', orderable: true,searchable: true}
        ],
            responsive: true,
            columnDefs: [

            ],

        });
    }

    function loadPenunjang()
    {
        $("#tbl_list_penunjang").dataTable().fnDestroy();

        const page_url = "{{ route('template-soap.get-mpenunjang') }}";
        var table = $('#tbl_list_penunjang').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                type: 'GET'
            },
            columns: [
                { "data": null,"sortable": false,
                            render: function (data, type, row, meta) {
                                    var result = '<a class="btn btn-success btn-set-penunjang" href="#"\
                                    data-id="'+row.id+'"\
                                    data-name="'+row.name+'"\
                                    >Pilih</a>';
                                    return result;
                            }
                        },
                {data: 'name', name: 'name', orderable: true,searchable: true}
        ],
            responsive: true,
            columnDefs: [

            ],

        });
    }

    function loadKeluhan()
    {
        $("#tbl_list_subyektif").dataTable().fnDestroy();
        const page_url = "{{ route('template-soap.get-mkeluhan') }}";

        var table = $('#tbl_list_subyektif').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                type: 'GET'
            },
            columns: [
                { "data": null,"sortable": false,
                            render: function (data, type, row, meta) {
                                    var result = '<a class="btn btn-success btn-set-subyektif" href="#"\
                                    data-id="'+row.id+'"\
                                    data-subyektif="'+row.name+'"\
                                    >Pilih</a>';
                                    result += '&nbsp;<a class="btn btn-danger btn-del-subyektif" href="#"\
                                    data-idkeluhan="'+row.id+'"\
                                    >Hapus</a>';
                                    return result;
                            }
                        },
                {data: 'name', name: 'name', orderable: true,searchable: true}
        ],
            responsive: true,
            columnDefs: [

            ],

        });
    }



    $('body').on('click', '.btn-set-template', function () {
        var id_template_soap = $(this).data('id');
        var nmresep = $(this).data('nmresep');

        $('#id_template_soap').val(id_template_soap);
        $('#nmresep').val(nmresep);
        loadTempSubyektif(id_template_soap);
       // loadTempPenunjang(id_template_soap);
        loadTempAssesment(id_template_soap);
        loadTempTindakan(id_template_soap);
        loadTempObat(id_template_soap);
        $('#modalTemplateSoap').modal('hide');

    });

    function loadTempSubyektif(id)
    {
        $('#tbl_subyektif tbody').empty();
        var page_url = "{{ route('rekam-medis.get-detail-template-soap') }}";
        $.ajax({
        url: page_url,
        type: 'GET',
        data: {
            id_template_soap : id,
            jns_soap : "subyektif"
        },
        success: function(result) {
                for(i = 0; i < result.total; i++)
                {
                    currentdata = result.list[i];
                    var keterangan = '';
                    if(currentdata.keterangan == null){
                        keterangan = '&nbsp;';
                    } else{
                        keterangan = currentdata.keterangan;
                        keterangan = keterangan.replace(" ", "&nbsp;");
                    }

					var tr ="\
                        <tr>\
                            <td>"+ currentdata.subyektif +"</td>\
                            <td class='row-index text-left'>\
                                <input type='text' name='catatan_subyektif[]' value="+keterangan+" />\
                            </td>\
                            <td><a class='btn btn-danger btn-sm btn-delete-subyektif row-index'\
                            href='javascript:void(0);'>Hapus</a>\
                            <input type='hidden' name='id_subyektif[]' value="+currentdata.id+" />\
                            <input type='hidden' name='id_keluhan[]' value="+currentdata.idkeluhan+" />\
                            <textarea name='subyektif[]' style='display:none;'>"+currentdata.subyektif+"</textarea>\
                            </td>\
                        </tr>\
                    ";


                    $('#tbl_subyektif tbody').append(tr);
                }
        }
      });
    }
    function loadTempPenunjang(id)
    {
        $('#tbl_pemeriksaan_penunjang tbody').empty();
        var page_url = "{{ route('rekam-medis.get-detail-template-soap') }}";
        $.ajax({
        url: page_url,
        type: 'GET',
        data: {
            id_template_soap : id,
            jns_soap : "penunjang"
        },
        success: function(result) {
                for(i = 0; i < result.total; i++)
                {
                    currentdata = result.list[i];

                    if(currentdata.file == null || currentdata.file == '')
                    {
                        let file = '';
                    }else{
                        let file = currentdata.file;
                    }

					var tr ="\
                        <tr>\
                            <td>"+ currentdata.name +"</td>\
                            <td><a href='javascript:void(0)' onclick='downloadFile("+ currentdata.id +")'>"+ file +"</a></td>\
                            <td class='row-index text-left'>\
                                <input type='text' name='catatan_penunjang[]'/>\
                            </td>\
                            <td><a class='btn btn-danger btn-sm btn-delete-penunjang row-index'\
                            href='javascript:void(0);'>Hapus</a>\
                            <input type='hidden' name='id_penunjang[]' value="+currentdata.id+" />\
                            <input type='hidden' name='penunjang[]' value="+currentdata.name+" />\
                            </td>\
                        </tr>\
                    ";


                    $('#tbl_pemeriksaan_penunjang tbody').append(tr);
                }
        }
      });
    }
    function loadTempAssesment(id)
    {
        $('#tbl_diagnosa tbody').empty();
        var page_url = "{{ route('rekam-medis.get-detail-template-soap') }}";
        $.ajax({
        url: page_url,
        type: 'GET',
        data: {
            id_template_soap : id,
            jns_soap : "assesment"
        },
        success: function(result) {
                for(i = 0; i < result.total; i++)
                {
                    currentdata = result.list[i];

                    var keterangan = '';
                    if(currentdata.keterangan == null){
                        keterangan = '&nbsp;';
                    } else{
                        keterangan = currentdata.keterangan;
                        keterangan = keterangan.replace(" ", "&nbsp;");
                    }

					var tr ="\
                        <tr>\
                            <td>"+ currentdata.kd_diagnosa +"</td>\
                            <td>"+ currentdata.nm_diagnosa +"</td>\
                            <td class='row-index text-left'>\
                                <input type='text' name='catatan_diagnosa[]' value="+keterangan+" />\
                            </td>\
                            <td><a class='btn btn-danger btn-sm btn-delete-diagnosa row-index'\
                            href='javascript:void(0);'>Hapus</a>\
                            <input type='hidden' name='idpenyakit[]' value="+currentdata.kd_diagnosa+" />\
                            <input type='hidden' name='kdpenyakit[]' value="+currentdata.kd_diagnosa+" />\
                            <textarea name='nmpenyakiteng[]' style='display:none;'>"+currentdata.nm_diagnosa+"</textarea>\
                            </td>\
                        </tr>\
                    ";


                    $('#tbl_diagnosa tbody').append(tr);
                }
        }
      });
    }

    function loadTempObat(id)
    {
        $('#tbl_obat tbody').empty();
        var page_url = "{{ route('rekam-medis.get-detail-template-soap') }}";
        $.ajax({
        url: page_url,
        type: 'GET',
        data: {
            id_template_soap : id,
            jns_soap : "obat"
        },
        success: function(result) {
                for(i = 0; i < result.total; i++)
                {
                    currentdata = result.list[i];
                    let nm_item;
                    nm_item = currentdata.nm_item;

                    var at = currentdata.aturan_pakai;
                    var aturan_pakai = at.split("-");
                    rowIdxObat++;

					var tr ="\
                        <tr>\
                            <td>"+ nm_item +"</td>\
                            <td width='10%' class='row-index text-left'>\
                                <input type='text' class='form-control' name='qty[]' value='"+currentdata.qty+"'/>\
                            </td>\
                            <td class='row-index text-left'>\
                                <div class='form-row mt-1'>\
                                <div class='col-md-2'>\
                                  <input type='text' class='form-control' name='aturan_pakai1[]' id='aturan_pakai1"+rowIdxObat+"' value='"+aturan_pakai[0]+"' />\
                                </div>\
                                <div class='col-md-1'>\-</div>\
                                <div class='col-md-2'>\
                                  <input type='text' class='form-control' name='aturan_pakai2[]' id='aturan_pakai2"+rowIdxObat+"' value='"+aturan_pakai[1]+"' />\
                                </div>\
                                <div class='col-md-1'>-</div>\
                                <div class='col-md-2'>\
                                  <input type='text' class='form-control' name='aturan_pakai3[]' id='aturan_pakai3"+rowIdxObat+"' value='"+aturan_pakai[2]+"' />\
                                </div>\
                            </div>\
                                <hr>\
                                <div class='form-row mt-1'>\
                                    <div class='col-md-4'>\
                                        <select class='form-control form-control-sm select2 cb_ketentuan' name='ketentuan[]' id='ketentuan"+rowIdxObat+"'>\
                                            <option value=''>Pilih</option>\
                                            @foreach ($ketentuan as $row)\
                                                <option value='{{ $row->id }}'>{{ $row->name }}</option>\
                                            @endforeach\
                                        </select>\
                                    </div>\
                                    <div class='col-md-4'>\
                                    <input type='text' class='form-control' name='keterangan[]' id='keterangan"+rowIdxObat+"' />\
                                    </div>\
                                </div>\
                            </td>\
                            <td><a class='btn btn-danger btn-sm btn-delete-obat row-index'\
                            href='javascript:void(0);'>Hapus</a>\
                            <input type='hidden' name='kdbrg[]' value="+currentdata.kd_item+" />\
                            <input type='hidden' name='nmracik[]' value="+currentdata.nmracik+" />\
                            <input type='hidden' name='qtyracik[]' value="+currentdata.qtyracik+" />\
                            <textarea name='nmbrg[]' style='display:none;'>"+currentdata.nm_item+"</textarea>\
                            </td>\
                        </tr>\
                    ";


                    $('#tbl_obat tbody').append(tr);

                    $("#ketentuan"+rowIdxObat).val(currentdata.ketentuan);

                    //$(".cb_ketentuan").select2();
            }
        }
      });
    }

    function loadTempTindakan(id)
    {
        $('#tbl_tindakan tbody').empty();
        var page_url = "{{ route('rekam-medis.get-detail-template-soap') }}";
        $.ajax({
        url: page_url,
        type: 'GET',
        data: {
            id_template_soap : id,
            jns_soap : "tindakan"
        },
        success: function(result) {
                for(i = 0; i < result.total; i++)
                {
                    currentdata = result.list[i];

					var tr ="\
                        <tr>\
                            <td>"+ currentdata.nm_item +"</td>\
                            <td class='row-index text-left'>\
                                <input type='text' name='catatan_tindakan[]' value='"+currentdata.catatan+"'/>\
                            </td>\
                            <td><a class='btn btn-danger btn-sm btn-delete-tindakan row-index'\
                            href='javascript:void(0);'>Hapus</a>\
                            <input type='hidden' name='kdpelayanan[]' value="+currentdata.kd_item+" />\
                            <textarea name='nmpelayanan[]' style='display:none;'>"+currentdata.nm_item+"</textarea>\
                            </td>\
                        </tr>\
                    ";


                    $('#tbl_tindakan tbody').append(tr);
                }
        }
      });
    }


    function loadSoapSubyektif(id,norm)
    {
        $('#tbl_subyektif tbody').empty();
        var page_url = "{{ route('rekam-medis.get-detail-soap') }}";
        $.ajax({
        url: page_url,
        type: 'GET',
        data: {
            id_rekam_medis_soap : id,
            jns_soap : "subyektif",
            norm_pasien : norm
        },
        success: function(result) {
                for(i = 0; i < result.total; i++)
                {
                    currentdata = result.list[i];
                    var keterangan = '';
                    if(currentdata.keterangan == null){
                        keterangan = '&nbsp;';
                    } else{
                        keterangan = currentdata.keterangan;
                        keterangan = keterangan.replace(" ", "&nbsp;");
                    }

					var tr ="\
                        <tr>\
                            <td>"+ currentdata.subyektif +"</td>\
                            <td class='row-index text-left'>\
                                <input type='text' name='catatan_subyektif[]' value="+keterangan+" /> \
                            </td>\
                            <td><a class='btn btn-danger btn-sm btn-delete-subyektif row-index'\
                            href='javascript:void(0);'>Hapus</a>\
                            <input type='hidden' name='id_subyektif[]' value="+currentdata.id_subyektif+" />\
                            <input type='hidden' name='id_keluhan[]' value="+currentdata.idkeluhan+" />\
                            <textarea name='subyektif[]' style='display:none;'>"+currentdata.subyektif+"</textarea>\
                            </td>\
                        </tr>\
                    ";


                    $('#tbl_subyektif tbody').append(tr);
                }
        }
      });
    }
    function loadSoapObyektif(id)
    {
        $('#tbl_obyektif tbody').empty();
        var page_url = "{{ route('rekam-medis.get-detail-soap') }}";
        $.ajax({
        url: page_url,
        type: 'GET',
        data: {
            id_rekam_medis_soap : id,
            jns_soap : "obyektif"
        },
        success: function(result) {

                    data = result.list;

				$('#kesadaran').val(data.ttv_kesadaran);
				$('#nadi').val(data.ttv_nadi);
				$('#tekanan_darah').val(data.ttv_tek_darah);
				$('#frek_nafas').val(data.ttv_frek_nafas);
				$('#spo2').val(data.spo2);
				$('#suhu').val(data.ttv_suhu);
				$('#tb').val(data.pf_tb);
				$('#bb').val(data.pf_bb);
				$('#imt').val(data.pf_imt);
				$('#kepala').val(data.pf_kepala);
				$('#mata').val(data.pf_mata);
				$('#telinga').val(data.pf_telinga);
				$('#hidung').val(data.pf_hidung);
				$('#mulut_faring').val(data.pf_mulut_faring);
				$('#leher').val(data.pf_leher);
				$('#paru').val(data.pf_thoraks_paru);
				$('#jantung').val(data.pf_thoraks_jantung);
				$('#abdomen').val(data.pf_abdomen);
				$('#genitalia').val(data.pf_genitalia);
				$('#rectum').val(data.pf_rectum_anal);
				$('#extremitas').val(data.pf_extremitas);

        }
      });
    }

    function loadSoapPenunjang(id)
    {
        $('#tbl_pemeriksaan_penunjang tbody').empty();
        var page_url = "{{ route('rekam-medis.get-detail-soap') }}";
        $.ajax({
        url: page_url,
        type: 'GET',
        data: {
            id_rekam_medis_soap : id,
            jns_soap : "penunjang"
        },
        success: function(result) {
                for(i = 0; i < result.total; i++)
                {
                    currentdata = result.list[i];

                    if(currentdata.file == null || currentdata.file == '')
                    {
                        var file = '';
                    }else{
                        var file = currentdata.file;
                    }

					var tr ="\
                        <tr>\
                            <td>"+ currentdata.name +"</td>\
                            <td><a href='javascript:void(0)' onclick='downloadFile("+ currentdata.id +")'>"+ file +"</a></td>\
                            <td class='row-index text-left'>\
                                <input type='text' name='catatan_penunjang[]' value='"+currentdata.keterangan+"'/>\
                            </td>\
                            <td>\
                            <a class='btn btn-danger btn-sm btn-delete-penunjang row-index'\
                            href='#' data-id='"+currentdata.id+"'>Hapus</a>\
                            <input type='hidden' name='id_penunjang[]' value="+currentdata.id+" />\
                            <textarea name='penunjang[]' style='display:none;'>"+currentdata.name+"</textarea>\
                            </td>\
                        </tr>\
                    ";


                    $('#tbl_pemeriksaan_penunjang tbody').append(tr);
                }
        }
      });
    }
    function loadSoapAssesment(id,norm)
    {
        $('#tbl_diagnosa tbody').empty();
        var page_url = "{{ route('rekam-medis.get-detail-soap') }}";
        $.ajax({
        url: page_url,
        type: 'GET',
        data: {
            id_rekam_medis_soap : id,
            jns_soap : "assesment",
            norm_pasien : norm
        },
        success: function(result) {
                for(i = 0; i < result.total; i++)
                {
                    currentdata = result.list[i];
                    var keterangan = '';
                    if(currentdata.keterangan == null){
                        keterangan = '&nbsp;';
                    } else{
                        keterangan = currentdata.keterangan;
                        keterangan = keterangan.replace(" ", "&nbsp;");
                    }

					var tr ="\
                        <tr>\
                            <td>"+ currentdata.kd_diagnosa +"</td>\
                            <td>"+ currentdata.nm_diagnosa +"</td>\
                            <td class='row-index text-left'>\
                                <input type='text' name='catatan_diagnosa[]' value="+keterangan+" />\
                            </td>\
                            <td><a class='btn btn-danger btn-sm btn-delete-diagnosa row-index'\
                            href='javascript:void(0);'>Hapus</a>\
                            <input type='hidden' name='idpenyakit[]' value="+currentdata.kd_diagnosa+" />\
                            <input type='hidden' name='kdpenyakit[]' value="+currentdata.kd_diagnosa+" />\
                            <textarea name='nmpenyakiteng[]' style='display:none;'>"+currentdata.nm_diagnosa+"</textarea>\
                            </td>\
                        </tr>\
                    ";


                    $('#tbl_diagnosa tbody').append(tr);
                }
        }
      });
    }

    function loadSoapObat(id,norm)
    {
        $('#tbl_obat tbody').empty();
        var page_url = "{{ route('rekam-medis.get-detail-soap') }}";
        $.ajax({
        url: page_url,
        type: 'GET',
        data: {
            id_rekam_medis_soap : id,
            jns_soap : "obat",
            norm_pasien : norm
        },
        success: function(result) {
                for(i = 0; i < result.total; i++)
                {
                    currentdata = result.list[i];
                    let nm_item;
                    let keterangan;
                      nm_item = currentdata.nm_item;
                      var at = currentdata.aturan_pakai;
                    var aturan_pakai = at.split("-");
                    if(currentdata.catatan == 'null'){
                        keterangan = '';
                    } else{
                        keterangan = currentdata.catatan;
                    }
                    rowIdxObat++;

					var tr ="\
                    <tr id='R"+rowIdxObat+"'>\
                            <td>"+ nm_item +"</td>\
                            <td width='10%' class='row-index text-left'>\
                                <input type='text' class='form-control' name='qty[]' value='"+currentdata.qty+"'/>\
                            </td>\
                            <td class='row-index text-left'>\
                                <div class='form-row mt-1'>\
                                <div class='col-md-2'>\
                                  <input type='text' class='form-control' name='aturan_pakai1[]' id='aturan_pakai1"+rowIdxObat+"' value='"+aturan_pakai[0]+"' />\
                                </div>\
                                <div class='col-md-1'>\-</div>\
                                <div class='col-md-2'>\
                                  <input type='text' class='form-control' name='aturan_pakai2[]' id='aturan_pakai2"+rowIdxObat+"' value='"+aturan_pakai[1]+"' />\
                                </div>\
                                <div class='col-md-1'>-</div>\
                                <div class='col-md-2'>\
                                  <input type='text' class='form-control' name='aturan_pakai3[]' id='aturan_pakai3"+rowIdxObat+"' value='"+aturan_pakai[2]+"' />\
                                </div>\
                            </div>\
                                <hr>\
                                <div class='form-row mt-1'>\
                                    <div class='col-md-4'>\
                                        <select class='form-control form-control-sm select2 cb_ketentuan' name='ketentuan[]' id='ketentuan"+rowIdxObat+"'>\
                                            <option value=''>Pilih</option>\
                                            @foreach ($ketentuan as $row)\
                                                <option value='{{ $row->id }}'>{{ $row->name }}</option>\
                                            @endforeach\
                                        </select>\
                                    </div>\
                                    <div class='col-md-4'>\
                                                <input type='text' class='form-control' name='keterangan[]' id='keterangan"+rowIdxObat+"' value='"+keterangan+"' />\
                                        </div>\
                                </div>\
                            </td>\
                            <td><a class='btn btn-danger btn-sm btn-delete-obat row-index'\
                            href='javascript:void(0);'>Hapus</a>\
                            <input type='hidden' name='kdbrg[]' value="+currentdata.kd_item+" />\
                            <input type='hidden' name='nmracik[]' value="+currentdata.nmracik+" />\
                            <input type='hidden' name='qtyracik[]' value="+currentdata.qtyracik+" />\
                            <textarea name='nmbrg[]' style='display:none;'>"+currentdata.nm_item+"</textarea>\
                            </td>\
                        </tr>\
                    ";


                    $('#tbl_obat tbody').append(tr);
                    $("#ketentuan"+rowIdxObat).val(currentdata.ketentuan);

                   // loadAturanPakai(i);
                    //loadSediaan(i);
                    //loadKetentuan(i);
                    //loadKeteranganTambahan(i);
                }
        }
      });
    }

    function loadSoapTindakan(id,norm)
    {
        $('#tbl_tindakan tbody').empty();
        var page_url = "{{ route('rekam-medis.get-detail-soap') }}";
        $.ajax({
        url: page_url,
        type: 'GET',
        data: {
            id_rekam_medis_soap : id,
            jns_soap : "tindakan",
            norm_pasien : norm
        },
        success: function(result) {
                for(i = 0; i < result.total; i++)
                {
                    let catatan = '';
                    currentdata = result.list[i];
                    if(currentdata.catatan == 'null'){
                        catatan = '';
                    } else{
                        catatan = currentdata.catatan;
                    }


					var tr ="\
                        <tr>\
                            <td>"+ currentdata.nm_item +"</td>\
                            <td class='row-index text-left'>\
                                <input type='text' name='catatan_tindakan[]' value='"+catatan+"'/>\
                            </td>\
                            <td><a class='btn btn-danger btn-sm btn-delete-tindakan row-index'\
                            href='javascript:void(0);'>Hapus</a>\
                            <input type='hidden' name='kdpelayanan[]' value="+currentdata.kd_item+" />\
                            <textarea name='nmpelayanan[]' style='display:none;'>"+currentdata.nm_item+"</textarea>\
                            </td>\
                        </tr>\
                    ";


                    $('#tbl_tindakan tbody').append(tr);
                }
        }
      });
    }

var rowIdx = 0;

$('body').on('click', '.btn-set-subyektif', function () {
        var id = $(this).data('id');
        var subyektif = $(this).data('subyektif');
        $('#modalSubyektif').modal('hide');
        $('#tbody_subyektif').append(`<tr id="R${++rowIdx}">
            <td class="row-index text-left">
                ${subyektif}
            </td>
            <td class='row-index text-left'>\
                                <input type='text' name='catatan_subyektif[]'/>\
                            </td>\
            <td><a class='btn btn-danger btn-sm btn-delete-subyektif row-index'
            href='javascript:void(0);'>Hapus</a>
            <input type='hidden' name="id_keluhan[]" value="${id}" />
            <input type='hidden' name="subyektif[]" value="${subyektif}" />
            </td>

           </tr>`);

    });


    $('body').on('click', '.btn-del-subyektif', function () {
        if (confirm('Are you sure you want to delete?')) {
            $.ajax({
            type: "POST",
            url: "{{ url('mkeluhan/hapus') }}"+'?_token=' + '{{ csrf_token() }}',
            data: {
                id : $(this).data('idkeluhan')
            },
            success: function(result){
                var oTable = $('#tbl_list_subyektif').dataTable();
                    oTable.fnDraw(false);
            } ,error: function(xhr, status, error) {
                alert(error);
            },

        });
        } else {
        // Do nothing!
        console.log('Thing was not saved to the database.');
        }

});

$('body').on('click', '.btn-del-diagnosa', function () {
        if (confirm('Are you sure you want to delete?')) {
            $.ajax({
            type: "POST",
            url: "{{ url('mdiagnosa/hapus') }}"+'?_token=' + '{{ csrf_token() }}',
            data: {
                id : $(this).data('id')
            },
            success: function(result){
                var oTable = $('#tbl_list_diagnosa').dataTable();
                    oTable.fnDraw(false);
            } ,error: function(xhr, status, error) {
                alert(error);
            },

        });
        } else {
        // Do nothing!
        console.log('Thing was not saved to the database.');
        }

});

$('body').on('click', '.btn-del-obat-racikan', function () {
        if (confirm('Are you sure you want to delete?')) {
            $.ajax({
            type: "POST",
            url: "{{ url('mracikan/hapus') }}"+'?_token=' + '{{ csrf_token() }}',
            data: {
                id : $(this).data('id')
            },
            success: function(result){
                var oTable = $('#tbl_list_obat_racikan').dataTable();
                    oTable.fnDraw(false);
            } ,error: function(xhr, status, error) {
                alert(error);
            },

        });
        } else {
        // Do nothing!
        console.log('Thing was not saved to the database.');
        }

});

    $('#tbody_subyektif').on('click', '.btn-delete-subyektif', function () {
        // var child = $(this).closest('tr').nextAll();
        // child.each(function () {
        //   var id = $(this).attr('id');
        //   var dig = parseInt(id.substring(1));
        //   $(this).attr('id', `R${dig - 1}`);
        // });
        $(this).closest('tr').remove();
        rowIdx--;
    });


    var rowIdxPenunjang = 0;
    $('body').on('click', '.btn-set-penunjang', function () {
        var id = $(this).data('id');
        var name = $(this).data('name');
        $('#modalPenunjang').modal('hide');
        $('#tbody_pemeriksaan_penunjang').append(`<tr id="R${++rowIdxPenunjang}">
            <td class="row-index text-left">
                ${name}
            </td>
            <td><a class='btn btn-danger btn-sm btn-delete-penunjang row-index'
            href='javascript:void(0);'>Hapus</a>
            <input type='hidden' name="id_penunjang[]" value="${id}" />
            <input type='hidden' name="penunjang[]" value="${name}" />
            </td>

           </tr>`);

    });

    // $('#tbody_pemeriksaan_penunjang').on('click', '.btn-delete-penunjang', function () {

    //     $(this).closest('tr').remove();
    //     rowIdxPenunjang--;
    // });
    $('#tbody_pemeriksaan_penunjang').on('click', '.btn-delete-penunjang', function () {
        if (confirm('Are you sure you want to delete?')) {
            $.ajax({
            type: "POST",
            url: "{{ url('rkmedis-penunjang/hapus') }}"+'?_token=' + '{{ csrf_token() }}',
            data: {
                id : $(this).data('id')
            },
            success: function(result){
                location.reload();
            //     $("html, body").animate({
            // scrollTop: $('html, body').get(0).scrollHeight}, 1000);
            } ,error: function(xhr, status, error) {
                alert(error);
            },

        });
        } else {
        // Do nothing!
        console.log('Thing was not saved to the database.');
        }
    });

    var rowIdxDiagnosa = 0;
    $('body').on('click', '.btn-set-diagnosa', function () {
        var id = $(this).data('id');
        var name = $(this).data('name');
        $('#modalDiagnosa').modal('hide');
        $('#tbody_diagnosa').append(`<tr id="R${++rowIdxDiagnosa}">
            <td class="row-index text-left">
                ${id}
            </td>
            <td class="row-index text-left">
                ${name}
            </td>
            <td class='row-index text-left'>\
                                <input type='text' name='catatan_diagnosa[]'/>\
                            </td>\
            <td><a class='btn btn-danger btn-sm btn-delete-diagnosa row-index'
            href='javascript:void(0);'>Hapus</a>
            <input type='hidden' name="idpenyakit[]" value="${id}" />
            <input type='hidden' name="kdpenyakit[]" value="${id}" />
            <input type='hidden' name="nmpenyakiteng[]" value="${name}" />
            </td>

           </tr>`);

    });

    $('#tbody_diagnosa').on('click', '.btn-delete-diagnosa', function () {
        // var child = $(this).closest('tr').nextAll();
        // child.each(function () {
        //   var id = $(this).attr('id');
        //   var dig = parseInt(id.substring(1));
        //   $(this).attr('id', `R${dig - 1}`);
        // });
        $(this).closest('tr').remove();
        rowIdxDiagnosa--;
    });


    $('body').on('click', '.btn-set-obat', function () {
        var kode = $(this).data('kode');
        var name = $(this).data('name');
        var stok = $(this).data('stok');
        if(stok == 0){
            Swal.fire("Error!", "Obat ini habis", "error");
        }else{
            $('#modalObat').modal('hide');
            $('#tbody_obat').append(`<tr id="R${++rowIdxObat}">
                <td width="40%" class="row-index text-left">
                    ${name}
                </td>
                <td width="10%" class="row-index text-left">
                    <input type='number' class='form-control' name="qty[]" value="1"/>
                </td>
                <td class="row-index text-left">
                    <div class="form-row mt-1">
                                <div class="col-md-2">
                                  <input type='text' class='form-control' name="aturan_pakai1[]" id="aturan_pakai1${rowIdxObat}" />
                                </div>
                                <div class="col-md-1">
                                  -
                                </div>
                                <div class="col-md-2">
                                  <input type='text' class='form-control' name="aturan_pakai2[]" id="aturan_pakai2${rowIdxObat}" />
                                </div>
                                <div class="col-md-1">
                                  -
                                </div>
                                <div class="col-md-2">
                                  <input type='text' class='form-control' name="aturan_pakai3[]" id="aturan_pakai3${rowIdxObat}" />
                                </div>
                            </div>
                    <hr>
                    <div class="form-row mt-1">
                        <div class="col-md-6">
                            <select class="form-control form-control-sm select2 cb_ketentuan" name="ketentuan[]" id="ketentuan${rowIdxObat}">
                                <option value="">Pilih</option>
                                @foreach ($ketentuan as $row)
                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                              </select>
                          </div>
                          <div class="col-md-4">
                                  <input type='text' class='form-control' name="keterangan[]" id="keterangan${rowIdxObat}">
                          </div>
                    </div>
                </td>
                <td><a class='btn btn-danger btn-sm btn-delete-obat row-index'
                href='javascript:void(0);'>Hapus</a>
                <input type='hidden' name="kdbrg[]" value="${kode}" />
                <input type='hidden' name="nmbrg[]" value="${name}" />
                <input type='hidden' name="nmracik[]" value="" />
                <input type='hidden' name="qtyracik[]" value="0" />
                </td>

            </tr>`);

            $(".cb_ketentuan").select2();

        }


    });

    $('body').on('click', '.btn-set-obat-racikan', function () {
    let idracik = $(this).data('id');
    let nmracik = $(this).data('nmracik');
    let qtyracik = $(this).data('qtyracik');
    $('#tbody_obat').append(`<tr id="R${++rowIdxObat}">
                        <td width="40%" class="row-index text-left">
                            ${nmracik}<br>
                        </td>
                        <td width="10%" class="row-index text-left">
                            <input type='number' class='form-control' name="qty[]" value="${qtyracik}"/>
                        </td>
                        <td class="row-index text-left">
                            <div class="form-row mt-1">
                                <div class="col-md-2">
                                  <input type='text' class='form-control' name="aturan_pakai1[]" id="aturan_pakai1${rowIdxObat}" />
                                </div>
                                <div class="col-md-1">
                                  -
                                </div>
                                <div class="col-md-2">
                                  <input type='text' class='form-control' name="aturan_pakai2[]" id="aturan_pakai2${rowIdxObat}" />
                                </div>
                                <div class="col-md-1">
                                  -
                                </div>
                                <div class="col-md-2">
                                  <input type='text' class='form-control' name="aturan_pakai3[]" id="aturan_pakai3${rowIdxObat}" />
                                </div>
                            </div>
                            <hr>
                            <div class="form-row mt-1">
                                <div class="col-md-6">
                                    <select class="form-control form-control-sm select2 cb_ketentuan" name="ketentuan[]" id="ketentuan${rowIdxObat}">
                                        <option value="">Pilih</option>
                                        @foreach ($ketentuan as $row)
                                            <option value="{{ $row->id }}">{{ $row->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                        <input type='text' class='form-control' name="keterangan[]" id="keterangan${rowIdxObat}" />
                                </div>
                            </div>
                        </td>
                        <td><a class='btn btn-danger btn-sm btn-delete-obat row-index'
                        href='javascript:void(0);'>Hapus</a>
                        <input type='hidden' name="kdbrg[]" value="${idracik}"/>
                        <input type='hidden' name="nmbrg[]" value="${nmracik}" />
                        <input type='hidden' name="nmracik[]" value="" />
                        <input type='hidden' name="qtyracik[]" value="0" />
                        </td>

                    </tr>`);

                    $(".cb_ketentuan").select2();
                    $("#modalListObatRacikan").modal('hide');
});


    function loadAturanPakai(x)
    {
        console.log(x);
        $("#aturan_pakai"+x).select2({
           // minimumInputLength: 2,
            placeholder: 'Pilih...',
        // width: '350px',
           // allowClear: true,
            ajax: {
                url: "{{ route('aturan-pakai.get-data') }}",
                dataType: 'json',
                cache: false,
                data: function(params) {
                    return {
                        term: params.term || '',
                        page: params.page || 1
                    }
                },

            }
        });
    }

    function loadSediaan(x)
    {
        console.log(x);
        $("#sediaan"+x).select2({
           // minimumInputLength: 2,
            placeholder: 'Pilih...',
        // width: '350px',
           // allowClear: true,
            ajax: {
                url: "{{ route('sediaan.get-data') }}",
                dataType: 'json',
                cache: false,
                data: function(params) {
                    return {
                        term: params.term || '',
                        page: params.page || 1
                    }
                },

            }
        });
    }

    function loadKetentuan(x)
    {
        console.log(x);
        $("#ketentuan"+x).select2({
        //    minimumInputLength: 2,
            placeholder: 'Pilih...',
        // width: '350px',
        //    allowClear: true,
            ajax: {
                url: "{{ route('ketentuan.get-data') }}",
                dataType: 'json',
                cache: false,
                data: function(params) {
                    return {
                        term: params.term || '',
                        page: params.page || 1
                    }
                },

            }
        });
    }
    function loadKeteranganTambahan(x)
    {
        console.log(x);
        $("#keterangan_tambahan"+x).select2({
        //    minimumInputLength: 2,
            placeholder: 'Pilih...',
        // width: '350px',
         //   allowClear: true,
            ajax: {
                url: "{{ route('keterangan-tambahan.get-data') }}",
                dataType: 'json',
                cache: false,
                data: function(params) {
                    return {
                        term: params.term || '',
                        page: params.page || 1
                    }
                },

            }
        });
    }


    $('#tbody_obat').on('click', '.btn-delete-obat', function () {
        // var child = $(this).closest('tr').nextAll();
        // child.each(function () {
        //   var id = $(this).attr('id');
        //   var dig = parseInt(id.substring(1));
        //   $(this).attr('id', `R${dig - 1}`);
        // });
        $(this).closest('tr').remove();
        rowIdxObat--;
    });


    var rowIdxTindakan = 0;
    $('body').on('click', '.btn-set-tindakan', function () {
        var kode = $(this).data('kode');
        var name = $(this).data('name');
        $('#modalTindakan').modal('hide');
        $('#tbody_tindakan').append(`<tr id="R${++rowIdxTindakan}">
            <td class="row-index text-left">
                ${name}
            </td>
            <td class="row-index text-left">
                <input type='text' name="catatan_tindakan[]"/>
            </td>

            <td><a class='btn btn-danger btn-sm btn-delete-tindakan row-index'
            href='javascript:void(0);'>Hapus</a>
            <input type='hidden' name="kdpelayanan[]" value="${kode}" />
            <input type='hidden' name="nmpelayanan[]" value="${name}" />
            </td>
           </tr>`);
    });


    $('#tbody_tindakan').on('click', '.btn-delete-tindakan', function () {
        // var child = $(this).closest('tr').nextAll();
        // child.each(function () {
        //   var id = $(this).attr('id');
        //   var dig = parseInt(id.substring(1));
        //   $(this).attr('id', `R${dig - 1}`);
        // });
        $(this).closest('tr').remove();
        rowIdxTindakan--;
    });

    function updateData()
    {
        var table_subyektif = document.getElementById("tbl_subyektif");
      var tbodyRowCountSubyektif = table_subyektif.tBodies[0].rows.length;
      var table_assesment = document.getElementById("tbl_diagnosa");
      var tbodyRowCountAssesment = table_assesment.tBodies[0].rows.length;

        var tglinput = $("#tglinput").val();
        var tglinput = $("#tglinput").val();
        if(tglinput == '' || tglinput == null){
            Swal.fire("Error!", "Tgl input tidak boleh kosong", "error");
        }
        else if(tbodyRowCountSubyektif < 1){
          Swal.fire("Error!", "Data subyektif tidak boleh kosong", "error");
        }
        else if(tbodyRowCountAssesment < 1){
          Swal.fire("Error!", "Data assesment tidak boleh kosong", "error");
        }
         else{
            $('#frm_rk_medis').submit();
        }

    }

    function templateSoap()
    {
        loadTemplateSoap();
        $('#modalTemplateSoap').modal('show');

        // window.open("{{ url('rekam-medis/cari-template') }}"+'/'+iddokter,'popUpWindow',
        // 'height=600,width=900,left=200,top=50,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,\
        //  status=yes');
    }

    function loadTemplateSoap()
    {
        $("#tbl_template_soap").dataTable().fnDestroy();
        const page_url = "{{ route('rekam-medis.get-template-soap') }}";

        var table = $('#tbl_template_soap').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                type: 'GET'
            },
            columns: [
                { "data": null,"sortable": false,
                            render: function (data, type, row, meta) {
                                    var result = '<a class="btn btn-success btn-set-template" href="#"\
                                    data-id="'+row.id+'"\
                                    data-nmresep="'+row.nmresep+'"\
                                    >Pilih</a>';
                                    return result;
                            }
                        },
                {data: 'soap_subyektifs', name: 'soap_subyektifs', searchable: true},
                {data: 'soap_assesments', name: 'soap_assesments', searchable: true},
                {data: 'soap_plannings', name: 'soap_plannings', searchable: false}
        ],
            responsive: true
        });
    }

    $('body').on('click', '.btn-resep', function () {
      loadResep();
       $('#modalResep').modal('show');

    });

    function loadResep()
    {
        $("#tbl_list_resep").dataTable().fnDestroy();

        const page_url = "{{ route('template-soap.get-mresep') }}";
        var table = $('#tbl_list_resep').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                type: 'GET'
            },
            columns: [
                { "data": null,"sortable": false,
                            render: function (data, type, row, meta) {
                                    var result = '<a class="btn btn-success btn-set-resep" href="#"\
                                    data-idtempobat="'+row.idtempobat+'"\
                                    data-nmtempobat="'+row.nmtempobat+'"\
                                    >Pilih</a>';
                                    return result;
                            }
                        },
                {data: 'nmtempobat', name: 'nmtempobat', orderable: true,searchable: true}
            ],
            responsive: true,
            columnDefs: [

            ],

        });
    }


     $('body').on('click', '.btn-set-resep', function () {
        $('#tbl_obat tbody').empty();
        var nmtempobat = $(this).data('nmtempobat');
        $('#nmresep').val(nmtempobat);

        $.ajax({
            type: "GET",
            url: "{{ route('template-soap.get-mresep-det') }}",
            data: {
                idtempobat : $(this).data('idtempobat')
            },
            success: function(result){
                                for(i = 0; i < result.total; i++){
                                  currentdata = result.data[i];
                                  var nmracik;
                                  if(currentdata.nmracikan == null || currentdata.nmracikan == 'undefined' || currentdata.nmracikan == '')
                                  {
                                      nmcarik = 'Non Racik';
                                  } else{
                                      nmracik = currentdata.nmracikan;
                                  }
                                  var item ="\
                                 <tr id='R"+i+"'>\
                                    <td width='40%' class='row-index text-left'>\
                                        "+currentdata.get_brg.nmbrg+"\
                                    </td>\
                                    <td class='row-index text-left'>\
                                        <input type='text' name='jenis_obat[]' value='"+nmracik+"'/>\
                                    </td>\
                                    <td width='10%' class='row-index text-left'>\
                                        <input type='text' class='form-control' name='qty[]' value='"+currentdata.qtyitem+"'/>\
                                    </td>\
                                    <td class='row-index text-left'>\
                                        <input type='text' name='catatan_obat[]'/>\
                                    </td>\
                                    <td><a class='btn btn-danger btn-sm btn-delete-obat row-index'\
                                    href='javascript:void(0);'>Hapus</a>\
                                    <input type='hidden' name='kdbrg[]' value='"+currentdata.kdbrg+"' />\
                                    <input type='hidden' name='nmbrg[]' value="+currentdata.get_brg.nmbrg+" />\
                                    </td>\
                                </tr>\
                                  ";
                                  $('#tbl_obat tbody').append(item);
                            }


                    $('#modalResep').modal('hide');
            } ,error: function(xhr, status, error) {
                alert(error);
            },

        });

        $('#modalResep').modal('hide');


    });

    function downloadFile(id){
            var url = "{{url('rekam-medis/get_file_pasien')}}"+"/"+id
        window.open(url);
    }


    var rowIdxObatRacikan = 0;
    $('body').on('click', '.btn-add-obat-in-racikan', function () {
        $('#tbody_obat_inracikan').append(`<tr id="R${++rowIdxObatRacikan}">
            <td width="50%" class="row-index text-left">
                <select class="form-control form-control-sm select2 cb_obat" name="obat[]" id="nmobat${rowIdxObatRacikan}">
                </select>
            </td>
            <td class="row-index text-left">
                <input type='text' class="form-control" name="qty[]" value="1" id="qty${rowIdxObatRacikan}"/>
            </td>
            <td><a class='btn btn-danger btn-sm btn-delete-obat-in-racikan row-index'
            href='javascript:void(0);'>Hapus</a>
            <input type='hidden' name="nmobat_real[]" id="nmobat_real${rowIdxObatRacikan}" />
            </td>

           </tr>`);
           loadObatSelect2(rowIdxObatRacikan);
    });


    $('#tbody_obat_inracikan').on('click', '.btn-delete-obat-in-racikan', function () {
        var child = $(this).closest('tr').nextAll();
        child.each(function () {
          var id = $(this).attr('id');
          var dig = parseInt(id.substring(1));
          $(this).attr('id', `R${dig - 1}`);
        });
        $(this).closest('tr').remove();
        rowIdxObatRacikan--;
    });

    function loadObatSelect2(x)
    {
        $("#nmobat"+x).select2({
            minimumInputLength: 2,
            placeholder: 'Pilih Obat...',
        // width: '350px',
            allowClear: true,
            ajax: {
                url: "{{ route('obat.get-data') }}",
                dataType: 'json',
                cache: false,
                data: function(params) {
                    return {
                        term: params.term || '',
                        page: params.page || 1
                    }
                },

            }
        }).on('select2:select', function (e) {
            var data = e.params.data;
            $("#nmobat_real"+x).val(data.text)

        });
    }


$('body').on('click', '.btn-add-obat-racikan', function () {
    var table_racikan = document.getElementById("tbl_obat_inracikan");
      var tbodyRowCountObt = table_racikan.tBodies[0].rows.length;
      const dokter = $("#iddokter").val();
     const nmracik = $("#nmracik").val();
     const qtyracik = $("#qtyracik").val();
     if(nmracik == '' || qtyracik == ''){
        Swal.fire("Error!", "Nama racikan dan qty racikan tidak boleh kosong", "error");
     } else if(tbodyRowCountObt < 1){
        Swal.fire("Error!", "Obat belum ditambahkan", "error");
     }else if(dokter == ''){
        Swal.fire("Error!", "Dokter belum dipilih", "error");
     } else{
      var form_data = new FormData();
              form_data.append("nmracik", nmracik);
              form_data.append("qtyracik", qtyracik);
              form_data.append("iddokter", dokter);
              var rows = document.querySelectorAll('#tbl_obat_inracikan tbody tr');
                for (let i = 1; i <= rows.length; i++) {
                            let nmobat =  $("#nmobat"+i).val(); //rows[i];
                            let qty =  $("#qty"+i).val(); //rows[i];
                            form_data.append('nmobat[]', nmobat);
                            form_data.append("qty[]", qty);
                }
        $.ajax({
              type: "POST",
              url: "{{ route('mracikan.store') }}"+'?_token=' + '{{ csrf_token() }}',
              data: form_data,
              dataType: "json",
              contentType: false,
              cache : false,
              processData : false,
              success: function(result){
                  var oTable = $('#tbl_list_obat_racikan').dataTable();
                      oTable.fnDraw(false);
                      $('#modalObatRacikan').modal('hide');
                      $('#modalListObatRacikan').modal('show');
              },error: function(xhr, status, error) {
                  alert(error);
              }
          });
     }


});




</script>
@endpush
