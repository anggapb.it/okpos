@extends('layouts.main')

@section('page.title', 'Rekam Medis SOAP')
@section('page.heading', 'Rekam Medis SOAP')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                 {{Session::get('message')}}
            </div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                {{Session::get('error')}}
            </div>
        @endif
           <form method="POST" action="{{ route('rekam-medis.store') }}" id="frm_rk_medis" class="form-horizontal" enctype="multipart/form-data">
            @csrf

                 <div class="button-group">
                {{-- <button type="button" class="btn waves-effect waves-light btn-primary" onclick="inputBaru()">Input Baru</button> --}}
                <input type="hidden" class="form-control form-control-sm" autocomplete="off" id="noreg" name="noreg">
                {{-- <a href="{{ route('template-soap') }}" class="btn waves-effect waves-light btn-warning">Kembali</a> --}}
                </div>
                 <div class="form-row mt-1">
                        <label class="col-md-2 text-right">No RM <span class="text-danger">*</span></label>
                        <div class="col-md-2">
                            <input type="text" class="form-control form-control-sm" autocomplete="off" id="norm" name="norm" readOnly>
                          </div>
                           <div class="col-md-1">
                          <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-pasien">Cari</button>
                          </div>
                          <label class="col-md-2 text-right">Nama Pasien</label>
                          <div class="col-md-4">
                            <input type="text" class="form-control form-control-sm" autocomplete="off" id="nmpasien" name="nmpasien" readOnly>
                          </div>


                    </div>
                 <div class="form-row mt-1">
                        <label class="col-md-2 text-right">Jenis Kelamin</label>
                        <div class="col-md-1">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="jkelamin" name="jkelamin" readOnly>
                        </div>
                        <label class="col-md-4 text-right">Tanggal Lahir</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control form-control-sm" autocomplete="off" id="tgllahir" name="tgllahir" readOnly>
                          </div>


                    </div>
                 <div class="form-row mt-1">
                    <label class="col-md-2 text-right">Usia</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="usia" name="usia" readOnly>
                    </div>
                    <label class="col-md-2 text-right">Dokter</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="dokter" name="dokter" readOnly>
                    </div>

                    </div>
                    <hr>
                     <hr>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#riw_rkmedis" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">History rekam medis</span></a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#riw_kunjungan" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">History Kunjungan</span></a> </li>
                    </ul>
                     <div class="tab-content tabcontent-border">
                        <div class="tab-pane active" id="riw_rkmedis" role="tabpanel">
                            <div class="p-20">
                                   <div class="table-responsive">
                                    <table class="table" id="tbl_rekammedis_pasien">
                                        <thead>
                                            <tr>
                                                <th>Opsi</th>
                                                <th>Dokter</th>
                                                <th>Tanggal</th>
                                                <th>Subyektif (S)</th>
                                                <th>Obyektif (O)</th>
                                                <th>&nbsp;</th>
                                                <th>Assesment (A)</th>
                                                <th>Planning (P)</th>

                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane p-20" id="riw_kunjungan" role="tabpanel">
                            <div class="p-20">
                                <div class="table-responsive">
                                    <table class="table" id="tbl_kunjungan_pasien">
                                        <thead>
                                            <tr>
                                                <th>Tgl. Reg</th>
                                                <th>No. Reg</th>
                                                <th>Poli</th>
                                                <th>Dokter</th>
                                                <th>Status Rk. medis</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>


           </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalPasienRegistrasi" tabindex="-1" role="dialog" aria-labelledby="modalPasienLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">Data Pasien</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-5">
                <h5 class="box-title m-t-30">Periode</h5>

                <div class="input-daterange input-group" id="date-range">
                    <input type="text" autocomplete="off" class="form-control" id="tglawal" />
                    <div class="input-group-append">
                        <span class="input-group-text bg-info b-0 text-white">s/d</span>
                    </div>
                    <input type="text" autocomplete="off" class="form-control" id="tglakhir" />
                </div>
              </div>
              <div class="col-mt-1">
                <h5 class="box-title m-t-30">&nbsp;</h5>
                <button type="button" id="btn_cari" class="btn btn-primary">Cari</button>
              </div>
            </div>
              <div class="table-responsive">
                  <table class="table table-bordered" id='tbl_pasien_registrasi' width="100%">
                  <thead>
                    <tr>
                      <th class="text-center">Aksi</th>
                      <th class="text-center">No. Register</th>
                      <th class="text-center">Tgl Register</th>
                      <th class="text-center">No RM</th>
                      <th class="text-center">Nama</th>
                      <th class="text-center">L/P</th>
                      <th class="text-center">Tanggal Lahir</th>
                      <th class="text-center">No. HP</th>
                      <th class="text-center">Status Rekam Medis</th>
                    </tr>
                  </thead>
                </table>
              </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
  </div>
@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){
  $('#tglawal').datepicker('update', new Date());
    $('#tglakhir').datepicker('update', new Date());
});
 $('body').on('click', '.btn-pasien', function () {
    loadPasienReg();
       $('#modalPasienRegistrasi').modal('show');

    });

function loadPasienReg(){
    $("#tbl_pasien_registrasi").dataTable().fnDestroy();

    const page_url = "{{ route('rekam-medis.list-reg') }}";

    var table = $('#tbl_pasien_registrasi').DataTable({
          processing: true,
          serverSide: true,
          ajax: {
              url: page_url,
              type: 'GET',
              data: {
                        tglawal :  $("#tglawal").val(),
                        tglakhir :  $("#tglakhir").val()
                }
          },
          columns: [
            { data: 'noreg', sortable: false,
                        render: function (data, type, row, meta) {
                            if(row.st_btn_periksa == 1){
                                var result = '<a class="btn btn-success btn-sm btn-pilih-pasien" href="#"\
                                data-norm="'+row.get_registrasi.norm+'"\
                                data-nmpasien="'+row.get_registrasi.get_pasien.nmpasien+'"\
                                data-tgllahir="'+row.get_registrasi.get_pasien.tgllahir+'"\
                                data-jkelamin="'+row.get_registrasi.get_pasien.get_jkelamin.kdjnskelamin+'"\
                                data-dokter="'+row.get_dokter.nmdoktergelar+'"\
                                data-noreg="'+row.noreg+'"\
                                >Pilih</a>';
                                return result;
                            }else{
                                return '';
                            }

                         }
                    },
            {data: 'noreg', name: 'noreg', orderable: false,searchable: true},
            {data: 'tglreg', name: 'tglreg', orderable: false,searchable: true},
            {data: 'get_registrasi.norm', name: 'get_registrasi.norm', orderable: false,searchable: true},
            {data: 'get_registrasi.get_pasien.nmpasien', name: 'get_registrasi.get_pasien.nmpasien', orderable: true,searchable: true},
            {data: 'get_registrasi.get_pasien.get_jkelamin.kdjnskelamin', name: 'get_registrasi.get_pasien.get_jkelamin.kdjnskelamin', orderable: false,searchable: true},
            {data: 'get_registrasi.get_pasien.tgllahir', name: 'get_registrasi.get_pasien.tgllahir', orderable: false,searchable: true},
            {data: 'get_registrasi.get_pasien.nohp', name: 'get_registrasi.get_pasien.nohp', orderable: false,searchable: true},
            {data: 'st_rkmedis', name: 'st_rkmedis', orderable: false,searchable: true},
          ],
          responsive: true,
          columnDefs: [
              {
                // render:function(data){
                //     return moment(data).format('MM-DD-YYYY');
                // }, targets:[2],
              }
          ],
         // dom: 't',

      });
}

$('body').on('click', '.btn-pilih-pasien', function () {
        $("#norm").val($(this).data('norm'));
        $("#nmpasien").val($(this).data('nmpasien'));
        $("#tgllahir").val($(this).data('tgllahir'));
        $("#dokter").val($(this).data('dokter'));
        $("#jkelamin").val($(this).data('jkelamin'));
        $("#noreg").val($(this).data('noreg'));
        umur(new Date($(this).data('tgllahir')));
        loadRekamMedisPasien($(this).data('norm'));
        loadKunjunganPasien($(this).data('norm'));

        $('#modalPasienRegistrasi').modal('hide');
    });

function umur(val) {
		var date = new Date();
		var td = date.getDate();var d = val.getDate();
		var tm = date.getMonth();var m = val.getMonth();
		var ty = date.getFullYear();var y = val.getFullYear();

		if(td-d<0){
			day=(parseInt(td)+30)-d;
			tm--;
		}
		else{
			day=td-d;
		}
		if(tm-m<0){
			month=(parseInt(tm)+12)-m;
			ty--;
		}
		else{
			month=tm-m;
		}
		year=ty-y;
        $("#usia").val(year+' Tahun '+month+' Bulan '+day+' Hari');
	}

    function inputBaru()
    {
        var noreg = $("#noreg").val();
        if(noreg == '' || noreg == null){
            Swal.fire("Error!", "Pasien belum dipilih", "error");
        }else{
            window.location = "{{ url('rekam-medis/create') }}"+'/'+noreg;
        }

    }


function loadRekamMedisPasien(pnorm)
{
        $("#tbl_rekammedis_pasien").dataTable().fnDestroy();
        var page_url = "{{ route('rekam-medis.list-data-bypasien') }}";

        var table = $('#tbl_rekammedis_pasien').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                type: 'GET',
                data: {
                norm :  pnorm,
              }
            },
            columns: [
                { "data": null,"sortable": false,
                    render: function (data, type, row, meta) {
                        var result = '<a class="btn waves-effect waves-light btn-primary btn-sm" href="{{ url('rekam-medis/edit').'/' }}'+row.id+'">Periksa</a>';
                      //  result += '<hr><a class="btn waves-effect waves-light btn-danger btn-sm" href="javascript:delete_data('+row.id+')">Hapus</a>';

                        return result;
                    }
                },
                {data: 'dokter.nmdoktergelar', name: 'dokter.nmdoktergelar', searchable: true},
                {data: 'tgl_rekam_medis', name: 'tgl_rekam_medis', searchable: true},
                {data: 'soap_subyektifs', name: 'soap_subyektifs', searchable: true},
                {data: 'soap_obyektif', name: 'soap_obyektif', searchable: true},
                {data: 'soap_penunjangs', name: 'soap_penunjangs', searchable: true, sortable: false},
                {data: 'soap_assesments', name: 'soap_assesments', searchable: true},
                {data: 'soap_plannings', name: 'soap_plannings', searchable: true},


        ],
            responsive: true
        });

}

function loadKunjunganPasien(pnorm)
{
        $("#tbl_kunjungan_pasien").dataTable().fnDestroy();
        var page_url = "{{ route('rekam-medis.list-kunjungan-bypasien') }}";

        var table = $('#tbl_kunjungan_pasien').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                type: 'GET',
                data: {
                norm :  pnorm,
              }
            },
            columns: [
                {data: 'get_reg_det.tglreg', name: 'get_reg_det.tglreg', searchable: true},
                {data: 'noreg', name: 'noreg', searchable: true},
                {data: 'get_reg_det.get_dokter.nmdoktergelar', name: 'get_reg_det.get_dokter.nmdoktergelar', searchable: true},
                {data: 'get_reg_det.get_bagian.nmbagian', name: 'get_reg_det.get_bagian.nmbagian', searchable: true, sortable: false},
                {data: 'st_rkmedis', name: 'st_rkmedis', searchable: true}
        ],
            responsive: true
        });

}

function delete_data(id){
     var r = confirm("Apakah anda akan menghapus data ini?");
     var page_url = "{{ url('rekam-medis/hapus') }}"+'/'+id;
     if(r == true){
             $.ajax({
              type: "GET",
              url:page_url,
              //data: form_data,
              datatype: "json",
              contentType: false,
              cache: false,
              processData: false,
              beforeSend:function(){
            //   $('#uploaded_file'+x).html("<label class='text-danger'>Sedang Menghapus, Mohon Tunggu . . .</label>");
              },
              success:function(data)
              {
                var oTable = $('#tbl_rekammedis_pasien').dataTable();
                    oTable.fnDraw(false);
              }
          });
     }

  }


  $('#btn_cari').click(function(){
          $('#tbl_pasien_registrasi').DataTable().destroy();
          loadPasienReg();
});

function downloadFile(id){
            var url = "{{url('rekam-medis/get_file_pasien')}}"+"/"+id
        window.open(url);
    }
</script>
@endpush
