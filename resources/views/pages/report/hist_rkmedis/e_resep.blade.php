<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>E-RESEP</title>
</head>
<style>
    #customers {
      font-family: Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    #customers td, #customers th {
      border: 1px solid #ddd;
      padding: 8px;
    }

    #customers tr:nth-child(even){background-color: #f2f2f2;}

    #customers tr:hover {background-color: #ddd;}

    #customers th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      background-color: #4CAF50;
      color: white;
    }

    h3 {text-align: center;}

    p {
          font-family: Arial, Helvetica, sans-serif;
          font-size:5px;
          margin-top:3px;
        }
        .outside {
            border: 1px solid black;
            padding: 5px;
            margin: 0px;
            }
        .right{
            float:right;
            border: 1px solid black;
            padding: 1px;
            margin: 0px;
        }
        .left{
            float:left;
            border: 1px solid black;
            padding: 1px;
            margin: 0px;
        }
        .collapse {

            border-collapse: collapse;
        }
        .barcode{
            float:right;
            border: 1px solid black;
            padding: 0px;
            margin: 0px;
        }

    </style>
<body>

        @php
            if($rekam_medis_soap->soap_obyektif->pf_bb){
                $bb = $rekam_medis_soap->soap_obyektif->pf_bb;
            }else{
                $bb = '..........';
            }
            if($rekam_medis_soap->soap_obyektif->pf_tb){
                $tb = $rekam_medis_soap->soap_obyektif->pf_tb;
            }else{
                $tb = '..........';
            }
        @endphp
        <table cellpadding="0" style="line-height:1">
            <tr>
                <td><p style="font-size:10px;"><b>RESEP</b></p></td>
                <td><img src="{{ url('logo/logo.png') }}" /></td>
            </tr>
            <tr>
                <td class="left"><p>BB / TB: {{ $bb }} KG/  {{ $tb }} cm</p></td>
                <td><center><p  style="font-size:7px;"><b>Klinik Padjadjaran Medical Center</b>
                    <br>Jln. Pajajaran No. 149, Bandung Telp.(022) 20586889</p></center></td>
            </tr>
        </table>


        <table  width="100%" class="left" cellpadding="0" style="line-height:0">
            <tr>
                <td><p style="font-size:6px;">Nama Pasien</p></td>
                <td><p style="font-size:6px;">:</p></td>
                <td><p style="font-size:8px;">{{ $rekam_medis_soap->pasien->nmpasien }} ({{ $rekam_medis_soap->pasien->get_jkelamin->kdjnskelamin}})</p></td>
            </tr>
            <tr>
                <td><p style="font-size:6px;">No. RM</p></td>
                <td><p style="font-size:6px;">:</p></td>
                <td><p style="font-size:8px;">{{ $rekam_medis_soap->pasien->norm }}</p></td>
            </tr>
             <tr>
                <td><p style="font-size:6px;">Tanggal Lahir</p></td>
                <td><p style="font-size:6px;">:</p></td>
                <td><p style="font-size:8px;">{{ $rekam_medis_soap->pasien->tgllahir }}</p></td>
                <td><p style="font-size:6px;">Umur</p></td>
                <td><p style="font-size:6px;">:</p></td>
                <td><p style="font-size:8px;">{{ $rekam_medis_soap->regdet->umurtahun }} Tahun</p></td>
            </tr>
              <tr>
                <td><p style="font-size:6px;">Nama Dokter</p></td>
                <td><p style="font-size:6px;">:</p></td>
                <td><p style="font-size:8px;">{{ $rekam_medis_soap->dokter->nmdoktergelar }}</p></td>
            </tr>
             <tr>
                <td><p style="font-size:6px;">Poliklinik</p></td>
                <td><p style="font-size:6px;">:</p></td>
                <td><p style="font-size:8px;">{{ $rekam_medis_soap->regdet->get_bagian->nmbagian }}</p></td>
            </tr>
             <tr>
                <td><p style="font-size:12px;">ITER</p></td>
                <td><p style="font-size:12px;">:</p></td>
                <td><p style="font-size:12px;">{{ !empty($rekam_medis_soap->iter) ? $rekam_medis_soap->iter : '' }}</p></td>
            </tr>
        </table>


        <p>&nbsp;</p>



            <span style="float:left"><p>Riwayat Alergi Obat<br><span style='font-size:10px;'>&#9633;</span> Ya, Nama Obat<br><span style='font-size:10px;'>&#9633;</span> Tidak</p>

                @foreach ($rekam_medis_soap->soap_plannings as $soap_planning)
                    @php
                         if($soap_planning->aturan_pakai){
                                $aturanpakai = $soap_planning->aturan_pakai;
                            }else{
                                $aturanpakai = ' ....';
                            }
                    @endphp
                    @php
                         if($soap_planning->catatan){
                                $catatan = $soap_planning->catatan;
                            }else{
                                $catatan = ' ....';
                            }
                    @endphp
                 @if(substr($soap_planning->kd_item,0,1) !== 'T')
                    <p style="font-size:9px">R\ {{ $soap_planning->nm_item. ' No. '.$soap_planning->qty }}</p>
                    @if($soap_planning->obat_racikan_det()->exists())
                    @foreach ($soap_planning->obat_racikan_det as $obat_racikan_dt)
                       <span><p style="font-size:8px">{{ $obat_racikan_dt->nm_item }} ({{ $obat_racikan_dt->qty }})</p></span>
                    @endforeach
                   @endif
                    <p style="font-size:9px">S {{ $aturanpakai }}</p>
                    @if($soap_planning->waktu_pakai()->exists())
                    <p style="font-size:9px">{{ $soap_planning->waktu_pakai->name }}</p>
                    @endif
                    <p style="font-size:9px">{{ $catatan }}</p>

                @endif
                @endforeach


            </span><br>


           <span style="float:right">
            <center><p>TELAAH RESEP</p></center>
            <table border="1" class="collapse" cellpadding="0" style="line-height:0">
                <tr>
                   <td><p>ASPEK TELAAH</p></td>
                   <td  width="15px"><p>YA</p></td>
                   <td  width="15px"><p>TIDAK</p></td>
                </tr>
                <tr>
                    <td><p>Kejelasan Tulisan</p></td>
                    <td></td>
                    <td></td>

                </tr>
                <tr>
                    <td><p>Benar Identitas Pasien</p></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><p>Benar nama obat</p></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><p>Benar kekuatan obat</p></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><p>Benar bentuk kesediaan</p></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><p>Benar Jumlah</p></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><p>Benar Dosis</p></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><p>Benar aturan pakai</p></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><p>Duplikasi obat</p></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><p>Interaksi obat</p></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><p>Berat Badan</p></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><p>Kontra Indikasi</p></td>
                    <td></td>
                    <td></td>
                </tr>

            </table>
            <center><p>INFORMASI OBAT</p></center>
            <table border="1" class="collapse" cellpadding="0" style="line-height:1">
                <tr>
                    <td width="150px"><p>&nbsp;</p></td>
                    <td width="15px"><p>&nbsp;</p></td>
                    <td width="15px"><p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td><p>Nama obat sesuai dengan resep</p></td>
                    <td></td>
                    <td></td>

                </tr>
                <tr>
                    <td><p>Jumlah/dosis obat sesuai dengan resep</p></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><p>Cara pemakaian obat sesuai dengan resep</p></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><p>Waktu pemakaian obat sesuai dengan resep</p></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><p>Penyimpanan obat</p></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><p style="font-size:6px;">Pasien sudah mendapat informasi obat dan telah mengerti</p></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="3"><center><p style="font-size:7px;">KETERANGAN TAMBAHAN<br><br></p></center></td>
                </tr>
            </table>
                <table border="1" class="collapse" cellpadding="0" style="line-height:1">
                <tr>
                    <td width="100px"><center><p style="font-size:7px;">AA VERIF</p></center></td>
                    <td width="90px"><center><p style="font-size:7px;">APOTEKER VERIF</p></center></td>
                </tr>
                <tr>
                    <td><center><p style="font-size:8px;">&nbsp;<br></p></center></td>
                    <td><center><p style="font-size:8px;">&nbsp;<br></p></center></td>
                </tr>

            </table>
        </table>
        <table class="outside" cellpadding="0" style="line-height:1">
        <tr>
            <td width="177px"><center><p style="font-size:7px;">Telah menerima obat dan informasi</p></center></td>

        </tr>

        <tr>
            <td><center><p style="font-size:7px;">(.................................................)<br>Tanda Tangan dan Nama Jelas</p></center></td>
        </tr>
        <tr>
            <td> {!! DNS2D::getBarcodeHTML('"'.$rekam_medis_soap->regdet->noreg.' - '.$rekam_medis_soap->dokter->nmdoktergelar.' - '.$rekam_medis_soap->dokter->sip.'"', 'QRCODE',2.5,2.5) !!}</td>
        </tr>

    </table>

            </span>




           <br>
           <br>
           <br>

           <br>
           <br>
           <br>
           <br>
           <br>
           <br>
           <br>
           <br>
           <br>
           <br>
           <br>
           <br>
           <br>
           <br>
           <br>
           <br>
           <br>



            <p style="font-size:8px;">Bandung, {{ date("d/m/Y")}}</p>
</body>
</html>
