@extends('layouts.main')

@section('page.title', 'History Rk. Medis')
@section('page.heading', 'History rekam medis per pasien')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                 {{Session::get('message')}}
            </div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                {{Session::get('error')}}
            </div>
        @endif
           <form method="POST" action="{{ route('rekam-medis.store') }}" id="frm_rk_medis" class="form-horizontal" enctype="multipart/form-data">
            @csrf

                 <div class="button-group">
                {{-- <a href="{{ route('template-soap') }}" class="btn waves-effect waves-light btn-warning">Kembali</a> --}}
                </div>
                 <div class="form-row mt-1">
                        <label class="col-md-2 text-right">No RM <span class="text-danger">*</span></label>
                        <div class="col-md-2">
                            <input type="text" class="form-control form-control-sm" autocomplete="off" id="norm" name="norm" readOnly>
                          </div>
                           <div class="col-md-1">
                          <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-pasien">Cari</button>
                          </div>
                          <label class="col-md-2 text-right">Nama Pasien</label>
                          <div class="col-md-4">
                            <input type="text" class="form-control form-control-sm" autocomplete="off" id="nmpasien" name="nmpasien" readOnly>
                          </div>


                    </div>
                 <div class="form-row mt-1">
                        <label class="col-md-2 text-right">Jenis Kelamin</label>
                        <div class="col-md-1">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="jkelamin" name="jkelamin" readOnly>
                        </div>
                        <label class="col-md-4 text-right">Tanggal Lahir</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control form-control-sm" autocomplete="off" id="tgllahir" name="tgllahir" readOnly>
                          </div>


                    </div>
                 <div class="form-row mt-1">
                    <label class="col-md-2 text-right">Usia</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="usia" name="usia" readOnly>
                    </div>

                    </div>
                    <hr>

                    <div class="table-responsive m-t-10">
                        <a class="btn btn-primary" href="javascript:void" onclick="export_pdf()">Export to PDF</a>
                        <table id="tbl_rekammedis_pasien" class="table table-bordered table-striped display">
                            <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Subyektif (S)</th>
                                    <th>Obyektif (O)</th>
                                    <th>&nbsp;</th>
                                    <th>Assesment (A)</th>
                                    <th>Planning (P)</th>
                                    <th>Paraf</th>
                                    <th>Aksi</th>

                                </tr>
                            </thead>


                        </table>
                    </div>
           </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalPasien" tabindex="-1" role="dialog" aria-labelledby="modalPasienLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">Data Pasien</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
              <div class="table-responsive">
                  <table class="table table-bordered" id='tbl_pasien' width="100%">
                  <thead>
                    <tr>
                      <th class="text-center">Aksi</th>
                      <th class="text-center">No RM</th>
                      <th class="text-center">Nama</th>
                      <th class="text-center">L/P</th>
                      <th class="text-center">Tanggal Lahir</th>
                      <th class="text-center">No. HP</th>
                    </tr>
                  </thead>
                </table>
              </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
  </div>
@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){
    loadPasien();
});
 $('body').on('click', '.btn-pasien', function () {
       $('#modalPasien').modal('show');
    });

function loadPasien(){
    $("#tbl_pasien").dataTable().fnDestroy();

    const page_url = "{{ route('rekam-medis.list-pasien') }}";

    var table = $('#tbl_pasien').DataTable({
          processing: true,
          serverSide: true,
          ajax: {
              url: page_url,
              type: 'GET'
          },
          columns: [
            {data: 'norm',sortable: false,
                        render: function (data, type, row, meta) {
                                var result = '<a class="btn btn-success btn-sm btn-pilih-pasien" href="#"\
                                data-norm="'+row.norm+'"\
                                data-nmpasien="'+row.nmpasien+'"\
                                data-tgllahir="'+row.tgllahir+'"\
                                data-jkelamin="'+row.get_jkelamin.kdjnskelamin+'"\
                                >Pilih</a>';
                                return result;
                         }
                    },
            {data: 'norm', name: 'norm', orderable: false,searchable: true},
            {data: 'nmpasien', name: 'nmpasien', orderable: true,searchable: true},
            {data: 'get_jkelamin.kdjnskelamin', name: 'get_jkelamin.kdjnskelamin', orderable: false,searchable: false},
            {data: 'tgllahir', name: 'tgllahir', orderable: false,searchable: false},
            {data: 'nohp', name: 'nohp', orderable: false,searchable: false},
          ],
          responsive: true,
          columnDefs: [
              {
                // render:function(data){
                //     return moment(data).format('MM-DD-YYYY');
                // }, targets:[2],
              }
          ],
         // dom: 't',

      });
}

$('body').on('click', '.btn-pilih-pasien', function () {
        $("#norm").val($(this).data('norm'));
        $("#nmpasien").val($(this).data('nmpasien'));
        $("#tgllahir").val($(this).data('tgllahir'));
        $("#jkelamin").val($(this).data('jkelamin'));
        umur(new Date($(this).data('tgllahir')));
        loadRekamMedisPasien($(this).data('norm'));

        $('#modalPasien').modal('hide');
    });

function umur(val) {
		var date = new Date();
		var td = date.getDate();var d = val.getDate();
		var tm = date.getMonth();var m = val.getMonth();
		var ty = date.getFullYear();var y = val.getFullYear();

		if(td-d<0){
			day=(parseInt(td)+30)-d;
			tm--;
		}
		else{
			day=td-d;
		}
		if(tm-m<0){
			month=(parseInt(tm)+12)-m;
			ty--;
		}
		else{
			month=tm-m;
		}
		year=ty-y;
        $("#usia").val(year+' Tahun '+month+' Bulan '+day+' Hari');
	}

    function inputBaru()
    {
        var noreg = $("#noreg").val();
        if(noreg == '' || noreg == null){
            Swal.fire("Error!", "Pasien belum dipilih", "error");
        }else{
            window.location = "{{ url('rekam-medis/create') }}"+'/'+noreg;
        }

    }


function loadRekamMedisPasien(pnorm)
{
        $("#tbl_rekammedis_pasien").dataTable().fnDestroy();
        var page_url = "{{ route('rekam-medis.list-data-bypasien-rpt') }}";
        var nmpasien = $('#nmpasien').val();
        var jkelamin = $('#jkelamin').val();
        var table = $('#tbl_rekammedis_pasien').DataTable({
        //    dom: 'B<br>lfrtip',
        //     buttons: [
        //         {
        //             extend: 'pdf',
        //             title: 'History Rekam Medis '+nmpasien,
        //            // messageTop:'Jenis Kelamin : '+jkelamin+'<br>',
        //             filename: 'History Rk. Medis '+nmpasien
        //         }
        //     ],
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                type: 'GET',
                data: {
                    norm :  pnorm,
                }
            },
            columns: [
                {data: 'tgl_rekam_medis', name: 'tgl_rekam_medis', searchable: true},
                {data: 'soap_subyektifs', name: 'soap_subyektifs', searchable: true},
                {data: 'soap_obyektif', name: 'soap_obyektif', searchable: true},
                {data: 'soap_penunjangs', name: 'soap_penunjangs', searchable: true, sortable: false},
                {data: 'soap_assesments', name: 'soap_assesments', searchable: true},
                {data: 'soap_plannings', name: 'soap_plannings', searchable: true},
                {data: 'dokter.nmdoktergelar', name: 'dokter.nmdoktergelar', searchable: true},
                { "data": null,"sortable": false,
                        render: function (data, type, row, meta) {
                            var result = '<a class="btn btn-info btn-sm" href="javascript:void();" onclick="e_resep('+row.id+')"\
                                >E-Resep</a>';
                                return result;
                         }
                    },


         ]
        });

}

function downloadFile(id){
            var url = "{{url('rekam-medis/get_file_pasien')}}"+"/"+id
        window.open(url);
    }

function export_pdf()
{
 let norm = $("#norm").val();
 window.open("{{ url('hist-rkmedis-pasien/pdf') }}"+'/'+norm);
}

function e_resep(id)
{

 window.open("{{ url('hist-rkmedis-pasien/e_resep') }}"+'/'+id, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=200,width=500,height=550");
}


</script>
@endpush
