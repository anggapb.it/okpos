<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Rekam Medis SOAP</title>
</head>
<style>
    #customers {
      font-family: Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    #customers td, #customers th {
      border: 1px solid #ddd;
      padding: 8px;
    }

    #customers tr:nth-child(even){background-color: #f2f2f2;}

    #customers tr:hover {background-color: #ddd;}

    #customers th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      background-color: #4CAF50;
      color: white;
    }

    h3 {text-align: center;}
    </style>
<body>

    <h3>E-Rekam medis SOAP</h3>
    <p>Norm : {{ $pasien->norm }} </p>
    <p>Nama : {{ $pasien->nmpasien }}</p>
    <br>
    <table id="customers">
        <tr>
            <th>Tanggal</th>
            <th>Subyektif (S)</th>
            <th>Obyektif (O)</th>
            <th>Assesment (A)</th>
            <th>Planning (P)</th>
            <th>Paraf</th>
        </tr>

            @foreach ($rekam_medis_soap as $rkmedis)
            <tr>
            <td>{{ date("d-m-Y", strtotime($rkmedis->tgl_rekam_medis)) }}</td>
            <td>@foreach ($rkmedis->soap_subyektifs as $soap_subyektif)
                <p>{{ $soap_subyektif->subyektif.' '.$soap_subyektif->keterangan }}</p>
                @endforeach
            </td>
            <td
                <p>Tensi : {{ $rkmedis->soap_obyektif->ttv_tek_darah }}, Nadi : {{ $rkmedis->soap_obyektif->ttv_nadi }} </p>
                <p>Suhu : {{ $rkmedis->soap_obyektif->ttv_suhu }}, TB : {{ $rkmedis->soap_obyektif->pf_tb }} </p>
                <p>BB : {{ $rkmedis->soap_obyektif->pf_bb }}, IMT : {{ $rkmedis->soap_obyektif->pf_imt }} </p>
                <hr>
                @foreach ($rkmedis->soap_penunjangs as $soap_penunjang)
                <p>{{ $soap_penunjang->name }} || File : <a href="{{ url('file_pasien/'.$soap_penunjang->file) }}" target="_blank">{{ $soap_penunjang->file }} </a>  Ket : {{ $soap_penunjang->keterangan }}</p>
                @endforeach
            </td>
            <td>@foreach ($rkmedis->soap_assesments as $soap_assesment)
                <p>{{ $soap_assesment->nm_diagnosa.' '.$soap_assesment->keterangan }}</p>
                @endforeach
            </td>
            <td>@foreach ($rkmedis->soap_plannings as $soap_planning)
                @if(substr($soap_planning->kd_item,0,1) === 'T')
                    <p>{{ $soap_planning->nm_item }}</p>

                @else
                    <p>{{ $soap_planning->nm_item. ' (Qty:'.$soap_planning->qty.' , Aturan Pakai:'.$soap_planning->aturan_pakai.')'}}</p>
                    @if($soap_planning->obat_racikan_det()->exists())
                    <hr>
                     @foreach ($soap_planning->obat_racikan_det as $obat_racikan_dt)
                        <span style="font-size:10px">{{ $obat_racikan_dt->nm_item }} (Qty : {{ $obat_racikan_dt->qty }})</span>
                     @endforeach
                    @endif
                @endif
                @endforeach
            </td>
            <td>
                <p>{{ $rkmedis->dokter->nmdoktergelar }}</p>

            </td>
        </tr>
            @endforeach

    </table>

</body>
</html>
