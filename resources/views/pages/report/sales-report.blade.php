@extends('layouts.main')

@section('page.title', 'Laporan Penjualan')
@section('page.heading', 'Laporan Penjualan')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
          <x-auth-validation-errors class="mb-4" :errors="$errors" />

                <div class="col-md-4">
                    <div class="example">
                        <h5 class="box-title m-t-30">Periode</h5>

                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" autocomplete="off" class="form-control" id="dateFirst" />
                            <div class="input-group-append">
                                <span class="input-group-text bg-info b-0 text-white">s/d</span>
                            </div>
                            <input type="text" autocomplete="off" class="form-control" id="dateEnd" />
                        </div>
                    </div>
                </div>
                <br>
                <div class="col-md-2">
                    <div class="button-group">
                        <button type="button" id="btn_print" onclick="myFunction()" class="btn waves-effect waves-light btn-info">Export</button>
                    </div>
                </div>

    </div>
</div>


@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){
    $('#date-range').datepicker({
        format: 'yyyy-mm-dd',
            toggleActive: true
    });


});


function myFunction() {
    let dateFirst = $('#dateFirst').val();
    let dateEnd = $('#dateEnd').val();
    let page_url = "{{ url('sales-report/export') }}" + '/' + dateFirst + '/' + dateEnd;

    window.open(page_url, '_blank');
}



</script>
@endpush
