@extends('layouts.main')

@section('page.title', 'Kategori Barang')
@section('page.heading', 'Kategori Barang')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

            <div class="row">
                <a href="{{ route('item-category.create') }}" class="btn btn-primary mt-4 ml-4">Tambah</a>
              </div>
              <div class="table-responsive">
                <table  id="tbl_item-category" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">No</th>
                      <th class="align-middle" scope="col">Nama</th>
                      <th class="align-middle" scope="col">Opsi</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>

    </div>
</div>

<div class="modal fade" id="modal_item-category" tabindex="-1" aria-labelledby="modal_label_item-category" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_label_item-category">Form Create Kategori Barang</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ url('item-category/store') }}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="iditem-category" name="iditem-category">
          <div class="modal-body">

            <div class="form-group row">
              <label  class="col-sm-3 col-form-label">Nama</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="name" name="name" required>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){
    loadList();
$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
 });
//$('.select2').select2({ width: '100%',allowClear: true, placeholder :'--Pilih--' });

});

function loadList(){
        const page_url = '{{ url('item-category/get-data') }}';

        var table = $('#tbl_item-category').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: page_url,
        },
        columns: [
        { "data": null,"sortable": false,
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        },
        {data: 'name', name: 'name'},
        { "data": null,"sortable": false,
            render: function (data, type, row, meta) {
                var result = '<a class="btn waves-effect waves-light btn-warning" href="{{ url('item-category/edit').'/' }}'+row.id+'">Ubah</a>';
                result += '&nbsp;<a class="btn waves-effect waves-light btn-danger" href="{{ url('item-category/hapus').'/' }}'+row.id+'">Hapus</a>';

                return result;
            }
        }

        ],
        responsive: true,
        oLanguage: {
            sLengthMenu: "_MENU_",
            sSearch: ""
        },
        aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
        order: [[1, "asc"]],
        pageLength: 10
    });
}
</script>
@endpush
