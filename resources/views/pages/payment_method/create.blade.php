@extends('layouts.main')

@section('page.title', 'Create Metode Bayar')
@section('page.heading', 'Create Metode Bayar')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
          <x-auth-validation-errors class="mb-4" :errors="$errors" />

            <form method="POST" action="{{ route('payment-method.store') }}" id="frm_pp" class="form-horizontal" enctype="multipart/form-data">
                @csrf


                <div class="form-row mt-1" id="div_name">
                    <label  class="col-sm-2 text-right">Nama</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="name" name="name" :value="old('name')" required>
                    </div>

                </div>

                <br>
                <div class="button-group">
                    <button type="submit" class="btn waves-effect waves-light btn-info">Simpan</button>
                    <a href="{{ route('payment-method') }}" class="btn waves-effect waves-light btn-warning">Kembali</a>
                </div>
            </form>


    </div>
</div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){

$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
 });

</script>
@endpush
