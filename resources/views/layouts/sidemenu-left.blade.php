@php
    $user = auth()->user();
@endphp
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->

        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <li class="nav-small-cap">MAIN</li>
                   @if ($user->hasAnyRole('kasir'))
                    <li> <a class="waves-effect " href="{{ route('billing') }}"><i class="mdi mdi-cash-multiple"></i><span>Kasir</span></a>
                    <li> <a class="waves-effect " href="{{ route('sales.history') }}"><i class="mdi mdi-clock"></i><span>Riwayat Penjualan</span></a>
                    <li> <a class="waves-effect " href="{{ route('customer') }}"><i class="mdi mdi-account"></i><span>Pelanggan</span></a>

                    @endif
                    @if ($user->hasAnyRole('admin'))
                <li> <a class="waves-effect " href="{{ route('dashboard') }}"><i class="mdi mdi-view-dashboard"></i><span>Dashboard</span></a>

                    <li> <a class="waves-effect " href="{{ route('items') }}"><i class="mdi mdi-shopping"></i><span>Barang</span></a>

                    <li> <a class="waves-effect " href="{{ route('billing') }}"><i class="mdi mdi-cash-multiple"></i><span>Kasir</span></a>

                    <li> <a class="waves-effect " href="{{ route('sales.history') }}"><i class="mdi mdi-clock"></i><span>Riwayat Penjualan</span></a>

                    <li> <a class="waves-effect " href="{{ route('so') }}"><i class="mdi mdi-book"></i><span>Stok Opname</span></a>

                    <li> <a class="waves-effect " href="{{ route('mutation') }}"><i class="mdi mdi-book"></i><span>Mutasi Barang</span></a>

                        <li> <a class="waves-effect " href="{{ route('stockin') }}"><i class="mdi mdi-book"></i><span>Tambah Stok Masuk</span></a>

                <li class="nav-small-cap">SETTING</li>
                <li> <a class="waves-effect " href="{{ route('user') }}"><i class="mdi mdi-account"></i><span>Pengguna</span></a>
                <li> <a class="waves-effect " href="{{ route('print-barcode') }}"><i class="mdi mdi-barcode"></i><span>Cetak Barcode</span></a>
                <li> <a class="waves-effect " href="{{ route('warehouse') }}"><i class="mdi mdi-store"></i><span>Gudang</span></a>
                {{-- <li> <a class="waves-effect " href="{{ route('supplier') }}"><i class="mdi mdi-account"></i><span>Supplier</span></a> --}}
                <li> <a class="waves-effect " href="{{ route('customer') }}"><i class="mdi mdi-account"></i><span>Pelanggan</span></a>
                <li> <a class="waves-effect " href="{{ route('item-category') }}"><i class="mdi mdi-shopping"></i><span>Kategori Barang</span></a>
                <li> <a class="waves-effect " href="{{ route('profile.edit') }}"><i class="mdi mdi-account"></i><span>Profil</span></a>
                    <li> <a class="waves-effect " href="{{ route('activity-log') }}"><i class="mdi mdi-account"></i><span>Activity Log</span></a>

                <li class="nav-small-cap">REPORT</li>
                <li> <a class="waves-effect " href="{{ route('salesReport') }}"><i class="mdi mdi-book"></i><span>Lap. Penjualan</span></a>
                <li> <a class="waves-effect " href="{{ route('sales-detail.report') }}"><i class="mdi mdi-book"></i><span>Lap. Detail Penjualan</span></a>
                <li> <a class="waves-effect " href="{{ route('customer.report') }}"><i class="mdi mdi-book"></i><span>Lap. Pelanggan</span></a>
                <li> <a class="waves-effect " href="{{ route('items.report') }}"><i class="mdi mdi-book"></i><span>Lap. Barang</span></a>
                <li> <a class="waves-effect " href="{{ route('stock.card') }}"><i class="mdi mdi-book"></i><span>Kartu Stok</span></a>
                <li> <a class="waves-effect " href="{{ route('check-stock') }}"><i class="mdi mdi-book"></i><span>Cek Stok Barang</span></a>
                    @endif
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
