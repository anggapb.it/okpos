@extends('layouts.main')

@section('page.title', 'Dashboard')
@section('page.heading', 'Dashboard')

@section('page.content')
 <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-briefcase-check text-info"></i></h2>
                                    <h3 class=""><strong>Rp. {{ number_format($profitSalesToday->amount,0,",",".") }}</strong></h3>
                                    <h6>Keuntungan hari ini</h6></div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-alert-circle text-success"></i></h2>
                                    <h3 class=""><strong>{{ $amountItemSalesToday->item_sales }}</strong></h3>
                                    <h6>Item terjual hari ini</h6></div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-wallet text-purple"></i></h2>
                                    <h3 class=""><strong>{{ $salesToday }}</strong></h3>
                                    <h6>Transaksi hari ini</h6></div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-buffer text-warning"></i></h2>
                                    <h3 class=""><strong>{{ $salesMonth }}</strong></h3>
                                    <h6>Transaksi bulan ini</h6></div>

                            </div>
                        </div>
                    </div>
                </div>
<!-- Row -->
<div class="row">
    <div class="col-lg-8 col-md-7">
        <div class="card">
            <div class="card-body">
                <div class="d-flex flex-wrap">
                    <div>
                        <h4 class="card-title">Penjualan Per Tahun</h4>
                    </div>
                </div>
                <div id="container-chart" style="height: 405px;"></div>

            </div>
        </div>
        <div class="card card-default">
                    <div class="card-header">
                        <div class="card-actions">
                            <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                            <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                            <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
                        </div>
                        <h4 class="card-title m-b-0">5 Item Terlaris</h4>
                    </div>
                    <div class="card-body collapse show">
                        <div class="table-responsive">
                            <table class="table product-overview">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Item</th>
                                        <th>Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($bestSellingItems as $bsi)

                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            @if(!empty($bsi->itemVariant))
                                            <td>{{$bsi->itemVariant->get_items->name.' '.$bsi->itemVariant->color.' '.$bsi->itemVariant->size}}</td>
                                            @else
                                            <td>&nbsp;</td>
                                            @endif
                                            <td>{{$bsi->qty}}</td>
                                        </tr>

                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
    </div>
    <div class="col-lg-4 col-md-5">

        <!-- Column -->
        <div class="card card-default">
            <div class="card-header">
                <div class="card-actions">
                    <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                    <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
                </div>
                <h4 class="card-title m-b-0">Transaksi Terakhir</h4>
            </div>
            <div class="card-body collapse show bg-info">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        @if (count($latestSales) == 0)
                        <div class="carousel-item flex-column active">
                            <h3 class="text-white font-light">Belum ada data</h3>
                        </div>
                        @endif
                       @foreach ($latestSales as $key => $val)
                        @php
                         if($key == 0){
                             $stats = 'active';
                         } else {
                             $stats = null;
                         }
                        @endphp
                        <div class="carousel-item flex-column {{ $stats }}">
                            <i class="fa fa-shopping-cart fa-2x text-white"></i>
                            <p class="text-white">{{ $val->created_at }}</p>
                            <h3 class="text-white font-light">Total : <span class="font-bold">Rp. {{ number_format($val->cost_amount,0,",",".") }}</span></h3>
                            <div class="text-white m-t-20">
                                <i>- {{ ($val->customer) ? $val->customer->name : '' }}</i>
                            </div>
                        </div>
                       @endforeach

                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <div class="card card-default">
            <div class="card-header">
                <div class="card-actions">
                    <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                    <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
                </div>
                <h4 class="card-title m-b-0">Item Baru</h4>
            </div>
            <div class="card-body p-0 collapse show text-center">
                <div id="myCarousel2" class="carousel slide" data-ride="carousel">
                    <!-- Carousel items -->
                    <div class="carousel-inner">

                        <div class="carousel-item flex-column active">
                            {{-- <img src="../assets/images/gallery/chair3.jpg" alt="user"> --}}
                            @if(!empty($latestItem))
                            <h4 class="m-b-30">{{ $latestItem->name }}</h4>
                            <h6 class="m-b-30"><i>{{ $latestItem->description }}</i></h6>
                            @endif

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('bottom.scripts')
<script>
    let salesReportPerMonth = <?php echo json_encode($salesReport) ?>;
    let salesMonth = <?php echo json_encode($month) ?>;
    let bulan = translate(salesMonth);
    Highcharts.chart('container-chart',{
        title:{
            text:"Penjualan Per Tahun"
        },
        xAxis:{
            categories:salesMonth
        },
        yAxis:{
            title:{
                text: "Jumlah Penjualan"
            }
        },
        series:[{
            name:"Penjualan",
            data:salesReportPerMonth
        }]
    });
    function translate(salesMonth) {
        for (let i = 0; i < salesMonth.length; i++) {
            if (salesMonth[i] == 'January' ) {
                salesMonth[i] = 'Januari';
            }
            if (salesMonth[i] == 'February' ) {
                salesMonth[i] = 'Februari';
            }
            if (salesMonth[i] == 'March' ) {
                salesMonth[i] = 'Maret';
            }
            if (salesMonth[i] == 'May' ) {
                salesMonth[i] = 'Mei';
            }
            if (salesMonth[i] == 'June' ) {
                salesMonth[i] = 'Juni';
            }
            if (salesMonth[i] == 'July' ) {
                salesMonth[i] = 'Juli';
            }
            if (salesMonth[i] == 'August' ) {
                salesMonth[i] = 'Agustus';
            }
            if (salesMonth[i] == 'October' ) {
                salesMonth[i] = 'Oktober';
            }
            if (salesMonth[i] == 'December' ) {
                salesMonth[i] = 'Desember';
            }
        }
    }
</script>
@endpush
