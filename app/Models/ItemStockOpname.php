<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemStockOpname extends Model
{
    use HasFactory;

    protected $table= "item_stock_opname";
    protected $guarded = [];
    public $timestamps = true;
}
