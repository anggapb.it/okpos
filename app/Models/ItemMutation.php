<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemMutation extends Model
{
    use HasFactory;

    protected $table= "item_mutations";
    protected $primaryKey= "id";
    protected $guarded = [];
}
