<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemsStockWarehouse extends Model
{
    use HasFactory, SoftDeletes;

    protected $table= "item_stock_warehouse";
    protected $guarded = [];
    public $timestamps = true;

    public function get_item_variant(){
        return $this->belongsTo(ItemsVariant::class, 'id_item_variant', 'id');
    }

    public function warehouse()
    {
        return $this->hasOne(Warehouse::class, 'id', 'id_warehouse');
    }
}
