<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataProfile extends Model
{
    use HasFactory;

    protected $fillable = [
        'store_name',
        'address',
        'phone_number',
        'logo_images'
    ];
}
