<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    use HasFactory;

    protected $table= "sales";
    protected $guarded = [];
    public $timestamps = true;
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    public function customer()
    {
        return $this->hasOne(Customer::class, 'id', 'customer_id');
    }
    public function salesdetail(){
        return $this->hasMany(SalesDetail::class, 'id', 'sales_id');
    }
}
