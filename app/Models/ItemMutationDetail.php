<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemMutationDetail extends Model
{
    use HasFactory, SoftDeletes;

    protected $table= "item_mutation_details";
    protected $primaryKey= "id";
    protected $guarded = [];
}
