<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ItemsVariant;
use Illuminate\Database\Eloquent\SoftDeletes;

class Items extends Model
{
    use HasFactory, SoftDeletes;

    protected $table= "items";
    protected $primaryKey= "id";
    protected $guarded = [];

    public function itemsVariant(){
        return $this->hasMany(ItemsVariant::class, 'item_id', 'id');
    }

    public static function boot() {
        parent::boot();
        self::deleting(function($item) { // before delete() method call this
             $item->itemsVariant()->each(function($itemVariant) {
                $itemVariant->delete(); // <-- direct deletion
             });
             // do the rest of the cleanup...
        });
    }


}
