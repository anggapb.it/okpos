<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Items;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemsVariant extends Model
{
    use HasFactory, SoftDeletes;

    protected $table= "item_variants";
    protected $guarded = [];
    public $timestamps = true;

    public function get_items(){
        return $this->belongsTo(Items::class, 'item_id', 'id');
    }

    public function sales_detail(){
        return $this->hasOne(SalesDetail::class, '');
    }

    public function stock_warehouse()
    {
        return $this->hasMany(ItemsStockWarehouse::class, 'id_item_variant', 'id');
    }

    public function stock_card()
    {
        return $this->hasMany(ItemsStockCards::class, 'id_item_variant', 'id');
    }

    public function stock_mutation_detail()
    {
        return $this->hasMany(ItemMutationDetail::class, 'item_variant_id', 'id');
    }

    public function stock_opname_detail()
    {
        return $this->hasMany(ItemStockOpnameDetail::class, 'id_item_variant', 'id');
    }
    

    public static function boot() {
        parent::boot();
        self::deleting(function($itemVariant) { // before delete() method call this
             $itemVariant->stock_warehouse()->each(function($stockWareHouse) {
                $stockWareHouse->delete(); // <-- direct deletion
             });

             $itemVariant->stock_card()->each(function($stockCard) {
                $stockCard->delete(); // <-- direct deletion
             });

             $itemVariant->stock_mutation_detail()->each(function($stockMutation) {
                $stockMutation->delete(); // <-- direct deletion
             });

             $itemVariant->stock_opname_detail()->each(function($so) {
                $so->delete(); // <-- direct deletion
             });
             // do the rest of the cleanup...
        });
    }

}
