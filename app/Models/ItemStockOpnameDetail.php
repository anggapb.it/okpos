<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemStockOpnameDetail extends Model
{
    use HasFactory, SoftDeletes;

    protected $table= "item_stock_opname_details";
    protected $guarded = [];
    public $timestamps = true;
}
