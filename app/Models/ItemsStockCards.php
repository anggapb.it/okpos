<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemsStockCards extends Model
{
    use HasFactory, SoftDeletes;

    protected $table= "item_stock_cards";
    protected $guarded = [];
    public $timestamps = true;

    public function get_item_variant()
    {
        return $this->hasOne(ItemsVariant::class, 'id', 'id_item_variant');
    }
}
