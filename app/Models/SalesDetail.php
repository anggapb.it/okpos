<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesDetail extends Model
{
    use HasFactory, SoftDeletes;

    protected $table= "sales_details";
    protected $guarded = [];
    public $timestamps = true;

    public function itemVariant()
    {
        return $this->belongsTo(ItemsVariant::class, 'item_id', 'id');
    }
}
