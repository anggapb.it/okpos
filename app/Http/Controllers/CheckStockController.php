<?php

namespace App\Http\Controllers;

use App\Models\ItemsVariant;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class CheckStockController extends Controller
{
    public function index()
    {
       return view('pages/check_stock/index');
    }

    public function getItems()
    {
        $data = ItemsVariant::with(['get_items', 'stock_warehouse']);
        $dataTable = DataTables::eloquent($data)
        ->addColumn('warehouse', function (ItemsVariant $data) {
            return $data->stock_warehouse->map(function($a) {
                return $a->warehouse->name.' : '.$a->stock;
            })->implode('<br>');
        })
        ->addColumn('stock_warehouse', function (ItemsVariant $data) {
            return $data->stock_warehouse->sum('stock'); //it's cool!
        })
        ->addIndexColumn();

        return $dataTable->toJson();

    }
}
