<?php

namespace App\Http\Controllers;

use App\Models\ItemsCategory;
use App\Traits\ActivityLogTrait;
use Illuminate\Http\Request;

class ItemCategoryController extends Controller
{
    use ActivityLogTrait;
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/item_category/index');
    }

    public function get_data()
    {
        $data = ItemsCategory::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function create()
    {
        return view('pages/item_category/create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
        ]);

        $item_category = ItemsCategory::create([
            'name' => $request->name
        ]);
        // Activity Log
        $description = 'ID Kategori '.$item_category->id;
        $this->addLog('Tambah Kategori Barang', $description);
        return redirect()->route('item-category');
    }

    public function destroy($id)
    {
        // Activity Log
        $description = 'ID Kategori '.$id;
        $this->addLog('Hapus Kategori Barang', $description);
        $item_category = ItemsCategory::findOrFail($id);
        $item_category->delete();

        return redirect()->route('item-category');
    }

    public function edit($id)
    {
        $item_category = ItemsCategory::findOrFail($id);
        return view('pages/item_category/edit',compact(['item_category']));
    }

    public function update(Request $request)
    {
        $item_category = ItemsCategory::find($request->id);
        $request->validate([
            'name' => 'required|string|max:255'
        ]);

        $item_category->update([
            'name' => $request->name
        ]);
        // Activity Log
        $description = 'ID Kategori '.$item_category->id;
        $this->addLog('Ubah Data Kategori Barang', $description);
        return redirect()->route('item-category');
    }
}
