<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Traits\ActivityLogTrait;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    use ActivityLogTrait;
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/customer/index');
    }

    public function get_data()
    {
        $data = Customer::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function create()
    {
        return view('pages/customer/create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
        ]);

        $customer = Customer::create([
            'name' => $request->name,
            'discount' => $request->discount,
            'phone_number' => $request->phone_number,
            'address' => $request->address
        ]);
        // Activity Log
        $description = 'ID Customer '.$customer->id;
        $this->addLog('Tambah Customer', $description);
        return redirect()->route('customer');
    }

    public function destroy($id)
    {
        // Activity Log
        $description = 'ID Customer '.$id;
        $this->addLog('Hapus Gudang', $description);
        $customer = Customer::findOrFail($id);
        $customer->delete();

        return redirect()->route('customer');
    }

    public function edit($id)
    {
        $customer = Customer::findOrFail($id);
        return view('pages/customer/edit',compact(['customer']));
    }

    public function update(Request $request)
    {
        $customer = Customer::find($request->id);
        $request->validate([
            'name' => 'required|string|max:255'
        ]);

        $customer->update([
            'name' => $request->name,
            'discount' => $request->discount,
            'phone_number' => $request->phone_number,
            'address' => $request->address
        ]);
        // Activity Log
        $description = 'ID Customer '.$customer->id;
        $this->addLog('Ubah Data Customer', $description);
        return redirect()->route('customer');
    }
}
