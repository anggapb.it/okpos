<?php

namespace App\Http\Controllers;

use App\Models\ItemsStockCards;
use App\Models\ItemsVariant;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class StockCardReportController extends Controller
{
    public function index()
    {
        $warehouse = Warehouse::pluck('name', 'id');

        return view('pages/stock_card/index', compact(['warehouse']));
    }

    public function getItems()
    {
        $data = ItemsVariant::with(['get_items']);
        $dataTable = DataTables::eloquent($data)
        // ->addColumn('items', function ($data) {
        //     return $data->get_items->name.' '.$data->color.' '.$data->size;
        // })
        //  ->filterColumn('items', function($query, $keyword) {
        //     $sql = "get_items.name like ?";
        //     $query->whereRaw($sql, ["%{$keyword}%"]);
        // })
        // ->rawColumns(['items'])
        ->addIndexColumn();

        return $dataTable->toJson();

    }

    public function getItemsStockCard(Request $request)
    {
        $id_item_variant = $request->id_item_variant;
        $id_warehouse = $request->id_warehouse;

        $stockCard = ItemsStockCards::with(['get_item_variant','get_item_variant.get_items'])->where('id_item_variant', $id_item_variant)->where('id_warehouse', $id_warehouse);
        $total = $stockCard->count();
        $list = $stockCard->get();

        return response()->json([
            'status' => true,
            'total' => $total,
            'list' => $list
        ]);
    }
}
