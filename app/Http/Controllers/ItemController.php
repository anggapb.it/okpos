<?php

namespace App\Http\Controllers;

use App\Models\Items;
use App\Models\ItemsCategory;
use App\Models\ItemsStockCards;
use App\Models\ItemsStockWarehouse;
use App\Models\ItemsVariant;
use App\Traits\ActivityLogTrait;
use Exception;
use Illuminate\Http\Request;
use App\Traits\StockCardTrait;
use Illuminate\Support\Facades\Storage;

class ItemController extends Controller
{
    use StockCardTrait, ActivityLogTrait;

    public function index()
    {
        return view('pages/items/index');
    }

    public function create()
    {
        $kategori_barang = ItemsCategory::all();
        return view('pages/items/create', compact(['kategori_barang']));
    }

    public function store(Request $req)
    {
        try {
                //insert items
            $items = Items::create([
                'name' => $req->nama_barang,
                'item_category_id' => $req->kategori_barang,
            ]);

            //insert items variant
            if($req->ukuran){
                foreach($req->get('ukuran') as $i => $ukuran){
                    $harga = $req->get('harga');
                  //  $qty = $req->get('qty');

                    $item_variant = ItemsVariant::create([
                        'item_id' => $items->id,
                        'size' => $ukuran,
                        'stock' => 0, //$qty["$i"],
                        'price' => $harga["$i"],

                    ]);


                    if($item_variant){
                        //insert to stock warehouse
                        ItemsStockWarehouse::create([
                            'id_item_variant' => $item_variant->id,
                            'id_warehouse' => 4, //default to gudang
                            'stock' => 0, //$qty["$i"]
                        ]);
                        //insert to stock cards
                        $this->saveStockCard($item_variant->id, 1, 0 , 'in', 'input new');
                    }
                }
            }
            $desc = 'ID Barang ' . $items->id;
            $this->addLog('Pengguna Menambahkan Barang', $desc);
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
        return redirect()->route('items')->with(['message' => 'data berhasil disimpan']);
    }

    public function getData()
    {
        $data = Items::with(['itemsVariant', 'itemsVariant.stock_warehouse'])->get(); //use eager loading
        return datatables()->of($data)
        ->addColumn('info_items', function ($data) {
            return $data->name;
        })
        ->addColumn('variant', function (Items $items) {
            return $items->itemsVariant->map(function($items_variant) {
                $url = url('items/hapus-variant/'.$items_variant->id);
                return '('.$items_variant->size.') '.$items_variant->color.' RP. '.number_format($items_variant->price,0,",","."). '<a class="btn-xs waves-effect waves-light btn-danger" href="'.$url.'">-</a>';
            })->implode('<hr>');
        })
        ->addColumn('stock', function (Items $items) {
            return $items->itemsVariant->map(function($items_variant) {
                return $items_variant->stock_warehouse->sum('stock');
            })->implode('<hr>');
        })
        ->rawColumns(['info_items','variant','stock'])
        ->addIndexColumn()
        ->make(true);
    }

    public function edit($id)
    {
        $kategori_barang = ItemsCategory::all();
        $item = Items::with('itemsVariant')->where('id', $id)->first();
        return view('pages/items/edit', compact(['item', 'kategori_barang']));
    }

    public function update(Request $request)
    {
        $id = $request->get('id_item');
        $nama_barang = $request->get('nama_barang');
        $kategori_barang = $request->get('kategori_barang');

        try {
            $item = Items::find($id);
            $item->name = $nama_barang;
            $item->item_category_id = $kategori_barang;
            $item->save();

            if($request->get('idvariant')){
                foreach($request->get('idvariant') as $i => $idvariant){
                    $ukuran = $request->get('ukuran');
                    $harga = $request->get('harga');
                    //$qty = $request->get('qty');
                   // $gambar = $request->get('gambarLama');
                   // $g = $request->file('gambar'); //variable temp gambar baru
                    // if (!empty($g[$i])) {
                    //     $gambarBaru = $request->file('gambar');
                    //     $gambar["$i"] = $gambarBaru["$i"]->store('item_pictures');
                    //     $path["$i"] = $gambar["$i"];
                    // }
                    // else{
                    //     $path["$i"] = $gambar["$i"];
                    // }

                    $item_variant = ItemsVariant::updateOrCreate(
                        ['id' => $idvariant],
                        [
                            'item_id' => $item->id,
                            'size' => $ukuran["$i"],
                            'price' => $harga["$i"],
                           // 'stock' => $qty["$i"],
                          //  'image' => $path["$i"]
                        ]
                    );

                    if($item_variant->wasRecentlyCreated) {
                        ItemsStockWarehouse::create([
                            'id_item_variant' => $item_variant->id,
                            'id_warehouse' => 4, //default to gudang
                            'stock' => 0 //set default 0 //$qty["$i"]
                        ]);
                        //insert to stock cards
                        $this->saveStockCard($item_variant->id, 1, 0 , 'in', 'input new');
                    }
                    $desc = 'ID Variant ' . $idvariant;
                    $this->addLog('Pengguna Mengubah Data Barang', $desc);
                }
            }
            $desc = 'ID Barang ' . $id . ' Dan Nama Barang ' . $nama_barang;
            $this->addLog('Pengguna Mengubah Data Barang', $desc);
        } catch (\Exception $e) {
           return $e->getMessage();
        }

        return redirect()->route('items')->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        Items::findOrFail($id)->delete();
        $this->addLog('Pengguna Menghapus Barang', 'ID Barang '. $id);
        return redirect()->route('items')->with(['message' => 'data berhasil dihapus']);
    }

    public function deleteVariant($id)
    {
        ItemsVariant::findOrFail($id)->delete();
        $this->addLog('Pengguna Menghapus Barang', 'Item Variant '.$id);
        return redirect()->route('items')->with(['message' => 'data varian berhasil dihapus']);
    }
}
