<?php

namespace App\Http\Controllers;

use App\Models\ItemsVariant;
use App\Traits\ActivityLogTrait;
use Illuminate\Http\Request;
use DNS1D;
use Items;
use PDF;
use Yajra\DataTables\Facades\DataTables;

class PrintBarcodeController extends Controller
{
    use ActivityLogTrait;
    public function index()
    {
        return view('pages/print_barcode/index');
    }

    public function print(Request $req)
    {
        $id = $req->id;
        $amount = $req->amount;
        $idOri = str_pad($req->idori,10,"18", STR_PAD_LEFT);

        $data = ItemsVariant::with('get_items')->where('id', $id)->first();

        $barcode = DNS1D::getBarcodeHTML($idOri, 'C39', 2.6, 140);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('pages/print_barcode/pdf_view', compact('id', 'amount', 'barcode', 'data'));
        $pdf->setPaper([0, 0, 200, 500], 'landscape');
        // Activity Log
        $description = 'ID Barang '.$data->id.'. Jumlah Barcode '.$amount;
        $this->addLog('Transaksi Dengan Customer', $description);
        return $pdf->stream();
    }

    public function getItems()
    {
        $data = ItemsVariant::with(['get_items'])->get();

        return datatables()->of($data)
        ->addColumn('items', function ($data) {
            return $data->get_items->name . ' ' . $data->size;
        })
        ->rawColumns(['items'])
        ->addIndexColumn()
        ->make(true);

    }
}
