<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class PasswordController extends Controller
{
    public function update(Request $request){
        $dataValidated = $request->validate([
            'password' => 'required|min:6'
        ]);

        User::where('id', Auth::user()->id)
                ->update([
                    'password' => Hash::make($dataValidated['password'])
                ]);
        return redirect()->route('dashboard');
    }
}
