<?php

namespace App\Http\Controllers;

use App\Models\ItemMutation;
use App\Models\ItemMutationDetail;
use App\Models\ItemsStockCards;
use App\Models\ItemsStockWarehouse;
use App\Models\Warehouse;
use App\Traits\ActivityLogTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use App\Traits\StockCardTrait;

class MutationItemController extends Controller
{
    use StockCardTrait, ActivityLogTrait;

    public function index()
    {
        $warehouse = Warehouse::pluck('name', 'id');

        return view('pages/item_mutation/index', compact(['warehouse']));
    }

    public function getItemWarehouse($idWarehouse)
    {
        $data = ItemsStockWarehouse::with(['get_item_variant', 'get_item_variant.get_items'])->where('id_warehouse', $idWarehouse)->get();

        return datatables()->of($data)
        ->addColumn('items', function ($data) {
            return $data->get_item_variant->get_items->name . ' ' . $data->get_item_variant->size;
        })
        ->rawColumns(['items'])
        ->addIndexColumn()
        ->make(true);
    }
    public function getItemsById($id, $warehouse)
    {
        $data = ItemsStockWarehouse::with(['get_item_variant', 'get_item_variant.get_items'])->where('id_warehouse', $warehouse)->where('id_item_variant', $id)->first();
        if($data) {
            return response()->json([
                'success' => 'true',
                'data' => $data
            ]);
        } else {
            return response()->json([
                'success' => 'false',
                'data' => null
            ]);
        }
    }
    public function store(Request $request)
    {

         $mutation = ItemMutation::create([
            'warehouse_id_first' => $request->warehouse_id_first,
            'warehouse_id_end' => $request->warehouse_id_end,
            'user_input' => Auth::user()->id
        ]);

        if($request->id_item_variant){
            foreach ($request->get('id_item_variant') as $key => $id_item_variant) {
                $stock = $request->get('stock');
                $qtyMutation = $request->get('qty_mutation');

                $itemMutationDetail = ItemMutationDetail::create([
                    'item_variant_id' => $id_item_variant,
                    'item_mutation_id' => $mutation->id,
                    'stock' => $stock["$key"],
                    'qty_mutation' => $qtyMutation["$key"]
                ]);


                if($itemMutationDetail){
                     //kurangi stock di warehouse first
                    $itemStockWarehouseFirst = ItemsStockWarehouse::where('id_item_variant', $itemMutationDetail->item_variant_id)->where('id_warehouse', $mutation->warehouse_id_first)->first();
                    $itemStockWarehouseFirst->stock = (int) $stock["$key"] - (int) $qtyMutation["$key"];
                    $itemStockWarehouseFirst->save();
                    $this->saveStockCard($itemMutationDetail->item_variant_id, $mutation->warehouse_id_first, $qtyMutation["$key"], 'out', 'mutasi');

                     //tambah stock di warehouse end
                     $itemStockWarehouseEnd = ItemsStockWarehouse::where('id_item_variant', $itemMutationDetail->item_variant_id)->where('id_warehouse', $mutation->warehouse_id_end)->first();
                     if(!empty($itemStockWarehouseEnd)){
                        $itemStockWarehouseEnd->stock = (int) $itemStockWarehouseEnd->stock + (int) $qtyMutation["$key"];
                        $itemStockWarehouseEnd->save();
                        $this->saveStockCard($itemMutationDetail->item_variant_id, $mutation->warehouse_id_end, $qtyMutation["$key"], 'in', 'mutasi');

                     } else {
                         //first data
                        $itemStockWarehouseEnd = ItemsStockWarehouse::create([
                            'id_item_variant' => $itemMutationDetail->item_variant_id,
                            'id_warehouse' => $mutation->warehouse_id_end,
                            'stock' => $qtyMutation["$key"]
                        ]);
                        $this->saveStockCard($itemMutationDetail->item_variant_id, $mutation->warehouse_id_end, $qtyMutation["$key"], 'in', 'mutasi');
                     }



                }

            }
        }
        $description = 'ID Mutasi Barang '.$mutation->id;
        $this->addLog('Mutasi Barang', $description);
        return response()->json([
            'success' => 'true',
            'item_mutation_id' => $mutation->id
        ]);
    }
}
