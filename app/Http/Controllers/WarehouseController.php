<?php

namespace App\Http\Controllers;

use App\Models\Warehouse;
use App\Traits\ActivityLogTrait;
use Illuminate\Http\Request;

class WarehouseController extends Controller
{
    use ActivityLogTrait;
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/warehouse/index');
    }

    public function get_data()
    {
        $data = Warehouse::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function create()
    {
        return view('pages/warehouse/create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
        ]);

        $warehouse = Warehouse::create([
            'name' => $request->name
        ]);
        // Activity Log
        $description = 'ID Gudang '.$warehouse->id;
        $this->addLog('Tambah Gudang', $description);
        return redirect()->route('warehouse');
    }

    public function destroy($id)
    {
        // Activity Log
        $description = 'ID Gudang '.$id;
        $this->addLog('Hapus Gudang', $description);
        $warehouse = Warehouse::findOrFail($id);
        $warehouse->delete();
        return redirect()->route('warehouse');
    }

    public function edit($id)
    {
        $warehouse = Warehouse::findOrFail($id);
        return view('pages/warehouse/edit',compact(['warehouse']));
    }

    public function update(Request $request)
    {
        $warehouse = Warehouse::find($request->id);
        $request->validate([
            'name' => 'required|string|max:255'
        ]);

        $warehouse->update([
            'name' => $request->name
        ]);
        // Activity Log
        $description = 'ID Gudang '.$warehouse->id;
        $this->addLog('Ubah Data Gudang', $description);
        return redirect()->route('warehouse');
    }
}
