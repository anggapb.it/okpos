<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\DataProfile;
use App\Traits\ActivityLogTrait;
use Illuminate\Support\Facades\DB;

class DataProfileController extends Controller
{
use ActivityLogTrait;
    public function index(){
        return view('pages/data_profile/index');
    }
    public function store(Request $request){
        return $request->file('logo')->store('logo-images');
    }
    public function getData()
    {
        $data = DataProfile::all();
        if($data) {
            return response()->json([
                'success' => 'true',
                'data' => $data
            ]);
        } else {
            return response()->json([
                'success' => 'false',
                'data' => null
            ]);
        }
    }
    public function edit($id){
        $data = DataProfile::findOrFail($id);
        return view('pages/data_profile/edit', [
            'profile' => $data
        ]);
    }
    public function upOrIns(Request $request){
        
        $dataValidated = $request->validate([
            'store_name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'phone_number' => 'required|max:255',
            'logo_image' => 'image|mimes:jpg,png|file|max:5120'
        ]);
        if ($request->file('logo_image')) {
            $dataValidated['logo_image'] = $request->file('logo_image')->store('logo-images');
        }else{
            $dataValidated['logo_image'] = $request->old_logo_image;
        }
        // $data = DataProfile::findOrFail(1);
        DB::table('data_profiles')
                    ->updateOrInsert(
                        ['id' => 1],
                        [
                            'store_name' => $dataValidated['store_name'],
                            'address' => $dataValidated['address'],
                            'phone_number' => $dataValidated['phone_number'],
                            'logo_image' => $dataValidated['logo_image']
                        ]
                    );
        // Activity Log
        $description = 'Profil Toko';
        $this->addLog('Ubah Data Profil', $description);
        return redirect()->route('profile.edit',['id' => $request->id]);
    }
}
