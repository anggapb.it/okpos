<?php

namespace App\Http\Controllers;

use App\Models\ItemsStockWarehouse;
use App\Models\Warehouse;
use App\Traits\ActivityLogTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use App\Traits\StockCardTrait;

class StockInController extends Controller
{
    use StockCardTrait, ActivityLogTrait;

    public function index()
    {
        $warehouse = Warehouse::pluck('name', 'id');

        return view('pages/item_stockin/index', compact(['warehouse']));
    }

    public function getItemWarehouse($idWarehouse)
    {
        $data = ItemsStockWarehouse::with(['get_item_variant', 'get_item_variant.get_items'])->where('id_warehouse', $idWarehouse)->get();

        return datatables()->of($data)
        ->addColumn('items', function ($data) {
            return $data->get_item_variant->get_items->name . ' ' . $data->get_item_variant->size;
        })
        ->rawColumns(['items'])
        ->addIndexColumn()
        ->make(true);

    }
    public function getItemsById($id, $warehouse)
    {
        $data = ItemsStockWarehouse::with(['get_item_variant', 'get_item_variant.get_items'])->where('id_warehouse', $warehouse)->where('id_item_variant', $id)->first();
        if($data) {
            return response()->json([
                'success' => 'true',
                'data' => $data
            ]);
        } else {
            return response()->json([
                'success' => 'false',
                'data' => null
            ]);
        }
    }

    public function store(Request $request)
    {
        if($request->id_item_variant){
            foreach ($request->get('id_item_variant') as $key => $id_item_variant) {
                $stockIn = $request->get('stock_in');

                     //tambah stock di warehouse end
                     $itemStockWarehouse = ItemsStockWarehouse::where('id_item_variant', $id_item_variant)
                                             ->where('id_warehouse', $request->post('warehouse_id'))->first();
                     if(!empty($itemStockWarehouse)){
                        $itemStockWarehouse->stock = (int) $itemStockWarehouse->stock + (int) $stockIn["$key"];
                        $itemStockWarehouse->save();
                        $this->saveStockCard($id_item_variant, $request->post('warehouse_id'), $stockIn["$key"], 'in', 'stockin');
                     }
                // Activity Log
                $description = 'ID Variant Barang '.$id_item_variant.'Dan Quantity Barang Masuk '.$stockIn["$key"];
                $this->addLog('Penambahan Stok Barang', $description);
            }
        }
        return response()->json([
            'success' => 'true'
        ]);
    }
}
