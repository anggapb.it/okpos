<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\SalesDetailExport;
use App\Traits\ActivityLogTrait;
use Maatwebsite\Excel\Facades\Excel;

class SalesDetailReportController extends Controller
{
    use ActivityLogTrait;
    public $export;

    public function __construct(){
        $this->export = new salesDetailExport;
    }
    public function index()
    {
        return view('pages/sales_history/sales-detail-report');
    }
    public function all(){
        return Excel::download($this->export, 'barang.xlsx');
    }
    public function export($dateFirst, $dateEnd)
    {
        $export = new SalesDetailExport;
        $export->dateFirst($dateFirst);
        $export->dateEnd($dateEnd);
        // Activity Log
        $description = 'Cetak Laporan Detail Penjualan';
        $this->addLog('Cetak Laporan', $description);
        return Excel::download($export, 'detail-penjualan.xlsx');
    }
}
