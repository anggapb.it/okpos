<?php

namespace App\Http\Controllers;

use App\Models\ItemsStockCards;
use App\Models\ItemsStockWarehouse;
use App\Models\ItemStockOpname;
use App\Models\ItemStockOpnameDetail;
use App\Models\Warehouse;
use App\Traits\ActivityLogTrait;
use Illuminate\Http\Request;
use App\Traits\StockCardTrait;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class ItemStockOpnameController extends Controller
{
    use StockCardTrait, ActivityLogTrait;

    public function index()
    {
        $warehouse = Warehouse::pluck('name', 'id');

        return view('pages/item_so/index', compact(['warehouse']));
    }

    public function getItemWarehouse($idWarehouse)
    {
        $data = ItemsStockWarehouse::with(['get_item_variant', 'get_item_variant.get_items'])->where('id_warehouse', $idWarehouse)->get();

        return datatables()->of($data)
        ->addColumn('items', function ($data) {
            return $data->get_item_variant->get_items->name . ' ' . $data->get_item_variant->size;
        })
        ->rawColumns(['items'])
        ->addIndexColumn()
        ->make(true);
    }

    public function getItemsById($id, $warehouse)
    {
        $data = ItemsStockWarehouse::with(['get_item_variant', 'get_item_variant.get_items'])->where('id_warehouse', $warehouse)->where('id_item_variant', $id)->first();
        if($data) {
            return response()->json([
                'success' => 'true',
                'data' => $data
            ]);
        } else {
            return response()->json([
                'success' => 'false',
                'data' => null
            ]);
        }
    }

    public function store(Request $request)
    {
        $so = ItemStockOpname::create([
            'warehouse_id' => $request->warehouse_id,
            'user_id' => Auth::user()->id
        ]);

        if($request->id_item_variant){
            foreach ($request->get('id_item_variant') as $key => $id_item_variant) {
                $stock = $request->get('stock');
                $stockFisik = $request->get('stock_fisik');

                $soDet = ItemStockOpnameDetail::create([
                    'id_item_variant' => $id_item_variant,
                    'id_stock_opname' => $so->id,
                    'stock_system' => $stock["$key"],
                    'stock_real' => $stockFisik["$key"],
                    'stock_difference' => (int) $stock["$key"] - (int) $stockFisik["$key"]
                ]);

                if($soDet){
                     //update stock di warehouse
                    $itemStockWarehouse = ItemsStockWarehouse::where('id_item_variant', $soDet->id_item_variant)->where('id_warehouse', $so->warehouse_id)->first();
                    $itemStockWarehouse->stock = $stockFisik["$key"];
                    $itemStockWarehouse->save();
                    // add kartu stok
                    $this->saveStockCard($soDet->id_item_variant, $so->warehouse_id, $stockFisik["$key"], 'in', 'mutasi');
                }

            }
        }
        $description = 'ID Stock Opname Barang '.$so->id;
        $this->addLog('Stock Opname', $description);
        return response()->json([
            'success' => 'true',
            'item_stock_opname_id' => $so->id
        ]);
    }
}
