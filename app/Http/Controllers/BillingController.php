<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\DataProfile;
use App\Models\ItemsStockCards;
use App\Models\ItemsStockWarehouse;
use App\Models\ItemsVariant;
use App\Models\Sales;
use App\Models\SalesDetail;
use App\Traits\ActivityLogTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use App\Traits\StockCardTrait;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Profiler\Profile;

class BillingController extends Controller
{
    use StockCardTrait, ActivityLogTrait;

    public function index()
    {
        $customer = Customer::all();
        return view('pages/billing/index', compact(['customer']));
    }

    public function getItems()
    {
        $data = ItemsStockWarehouse::with(['get_item_variant', 'get_item_variant.get_items'])
            ->where('id_warehouse', Auth::user()->id_warehouse)
            ->get();

        return datatables()->of($data)
            ->addColumn('items', function ($data) {
                return $data->get_item_variant->get_items->name . ' ' . $data->get_item_variant->size;
            })
            ->rawColumns(['items'])
            ->addIndexColumn()
            ->make(true);
    }

    public function getItemsById($id)
    {
       // $idFix = str_split($id);
        $data = ItemsStockWarehouse::with(['get_item_variant', 'get_item_variant.get_items'])->where('id_warehouse', Auth::user()->id_warehouse)->where('id_item_variant', $id)->first();

        if ($data) {
            return response()->json([
                'success' => 'true',
                'data' => $data
            ]);
        } else {
            return response()->json([
                'success' => 'false',
                'data' => null
            ]);
        }
    }

    public function store(Request $req)
    {
        // if(!empty($req->salesId)){
        //     return response()->json([
        //         'success' => 'false',
        //         'sales_id' => $req->salesId
        //     ]);
        // } else{
        //ins sales
        $sales = Sales::create([
            'user_id' => Auth::user()->id,
            'customer_id' => $req->customer,
            'warehouse_id' => 4, //Auth::user()->id_warehouse,
            'cost_amount' => $req->amount,
            'discount' => $req->discount,
            'payment_method' => $req->payment_method,
            'total' => 0
        ]);
        if ($req->id_item_variant) {
            foreach ($req->get('id_item_variant') as $key => $id_item_variant) {
                $harga = $req->get('harga');
                $qty = $req->get('qty');

                $salesDetail = SalesDetail::create([
                    'item_id' => $id_item_variant,
                    'sales_id' => $sales->id,
                    'qty' => $qty["$key"],
                    'price' => $harga["$key"],
                    'subtotal' => (int) $qty["$key"] * (int) $harga["$key"]
                ]);
                //insert to stock cards
                $this->saveStockCard($id_item_variant, $sales->warehouse_id, $qty["$key"], 'out', 'billing');
                //cut stock
                $this->cutStockItemWarehouse($id_item_variant, $sales->warehouse_id, $qty["$key"]);
            }
        }
        // Activity Log
        $description = 'ID Penjualan Barang '. $sales->id;
        $this->addLog('Transaksi Dengan Customer', $description);
        return response()->json([
            'success' => 'true',
            'sales_id' => $sales->id
        ]);
        //  }

    }

    public function printReceipt($salesId)
    {
        $profile = DataProfile::take(1)->first();
        $sales = Sales::where('id', $salesId)->first();
        $salesDetail = SalesDetail::with(['itemVariant'])->where('sales_id', $salesId)->get();

        $showImage = Storage::disk('public')->get($profile->logo_image);
        return view('pages/billing/struk', compact(['sales', 'salesDetail', 'profile', 'showImage']));
    }

}
