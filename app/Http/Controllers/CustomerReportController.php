<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\CustomerExport;
use App\Traits\ActivityLogTrait;
use Maatwebsite\Excel\Facades\Excel;

class CustomerReportController extends Controller
{
    use ActivityLogTrait;
    public function index()
    {
        return view('pages/customer/customer-report');
    }

    public function export($dateFirst, $dateEnd)
    {
        $export = new CustomerExport;
        $export->dateFirst($dateFirst);
        $export->dateEnd($dateEnd);
        // Activity Log
        $description = 'Cetak Laporan Daftar Customer';
        $this->addLog('Cetak Laporan', $description);
        return Excel::download($export, 'customer.xlsx');
    }
}
