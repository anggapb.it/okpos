<?php
namespace App\Http\Controllers;
setlocale(LC_ALL, 'id_ID');

use App\Models\Items as ModelsItems;
use Illuminate\Http\Request;
use App\Models\RegistrasiDet;
use App\Models\Sales;
use App\Models\SalesDetail;
use App\Models\Items;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;

class DashboardController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $amountItemSalesToday = SalesDetail::selectRaw('ifnull(sum(qty),0) as item_sales')->whereRaw('DATE(created_at) = CURRENT_DATE')->first();

        $profitSalesToday = Sales::selectRaw('ifnull(sum(cost_amount),0) as amount')->whereRaw('DATE(created_at) = CURRENT_DATE')->first();

        $salesToday = Sales::whereRaw('DATE(created_at) = CURRENT_DATE')->count();

        $salesMonth = Sales::whereRaw('MONTH(created_at) = MONTH(now())')->whereRaw('YEAR(created_at) = YEAR(now())')->count();

        $latestSales = Sales::with('customer')->skip(0)->take(3)->orderByDesc('created_at')->get();

        $latestItem = Items::latest()->first();

        // Grafik
        $month = Sales::select(DB::raw("MONTHNAME(created_at) as bulan"))
                    ->GroupBy(DB::raw("MONTHNAME(created_at)"))
                    ->orderBy(DB::raw("created_at", "asc"))
                    ->pluck('bulan');
        $salesReport = Sales::select(DB::raw("COUNT(*) as count"))
                    ->whereYear("created_at",date('Y'))
                    ->groupBy(DB::raw("Month(created_at)"))
                    ->pluck('count');


        $bestSellingItems = SalesDetail::with('itemVariant')->orderBy('qty', 'desc')->take(5)->withTrashed()->get();
        return view('dashboard', compact(['amountItemSalesToday', 'salesToday', 'salesMonth', 'profitSalesToday', 'latestSales', 'latestItem', 'salesReport','bestSellingItems','month']));


    }
    // public function index()
    // {
    //     $data = RegistrasiDet::select(\DB::raw("COUNT(*) as count"),\DB::raw("MONTHNAME(tglreg) as bln"))
    //             //->whereYear('tglreg', date('Y'))
    //             ->whereRaw('DATE_SUB(tglreg, INTERVAL 1 YEAR)')
    //             ->groupBy(\DB::raw("MONTHNAME(tglreg)"))
    //             ->orderBy('tglreg','asc');
    //     $kunjungan = $data->pluck('count');
    //     $bln = $data->pluck('bln');

    //     return view('dashboard', compact('kunjungan','bln'));
    // }

}
