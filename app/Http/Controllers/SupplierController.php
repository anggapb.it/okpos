<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/supplier/index');
    }

    public function get_data()
    {
        $data = Supplier::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function create()
    {
        return view('pages/supplier/create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
        ]);

        $supplier = Supplier::create([
            'name' => $request->name
        ]);

        return redirect()->route('supplier');
    }

    public function destroy($id)
    {
        $supplier = Supplier::findOrFail($id);
        $supplier->delete();

        return redirect()->route('supplier');
    }

    public function edit($id)
    {
        $supplier = Supplier::findOrFail($id);
        return view('pages/supplier/edit',compact(['supplier']));
    }

    public function update(Request $request)
    {
        $supplier = Supplier::find($request->id);
        $request->validate([
            'name' => 'required|string|max:255'
        ]);

        $supplier->update([
            'name' => $request->name
        ]);

        return redirect()->route('supplier');
    }
}
