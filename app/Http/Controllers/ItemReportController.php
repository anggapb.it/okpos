<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\ItemExport;
use App\Traits\ActivityLogTrait;
use Maatwebsite\Excel\Facades\Excel;

class ItemReportController extends Controller
{
    use ActivityLogTrait;
    public $export;

    public function __construct(){
        $this->export = new ItemExport;
    }
    public function index(){
        return view('pages.items.item-report');
    }

    // Eksport Semua Data
    public function all(){
        return Excel::download($this->export, 'barang.xlsx');
    }
    // Ekspor Berdasarkan Tanggal
    public function export($dateFirst, $dateEnd){
        $this->export->dateFirst($dateFirst);
        $this->export->dateEnd($dateEnd);
        // Activity Log
        $description = 'Cetak Laporan Barang';
        $this->addLog('Cetak Laporan', $description);
        return Excel::download($this->export, 'barang.xlsx');
    }
}
