<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Warehouse;
use App\Traits\ActivityLogTrait;
use Spatie\Permission\Models\Role;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;



class CustomUserController extends Controller
{
    use ActivityLogTrait;
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $roles = Role::all();
        return view('pages/setting/user/index',compact(['roles']));
    }

    public function get_data()
    {
        $data = User::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function create()
    {
        $roles = Role::all();
        $warehouse = Warehouse::all();
        return view('pages/setting/user/create',compact(['roles','warehouse']));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed|min:8',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'role' => $request->role,
            'id_warehouse' => $request->warehouse,
            'password' => Hash::make($request->password)
        ]);

        $roles = $request->input('role') ? $request->input('role') : [];
        $user->assignRole($roles);

        // Activity Log
        $description = 'ID User '.$user->id;
        $this->addLog('Tambah User Baru', $description);
       // event(new Registered($user));

        return redirect()->route('user');
    }

    public function destroy($id)
    {
        // Activity Log
        $description = 'ID User '.$id;
        $this->addLog('Hapus User ', $description);

        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('user');
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all();
        $warehouse = Warehouse::all();
        return view('pages/setting/user/edit',compact(['roles','warehouse','user']));
    }

    public function update(Request $request)
    {
        $user = User::find($request->id);
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|confirmed|min:8',
        ]);

        $user->update([
            'role' => $request->role,
            'id_warehouse' => $request->warehouse,
            'name' => $request->name,
           // 'email' => $request->email
           'password' => Hash::make($request->password)
        ]);

        $roles = $request->input('role') ? $request->input('role') : [];
        $user->assignRole($roles);

        // Activity Log
        $description = 'ID User '.$user->id;
        $this->addLog('Ubah Data User', $description);

        return redirect()->route('user');
    }
}
