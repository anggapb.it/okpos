<?php

namespace App\Http\Controllers;
use App\Models\Sales;
use App\Models\ItemsVariant;
use App\Models\SalesDetail;
use App\Traits\ActivityLogTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Traits\StockCardTrait;

class SalesController extends Controller
{
    use StockCardTrait, ActivityLogTrait;
    public function index(){
        return view('pages/sales_history/index');
    }
    public function get_data(){
        $data = Sales::with('customer');
        return datatables()->of($data)
        ->addIndexColumn('customer', function(Sales $sales){
            return $sales->customer->name;
        })
        ->make(true);
    }
    public function get_data_sorted($dateF, $dateE){
        $dateFirst = $dateF;
        $dateEnd = $dateE;
        $data = Sales::with(['customer'])->whereRaw('date_format(created_at, "%Y-%m-%d") BETWEEN "'.$dateFirst.'" AND "'.$dateEnd.'" ')->get();
        return datatables()->of($data)
        ->addIndexColumn('customer', function(Sales $sales){
            return $sales->customer->name;
        })
        ->make(true);
    }
    public function retur($id){
        // $salesDetail = SalesDetail::with('itemVariant')->where('sales_id', $id)->get();
        $data = DB::table('sales')
                ->join('sales_details', 'sales_details.sales_id', '=', 'sales.id')
                ->join('item_variants', 'sales_details.item_id', '=', 'item_variants.id')
                ->join('item_stock_warehouse', 'item_stock_warehouse.id_item_variant', '=', 'item_variants.id')
                // ->join('item_stock_cards', 'item_stock_cards.id_item_variant', '=', 'item_variants.id')
                ->select('sales.id','sales.warehouse_id as warehouse_id', 'sales_details.*','item_variants.id as item_variant_id', 'item_stock_warehouse.id as item_stock_warehouse_id')
                ->where('sales.id', $id)
                ->get();
        // dd($data);
        foreach($data as $key => $data){
            // $qty += $d->qty;
            $this->addStockItemWarehouse($data->item_variant_id, $data->warehouse_id, $data->qty);
            $this->saveStockCard($data->item_variant_id, $data->warehouse_id, $data->qty , 'in', 'sales');
            // Activity Log
            $description = 'ID Variant Barang '.$$data->item_variant_id.', Quantity '.$data->qty;
            $this->addLog('Customer Retur Barang', $description);
        }
        Sales::findOrFail($id)->delete(); //Soft Delete
        return redirect()->route('sales.history')->with(['message' => 'data berhasil dihapus']);
    }
}
