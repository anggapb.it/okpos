<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\ActivityLog;
use Illuminate\Http\Request;

class ActivityLogController extends Controller
{
    public function index(){
        return view('pages.activity_log.index');
    }
    public function getData(){
        $data = ActivityLog::with('user')->get();
        return datatables()->of($data)->make(true);
    }
}
