<?php

namespace App\Http\Controllers;

use App\Models\PaymentMethod;
use Illuminate\Http\Request;

class PaymentMethodController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/payment_method/index');
    }

    public function get_data()
    {
        $data = PaymentMethod::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function create()
    {
        return view('pages/payment_method/create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
        ]);

        $payment_method = PaymentMethod::create([
            'name' => $request->name
        ]);

        return redirect()->route('payment-method');
    }

    public function destroy($id)
    {
        $payment_method = PaymentMethod::findOrFail($id);
        $payment_method->delete();

        return redirect()->route('payment-method');
    }

    public function edit($id)
    {
        $payment_method = PaymentMethod::findOrFail($id);
        return view('pages/payment_method/edit',compact(['payment_method']));
    }

    public function update(Request $request)
    {
        $payment_method = PaymentMethod::find($request->id);
        $request->validate([
            'name' => 'required|string|max:255'
        ]);

        $payment_method->update([
            'name' => $request->name
        ]);

        return redirect()->route('payment-method');
    }
}
