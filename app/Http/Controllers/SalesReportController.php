<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\SalesExport;
use App\Traits\ActivityLogTrait;
use Maatwebsite\Excel\Facades\Excel;

class SalesReportController extends Controller
{
    use ActivityLogTrait;
    public function index()
    {
        return view('pages/report/sales-report');
    }

    public function export($dateFirst, $dateEnd)
    {
        $export = new SalesExport;
        $export->dateFirst($dateFirst);
        $export->dateEnd($dateEnd);
        // Activity Log
        $description = 'Cetak Laporan Penjualan';
        $this->addLog('Cetak Laporan', $description);
        return Excel::download($export, 'penjualan.xlsx');
    }
}
