<?php

namespace App\Traits;

use App\Models\ItemsStockCards;
use App\Models\ItemsStockWarehouse;
use App\Models\ItemsVariant;
use Exception;

trait StockCardTrait
{
    public function saveStockCard($id_item_variant, $id_warehouse, $qty, $type = null, $notes = null)
    {
        $stockCardLatest = ItemsStockCards::where('id_item_variant', $id_item_variant)
                        ->where('id_warehouse', $id_warehouse)->latest('id')->first();
                        if(!empty($stockCardLatest)){
            // dd($stockCardLatest);
            $stockFirst = $stockCardLatest->stock_final;
            $stockIn = ($type == 'in') ? $qty : 0;
            $stockOut = ($type == 'out') ? $qty : 0;
            $stockFinal =  (int) $stockCardLatest->stock_final + (($type == 'in') ? (int) $qty : 0) - (($type == 'out') ? (int) $qty : 0);
        // dd($stockFinal);
        } else{
            $stockFirst = $qty;
            $stockIn = 0;
            $stockOut = 0;
            $stockFinal = $qty;
        }

        $arr = [
            'id_item_variant' => $id_item_variant,
            'id_warehouse' => $id_warehouse,
            'stock_in' => $stockIn,
            'stock_out' => $stockOut,
            'stock_first' => $stockFirst,
            'stock_final' => $stockFinal,
            'type_stock_cards' => $type,
            'notes' => $notes,
        ];

        try {
            $stockCard = ItemsStockCards::create($arr);
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function addStockItemWarehouse($id_item_variant, $id_warehouse, $qty)
    {
        try {
            $stockWarehouse = ItemsStockWarehouse::where('id_item_variant', $id_item_variant)->where('id_warehouse', $id_warehouse)->first();
            $stockWarehouse->stock = (int) $stockWarehouse->stock + (int) $qty;

            $stockWarehouse->save();

            $itemsVariant = ItemsVariant::where('id', $id_item_variant)->first();
            $itemsVariant->stock = (int) $itemsVariant->stock + (int) $qty;

            $itemsVariant->save();
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function cutStockItemWarehouse($id_item_variant, $id_warehouse, $qty)
    {
        try {
            $stockWarehouse = ItemsStockWarehouse::where('id_item_variant', $id_item_variant)->where('id_warehouse', $id_warehouse)->first();
            $stockWarehouse->stock = (int) $stockWarehouse->stock - (int) $qty;

            $stockWarehouse->save();

            $itemsVariant = ItemsVariant::where('id', $id_item_variant)->first();
            $itemsVariant->stock = (int) $itemsVariant->stock - (int) $qty;

            $itemsVariant->save();
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
