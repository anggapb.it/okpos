<?php
namespace App\Traits;

use App\Models\ActivityLog;
use Exception;
use Illuminate\Support\Facades\Auth;

trait ActivityLogTrait
{
    public static function addLog($activity, $description){ 
        $log = [
            'user_id' => Auth::user()->id,
            'activity' => $activity,
            'description' => $description,
        ];
        try {
            $logs = ActivityLog::create($log);
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}