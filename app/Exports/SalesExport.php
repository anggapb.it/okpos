<?php

namespace App\Exports;

use App\Models\Sales;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SalesExport implements FromView
{
    public function dateFirst($dateFirst)
    {
        $this->dateFirst = $dateFirst;

        return $this;
    }

    public function dateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    public function view(): View
    {
        return view('pages.exports.sales', [
            'sales' => Sales::with(['customer'])->whereRaw('date_format(created_at, "%Y-%m-%d") BETWEEN "'.$this->dateFirst.'" AND "'.$this->dateEnd.'" ')->get(),
            'dateFirst' => $this->dateFirst,
            'dateEnd' => $this->dateEnd,
        ]);
    }
}
