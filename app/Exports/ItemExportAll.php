<?php

namespace App\Exports;

use App\Models\ItemsVariant;
use Maatwebsite\Excel\Concerns\FromCollection;

class ItemExportAll implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return ItemsVariant::with(['get_items'])->get();
    }
}
