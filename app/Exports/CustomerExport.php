<?php

namespace App\Exports;

use App\Models\Customer;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class CustomerExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function dateFirst($dateFirst)
    {
        $this->dateFirst = $dateFirst;

        return $this;
    }

    public function dateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    public function view(): View
    {
        return view('pages.exports.customer', [
            'customer' => Customer::whereRaw('date_format(created_at, "%Y-%m-%d") BETWEEN "'.$this->dateFirst.'" AND "'.$this->dateEnd.'" ')->get(),
            'dateFirst' => $this->dateFirst,
            'dateEnd' => $this->dateEnd,
        ]);
    }
}
