<?php

namespace App\Exports;

use App\Models\SalesDetail;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;

class SalesDetailExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return SalesDetail::all();
    }
    public function dateFirst($dateFirst)
    {
        $this->dateFirst = $dateFirst;

        return $this;
    }

    public function dateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }
    public function view(): View
    {
        $data = DB::table('sales')
                ->join('sales_details', 'sales_details.sales_id', '=', 'sales.id')
                ->join('customers', 'sales.customer_id', '=', 'customers.id')
                ->join('item_variants', 'sales_details.item_id', '=', 'item_variants.id')
                ->join('items', 'item_variants.item_id', '=', 'items.id')
                ->select('sales.*', 'sales_details.*','customers.name as customers_name','item_variants.*','items.name as items_name','items.description')
                ->whereRaw('date_format(sales.created_at, "%Y-%m-%d") BETWEEN "'.$this->dateFirst.'" AND "'.$this->dateEnd.'" ')
                ->get();
        if (empty($this->dateFirst) && empty($this->dateEnd)) {
            return view('pages.exports.sales-detail', [
                'sales' => SalesDetail::with(['itemVariant'])->get()
            ]);
        }
        else{
            return view('pages.exports.sales-detail', [
                'salesDetail' => $data,
                'dateFirst' => $this->dateFirst,
                'dateEnd' => $this->dateEnd,
            ]);
        }
        
    }
}
