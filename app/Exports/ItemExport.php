<?php

namespace App\Exports;

use App\Models\ItemsVariant;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class ItemExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function dateFirst($dateFirst)
    {
        $this->dateFirst = $dateFirst;

        return $this;
    }

    public function dateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    public function view(): View
    {
        if (empty($this->dateFirst) && empty($this->dateEnd)) {
            return view('pages.exports.items', [
                'items' => ItemsVariant::with(['get_items'])->get()
            ]);
        }
        else{
            return view('pages.exports.items', [
                'items' => ItemsVariant::with(['get_items'])->whereRaw('date_format(created_at, "%Y-%m-%d") BETWEEN "'.$this->dateFirst.'" AND "'.$this->dateEnd.'" ')->get(),
                'dateFirst' => $this->dateFirst,
                'dateEnd' => $this->dateEnd,
            ]);
        }
        
    }
}
