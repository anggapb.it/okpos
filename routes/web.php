<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/debug-sentry', function () {
    throw new Exception('My first Sentry error!');
});

Route::middleware('auth')->group(function() {
    Route::get('/', [App\Http\Controllers\DashboardController::class, 'index']);

    Route::get('dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');


        //user
        Route::get('user', [App\Http\Controllers\Setting\CustomUserController::class, 'index'])->name('user');
        Route::get('user/create', [App\Http\Controllers\Setting\CustomUserController::class, 'create'])->name('user.create');
        Route::get('user/get-data', [App\Http\Controllers\Setting\CustomUserController::class, 'get_data']);
        Route::post('user/store', [App\Http\Controllers\Setting\CustomUserController::class, 'store'])->name('user.store');
        Route::post('user/update', [App\Http\Controllers\Setting\CustomUserController::class, 'update'])->name('user.update');
        Route::get('user/edit/{id}', [App\Http\Controllers\Setting\CustomUserController::class, 'edit']);
        Route::get('user/hapus/{id}', [App\Http\Controllers\Setting\CustomUserController::class, 'destroy']);

        // Change Password
        Route::post('password/reset', [App\Http\Controllers\PasswordController::class, 'update'])->name('password.edit');

        //OKPOS

        //Profile
        Route::get('profile', [App\Http\Controllers\DataProfileController::class, 'index'])->name('profile.edit');
        Route::get('profile/get-data', [App\Http\Controllers\DataProfileController::class, 'getData']);
        Route::post('profile/update', [App\Http\Controllers\DataProfileController::class, 'upOrIns'])->name('profile.update');


        Route::get('items', [App\Http\Controllers\ItemController::class, 'index'])->name('items');
        Route::get('items/create', [App\Http\Controllers\ItemController::class, 'create'])->name('items.create');
        Route::post('items/store', [App\Http\Controllers\ItemController::class, 'store'])->name('items.store');
        Route::post('items/update', [App\Http\Controllers\ItemController::class, 'update'])->name('items.update');
        Route::get('items/get-data', [App\Http\Controllers\ItemController::class, 'getData'])->name('items.get-data');
        Route::get('items/hapus/{id}', [App\Http\Controllers\ItemController::class, 'destroy']);
        Route::get('items/hapus-variant/{id}', [App\Http\Controllers\ItemController::class, 'deleteVariant']);
        Route::get('items/edit/{id}', [App\Http\Controllers\ItemController::class, 'edit']);

        Route::get('item-report', [App\Http\Controllers\ItemReportController::class, 'index'])->name('items.report');
        Route::get('item-report/export-all', [App\Http\Controllers\ItemReportController::class, 'all'])->name('items.export.all');
        Route::get('item-report/export/{tglAwal}/{tglAkhir}', [App\Http\Controllers\ItemReportController::class, 'export'])->name('export');

        Route::get('billing', [App\Http\Controllers\BillingController::class, 'index'])->name('billing');
        Route::get('billing/list-items', [App\Http\Controllers\BillingController::class, 'getItems'])->name('billing.list-items');
        Route::get('billing/list-items-by-id/{id}', [App\Http\Controllers\BillingController::class, 'getItemsById']);
        Route::post('billing/store', [App\Http\Controllers\BillingController::class, 'store'])->name('billing.store');
        Route::get('billing/print/{id}', [App\Http\Controllers\BillingController::class, 'printReceipt'])->name('billing.print');

        Route::get('supplier', [App\Http\Controllers\SupplierController::class, 'index'])->name('supplier');
        Route::get('supplier/create', [App\Http\Controllers\SupplierController::class, 'create'])->name('supplier.create');
        Route::get('supplier/get-data', [App\Http\Controllers\SupplierController::class, 'get_data']);
        Route::post('supplier/store', [App\Http\Controllers\SupplierController::class, 'store'])->name('supplier.store');
        Route::post('supplier/update', [App\Http\Controllers\SupplierController::class, 'update'])->name('supplier.update');
        Route::get('supplier/edit/{id}', [App\Http\Controllers\SupplierController::class, 'edit']);
        Route::get('supplier/hapus/{id}', [App\Http\Controllers\SupplierController::class, 'destroy']);

        Route::get('customer', [App\Http\Controllers\CustomerController::class, 'index'])->name('customer');
        Route::get('customer/create', [App\Http\Controllers\CustomerController::class, 'create'])->name('customer.create');
        Route::get('customer/get-data', [App\Http\Controllers\CustomerController::class, 'get_data']);
        Route::post('customer/store', [App\Http\Controllers\CustomerController::class, 'store'])->name('customer.store');
        Route::post('customer/update', [App\Http\Controllers\CustomerController::class, 'update'])->name('customer.update');
        Route::get('customer/edit/{id}', [App\Http\Controllers\CustomerController::class, 'edit']);
        Route::get('customer/hapus/{id}', [App\Http\Controllers\CustomerController::class, 'destroy']);

        Route::get('customer-report', [App\Http\Controllers\CustomerReportController::class, 'index'])->name('customer.report');
        Route::get('customer-report/export/{tglAwal}/{tglAkhir}', [App\Http\Controllers\CustomerReportController::class, 'export']);

        Route::get('warehouse', [App\Http\Controllers\WarehouseController::class, 'index'])->name('warehouse');
        Route::get('warehouse/create', [App\Http\Controllers\WarehouseController::class, 'create'])->name('warehouse.create');
        Route::get('warehouse/get-data', [App\Http\Controllers\WarehouseController::class, 'get_data']);
        Route::post('warehouse/store', [App\Http\Controllers\WarehouseController::class, 'store'])->name('warehouse.store');
        Route::post('warehouse/update', [App\Http\Controllers\WarehouseController::class, 'update'])->name('warehouse.update');
        Route::get('warehouse/edit/{id}', [App\Http\Controllers\WarehouseController::class, 'edit']);
        Route::get('warehouse/hapus/{id}', [App\Http\Controllers\WarehouseController::class, 'destroy']);

        Route::get('payment-method', [App\Http\Controllers\PaymentMethodController::class, 'index'])->name('payment-method');
        Route::get('payment-method/create', [App\Http\Controllers\PaymentMethodController::class, 'create'])->name('payment-method.create');
        Route::get('payment-method/get-data', [App\Http\Controllers\PaymentMethodController::class, 'get_data']);
        Route::post('payment-method/store', [App\Http\Controllers\PaymentMethodController::class, 'store'])->name('payment-method.store');
        Route::post('payment-method/update', [App\Http\Controllers\PaymentMethodController::class, 'update'])->name('payment-method.update');
        Route::get('payment-method/edit/{id}', [App\Http\Controllers\PaymentMethodController::class, 'edit']);
        Route::get('payment-method/hapus/{id}', [App\Http\Controllers\PaymentMethodController::class, 'destroy']);

        Route::get('item-category', [App\Http\Controllers\ItemCategoryController::class, 'index'])->name('item-category');
        Route::get('item-category/create', [App\Http\Controllers\ItemCategoryController::class, 'create'])->name('item-category.create');
        Route::get('item-category/get-data', [App\Http\Controllers\ItemCategoryController::class, 'get_data']);
        Route::post('item-category/store', [App\Http\Controllers\ItemCategoryController::class, 'store'])->name('item-category.store');
        Route::post('item-category/update', [App\Http\Controllers\ItemCategoryController::class, 'update'])->name('item-category.update');
        Route::get('item-category/edit/{id}', [App\Http\Controllers\ItemCategoryController::class, 'edit']);
        Route::get('item-category/hapus/{id}', [App\Http\Controllers\ItemCategoryController::class, 'destroy']);


        Route::get('print-barcode', [App\Http\Controllers\PrintBarcodeController::class, 'index'])->name('print-barcode');
        Route::get('print-barcode/get-items', [App\Http\Controllers\PrintBarcodeController::class, 'getItems'])->name('print.barcode.list.items');
        Route::get('print-barcode/print/{id}/{amount}/{idori}', [App\Http\Controllers\PrintBarcodeController::class, 'print']);

        Route::get('mutation-items', [App\Http\Controllers\MutationItemController::class, 'index'])->name('mutation');
        Route::get('mutation/list-items/{warehouseId}', [App\Http\Controllers\MutationItemController::class, 'getItemWarehouse']);
        Route::get('mutation/list-items-by-id/{id}/{warehouseId}', [App\Http\Controllers\MutationItemController::class, 'getItemsById']);
        Route::post('mutation-items/store', [App\Http\Controllers\MutationItemController::class, 'store'])->name('mutation.store');

        Route::get('stockin', [App\Http\Controllers\StockInController::class, 'index'])->name('stockin');
        Route::get('stockin/list-items/{warehouseId}', [App\Http\Controllers\StockInController::class, 'getItemWarehouse']);
        Route::get('stockin/list-items-by-id/{id}/{warehouseId}', [App\Http\Controllers\StockInController::class, 'getItemsById']);
        Route::post('stockin/store', [App\Http\Controllers\StockInController::class, 'store'])->name('stockin.store');

        Route::get('so', [App\Http\Controllers\ItemStockOpnameController::class, 'index'])->name('so');
        Route::get('so/list-items/{warehouseId}', [App\Http\Controllers\ItemStockOpnameController::class, 'getItemWarehouse']);
        Route::get('so/list-items-by-id/{id}/{warehouse}', [App\Http\Controllers\ItemStockOpnameController::class, 'getItemsById'])->name('so.list-items-by-id');
        Route::post('so/store', [App\Http\Controllers\ItemStockOpnameController::class, 'store'])->name('so.store');

        Route::get('sales-history', [App\Http\Controllers\SalesController::class, 'index'])->name('sales.history');
        Route::get('sales-history/retur/{id}', [App\Http\Controllers\SalesController::class, 'retur'])->name('sales.history.retur');
        Route::get('sales/get-data', [App\Http\Controllers\SalesController::class, 'get_data'])->name('sales.get-data');
        Route::get('sales-report', [App\Http\Controllers\SalesReportController::class, 'index'])->name('salesReport');
        Route::get('sales-report/export/{tglawal?}/{tglakhir?}', [App\Http\Controllers\SalesReportController::class, 'export']);

        Route::get('sorted-sales/get-data/{dateFirst?}/{dateEnd?}', [App\Http\Controllers\SalesController::class, 'get_data_sorted'])->name('sorted.sales');

        Route::get('sales-detail-report', [App\Http\Controllers\SalesDetailReportController::class, 'index'])->name('sales-detail.report');
        Route::get('sales-detail-report/export-all', [App\Http\Controllers\SalesDetailReportController::class, 'all'])->name('sales-detail.export.all');
        Route::get('sales-detail-report/export/{tglAwal}/{tglAkhir}', [App\Http\Controllers\SalesDetailReportController::class, 'export'])->name('sales-detail.export');


        Route::get('check-stock', [App\Http\Controllers\CheckStockController::class, 'index'])->name('check-stock');
        Route::get('check-stock/get-items', [App\Http\Controllers\CheckStockController::class, 'getItems'])->name('check.stock.list.items');

        Route::get('stock-card-report', [App\Http\Controllers\StockCardReportController::class, 'index'])->name('stock.card');
        Route::get('stock-card-report/get-items', [App\Http\Controllers\StockCardReportController::class, 'getItems'])->name('stock.card.list.items');
        Route::get('stock-card-report/get-stock-card', [App\Http\Controllers\StockCardReportController::class, 'getItemsStockCard'])->name('stock.card.list.data');

        Route::get('activity-log', [App\Http\Controllers\ActivityLogController::class, 'index'])->name('activity-log');
        Route::get('activity-log/getData', [App\Http\Controllers\ActivityLogController::class, 'getData']);
});

require __DIR__.'/auth.php';
